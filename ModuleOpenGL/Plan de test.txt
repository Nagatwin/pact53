Test dans le module OpenGL 

	1 - Importation d'un fichier XML (interface Android/PC)
	Préambule : La scène sera stockée dans une classe à part entière 'Scene'. Cette classe aura pour attribut des listes contenant les entités de chaque élément.
		Test de chargement d'une scène à partir d'un fichier xml
			Donnée : un fichier xml type (contenant un nombre connu d'éléments)
			Sortie : un objet de type 'Scene' 
		Automatisation du test : charger le même fichier xml et comparer la scène obtenue avec la scène type.

		
	2 - Affichage d'une scène
		Test d'affichage en OpenGL
			Donnée : un objet de type 'Scene'
			Sortie : Vue en OpenGL 
		Automatisation du test : charger une scène type, placer la caméra dans une vue donnée, et comparer les images obtenues.
		Test de déplacement de la caméra 
			Donnée : un déplacement à effectuer + une rotation
			Sortie : la nouvelle vue en OpenGL

	3 - Modification d'une scène
		3.1- Ajout d'objet :
			Test de création d'un objet : (Pour chaque classe d'objet)
				Donnée : paramètres du constructeur
				Sortie : un objet de type donné
			Test d'ajout d'un objet :
				Donnée : objet et sa position
				Sortie : la nouvelle scène obtenue
		3.2- Modification d'un objet :
			3.2.1 Sélection d'un objet :
			Test de mise en surbrillance :
				Donnée : objet à mettre en surbrillance
				Sortie : mise en surbrillance OpenGL
			3.2.2 Edition de l'objet :
			Test affichage de l'objet isolé (en dehors du cadre de la map pour plus de lisibilité) :
				Donnée : objet à afficher
				Sortie : Vue en OpenGL
			Test de changement de paramètres :
				Donnée : nouveau paramètre
				Sortie : Vue en OpenGL
		3.3- Suppression d'un objet :
			Test de suppression d'un objet :
				Donnée : objet à supprimer
				Sortie : la nouvelle scène
		3.4- Fusionner deux scènes : 
			Test de fusion de deux scènes : 
				Donnée : deux scènes à fusionner + paramètres
				Sortie : une unique scène contenant les scènes fusionnées

	4- Objets de la base de donnée :
		Tester le chargement d'un objet :
			Donnée : Objet à charger
			Sortie : l'objet chargé

	5- Sauvegarde d'une scène :
		Tester la sauvegarde d'une scène :
			Donnée : Scène à sauvegarder
			Sortie : un fichier xml représentant la scène
	6- Export BSP
	    Tester l'export d'un projet en fichier vmf :
	        Donnée : Projet à exporter
	        Sortie : Un fichier vmf
	    Tester la compilation en bsp :
	        Donnée : Fichier vmf
	        Sortie : Fichier bsp
	    Tester l'ajout de lumières :
	        Donnée : Fichier bsp
	        Sortie : Fichier bsp avec lumières

Autres tests : TOUS LES TESTS DE L'INTERFACE GRAPHIQUE (BOUTONS, RAFRAICHISSEMENT, MENUS...)
