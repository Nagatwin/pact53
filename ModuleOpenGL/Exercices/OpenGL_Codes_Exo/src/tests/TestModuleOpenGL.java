package tests;

import java.util.ArrayList;
import java.util.Arrays;

import prototype.Prototype;
import prototype.Texture;
import prototype.objects.RectangularCuboid;
import prototype.objects.Scene;
import prototype.objects.Vector;
import prototype.objects.Object;

public class TestModuleOpenGL {
	private Prototype prototype;
	
	public TestModuleOpenGL(Prototype prototype) {
		this.prototype = prototype;
	}
	
	public boolean testImportXML() {
		RectangularCuboid cube1 = new RectangularCuboid(new Vector(-10,0,-10), new Vector(20,0,30), 60, new Texture("data/textures/default.png"), prototype);
		RectangularCuboid cube2 = new RectangularCuboid(new Vector(-20,0,-40), new Vector(0,0,10), 70, new Texture("data/textures/default.png"), prototype);
		
		ArrayList<Object> liste1 = new ArrayList<Object>(Arrays.asList(cube1,cube2));
		ArrayList<Object> liste2 = new ArrayList<Object>(Arrays.asList(cube2,cube1));
		Scene scene1 = new Scene(new Vector(1,0,1), 50, liste1, prototype);
		Scene scene2 = new Scene(new Vector(1,0,1), 50, liste2, prototype);
		liste1.removeAll(liste2);
		return liste1.isEmpty();
	}
}
