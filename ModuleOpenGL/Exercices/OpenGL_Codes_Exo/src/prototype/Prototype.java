package prototype;

import static org.lwjgl.glfw.GLFW.GLFW_FALSE;
import static org.lwjgl.glfw.GLFW.GLFW_KEY_A;
import static org.lwjgl.glfw.GLFW.GLFW_KEY_B;
import static org.lwjgl.glfw.GLFW.GLFW_KEY_D;
import static org.lwjgl.glfw.GLFW.GLFW_KEY_DOWN;
import static org.lwjgl.glfw.GLFW.GLFW_KEY_ESCAPE;
import static org.lwjgl.glfw.GLFW.GLFW_KEY_LEFT;
import static org.lwjgl.glfw.GLFW.GLFW_KEY_RIGHT;
import static org.lwjgl.glfw.GLFW.GLFW_KEY_S;
import static org.lwjgl.glfw.GLFW.GLFW_KEY_UP;
import static org.lwjgl.glfw.GLFW.GLFW_KEY_W;
import static org.lwjgl.glfw.GLFW.GLFW_RELEASE;
import static org.lwjgl.glfw.GLFW.GLFW_RESIZABLE;
import static org.lwjgl.glfw.GLFW.GLFW_TRUE;
import static org.lwjgl.glfw.GLFW.GLFW_VISIBLE;
import static org.lwjgl.glfw.GLFW.glfwCreateWindow;
import static org.lwjgl.glfw.GLFW.glfwDefaultWindowHints;
import static org.lwjgl.glfw.GLFW.glfwGetPrimaryMonitor;
import static org.lwjgl.glfw.GLFW.glfwGetVideoMode;
import static org.lwjgl.glfw.GLFW.glfwInit;
import static org.lwjgl.glfw.GLFW.glfwMakeContextCurrent;
import static org.lwjgl.glfw.GLFW.glfwPollEvents;
import static org.lwjgl.glfw.GLFW.glfwSetErrorCallback;
import static org.lwjgl.glfw.GLFW.glfwSetKeyCallback;
import static org.lwjgl.glfw.GLFW.glfwSetWindowPos;
import static org.lwjgl.glfw.GLFW.glfwSetWindowShouldClose;
import static org.lwjgl.glfw.GLFW.glfwShowWindow;
import static org.lwjgl.glfw.GLFW.glfwSwapBuffers;
import static org.lwjgl.glfw.GLFW.glfwSwapInterval;
import static org.lwjgl.glfw.GLFW.glfwTerminate;
import static org.lwjgl.glfw.GLFW.glfwWindowHint;
import static org.lwjgl.glfw.GLFW.glfwWindowShouldClose;
import static org.lwjgl.opengl.GL11.glClearColor;
import static org.lwjgl.system.MemoryUtil.NULL;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;

import org.lwjgl.glfw.GLFWErrorCallback;
import org.lwjgl.glfw.GLFWKeyCallback;
import org.lwjgl.glfw.GLFWVidMode;
import org.lwjgl.opengl.ARBShaderObjects;
import org.lwjgl.opengl.GL;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GLCapabilities;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import prototype.objects.RectangularCuboid;
import prototype.objects.Scene;
import prototype.objects.Vector;
import prototype.objects.Wall;
import prototype.objects.Window;
import tests.TestModuleOpenGL;
import prototype.objects.CarvingElement;
import prototype.objects.Door;
import prototype.objects.Object;
import prototype.objects.Point;
import de.matthiasmann.twl.GUI;
import de.matthiasmann.twl.ThemeInfo;
import de.matthiasmann.twl.Widget;
import de.matthiasmann.twl.input.Input;
import de.matthiasmann.twl.renderer.Renderer;
import de.matthiasmann.twl.renderer.lwjgl.LWJGLRenderer;
import de.matthiasmann.twl.theme.ThemeManager;


public class Prototype {
	/* Attributes */
	//Window
	private long window;
		
	private boolean do_run = true; // runs until done is set to true
	private int display_width = 640;
	private int display_height = 640;

	private GLFWKeyCallback keyCallback;
	static GLCapabilities glcaps;

	//Camera
	private Camera camera;
	private int rotationSensitivity = 6;
	private int movementSpeed = 10;
	
	//Scene
	private Scene scene;
	private ArrayList<RectangularCuboid> skyBox = new ArrayList<RectangularCuboid>();
	//Writer
	private Writer writer;
	//Constructor
	public Prototype() {
		camera = new Camera();
		try {
			writer = new Writer("maps/new_map.vmf");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	//Getter
	public Writer getWriter() {
		return writer;
	}
	
	/* ------------------------------- GRAPHICS	---------------------- */
	
	//Running the program
	public void run() {
		//Initializations
		initDisplay();
		initGL();
		//Loading the scene
		loadScene();
		keyPressed();
		while(do_run) {
			if( glfwWindowShouldClose(window)) do_run = false;
			//On efface tout
			GL11.glClear(GL11.GL_COLOR_BUFFER_BIT| GL11.GL_DEPTH_BUFFER_BIT);
			//Main View
			setFirstView();
			camera.adjust();
			render();
			/* Plus tard
			//2DView from top
			setSecondView();
			render();*/

			glfwSwapBuffers(window);
			glfwPollEvents();
		}
		
		//Terminate GLFW and free the error callback
		glfwTerminate();
		glfwSetErrorCallback(null).free();
	}
	
	//Drawing the axis
	private void draw_axis() {
		//Axis
		//X axis
		GL11.glBegin(GL11.GL_LINES);
		GL11.glColor3f(1.0f, 0.0f, 0.0f); 
		GL11.glVertex3f(0f, 0f, 0f);
		GL11.glVertex3f(150f,0f,0f);
		GL11.glEnd();
		//Y axis
		GL11.glBegin(GL11.GL_LINES);
		GL11.glColor3f(0.0f,1.0f, 0.0f); 
		GL11.glVertex3f(0f, 0f, 0f);
		GL11.glVertex3f(0f,150f,0f);
		GL11.glEnd();
		//Z axis
		GL11.glBegin(GL11.GL_LINES);
		GL11.glColor3f(0f,0f,1f); 
		GL11.glVertex3f(0f, 0f, 0f);
		GL11.glVertex3f(0f,0f,150f);
		GL11.glEnd();
	}
	
	//Loading a fixed scene
	private void loadScene() {
		scene = loadFromXML(new XMLFile("data/plan.xml"));
	}
	
	public Scene getScene() {
		return scene;
	}
	
	//Rendering in the window
	private void render() {
		GL11.glEnable(GL11.GL_CULL_FACE);
		draw_axis();
		scene.drawGL();
		GL11.glDisable(GL11.GL_CULL_FACE);
	}
	
	private void initGL() {
		//Options
		GL11.glShadeModel(GL11.GL_SMOOTH);
		GL11.glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
		GL11.glClearDepth(1.0f);
		GL11.glEnable(GL11.GL_DEPTH_TEST);
		
		GL11.glDepthFunc(GL11.GL_LEQUAL);
		GL11.glEnable(GL11.GL_BLEND);
		GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
		GL11.glHint(GL11.GL_PERSPECTIVE_CORRECTION_HINT, GL11.GL_NICEST);
		
		GL11.glEnable(GL11.GL_TEXTURE_2D);
		GL11.glEnable(GL11.GL_COLOR_MATERIAL);
		GL11.glColorMaterial(GL11.GL_FRONT_AND_BACK, GL11.GL_AMBIENT_AND_DIFFUSE);
		
		float mat_specular[] = { 1f,1f,1f,1f };
		float ambient[] = {0.15f,0.15f,0.15f,1f};
		GL11.glEnable(GL11.GL_LIGHTING);
		GL11.glLightModelfv(GL11.GL_LIGHT_MODEL_AMBIENT, ambient);
		GL11.glLightfv(GL11.GL_LIGHT0,GL11.GL_AMBIENT, mat_specular);
		GL11.glEnable(GL11.GL_LIGHT0);

       GL11.glEnable(GL11.GL_CULL_FACE);
       GL11.glCullFace(GL11.GL_FRONT);
       GL11.glDisable(GL11.GL_CULL_FACE);

	}
	
	private void setFirstView() {
		//Definition de la fenetre
		GL11.glViewport(0, 0, display_width, display_height);
		//Definition de la vue
		GL11.glMatrixMode(GL11.GL_PROJECTION);
		GL11.glLoadIdentity();
		//GL11.glOrtho(-200f, 200f, -200f, 200f, -200f, 1000f);
		gluPerspective(80f,1f,0.1f,1000f);
		//GL11.glFrustum(-5f, 5f, -5f, 5f, 0.3f, 10f);
		GL11.glMatrixMode(GL11.GL_MODELVIEW);
		GL11.glLoadIdentity();
	}
	
	public static void gluPerspective(float fovy, float ratio, float pres, float loin) {
	    float bas = -pres * (float) Math.tan(fovy / 2);
	    float haut = -bas;
	    float gauche = ratio * bas;
	    float droite = -gauche;
	    GL11.glFrustum(gauche, droite, bas, haut, pres, loin);
	}
	
	//View from top
	private void setSecondView() {
		//Definition de la fenetre
		GL11.glViewport(display_width/2, 0, display_width/2, display_height);
		//Definition de la vue
		GL11.glMatrixMode(GL11.GL_PROJECTION);
		GL11.glLoadIdentity();
		GL11.glOrtho(-30f, 30f, -30f, 30f, -100f, 100f);
		GL11.glMatrixMode(GL11.GL_MODELVIEW);
		GL11.glLoadIdentity();
		GL11.glRotatef(90,1,0,0);
	}
	
	//KeyBoard Actions
	private void keyPressed() {
		glfwSetKeyCallback(window, new GLFWKeyCallback() {
			@Override
            public void invoke(long window, int key, int scancode, int action, int mods) {
				if ( key == GLFW_KEY_RIGHT ) camera.angleY(1*rotationSensitivity);
                if ( key == GLFW_KEY_LEFT ) camera.angleY(-1*rotationSensitivity);
	            if ( key == GLFW_KEY_UP ) camera.angleX(1*rotationSensitivity);
	            if ( key == GLFW_KEY_DOWN ) camera.angleX(-1*rotationSensitivity);
				if ( key == GLFW_KEY_D) camera.strafeRight(-movementSpeed);
				if ( key == GLFW_KEY_A) camera.strafeLeft(-movementSpeed);
				if ( key == GLFW_KEY_W) camera.walkForward(-movementSpeed);
				if ( key == GLFW_KEY_S) camera.walkBackward(-movementSpeed);
				if ( key == GLFW_KEY_ESCAPE && action == GLFW_RELEASE )
					glfwSetWindowShouldClose(window, true); // We will detect this in our rendering loop
				if (key == GLFW_KEY_B && action == GLFW_RELEASE) {
					writer.init(1, 0);
					for (RectangularCuboid sky : skyBox) {
						scene.addObject(sky);
					}
					scene.writeVMF();
					getWriter().addCTStart(new Point(300,300,40));
					writer.end();
					//Compilation de la map
					try {
							
				    		final String pathToProject =  new java.io.File( "." ).getCanonicalPath();;
				    		String line;
				    		//Compilation des textures
						      Process p = Runtime.getRuntime().exec(pathToProject+"\\data\\script\\compile-VTF.bat");
						      BufferedReader bri = new BufferedReader
						        (new InputStreamReader(p.getInputStream()));
						      BufferedReader bre = new BufferedReader
						        (new InputStreamReader(p.getErrorStream()));
						      while ((line = bri.readLine()) != null) {
						        System.out.println(line);
						      }
						      bri.close();
						      while ((line = bre.readLine()) != null) {
						        System.out.println(line);
						      }
						      bre.close();
						      p.waitFor();
				    		//Compilation BSP
					      p = Runtime.getRuntime().exec(pathToProject+"\\data\\script\\compile-VBSP.bat");
					      bri = new BufferedReader
					        (new InputStreamReader(p.getInputStream()));
					      bre = new BufferedReader
					        (new InputStreamReader(p.getErrorStream()));
					      while ((line = bri.readLine()) != null) {
					        System.out.println(line);
					      }
					      bri.close();
					      while ((line = bre.readLine()) != null) {
					        System.out.println(line);
					      }
					      bre.close();
					      p.waitFor();
					      System.out.println("Map compil�e avec succ�s !");
				    }
				    catch (Exception err) {
				      System.out.println("Erreur : la map n'a pas pu �tre compil�e");
				      err.printStackTrace();
				    }
					
					for (RectangularCuboid sky : skyBox) {
						scene.removeObject(sky);
					}
					
				}
			}
		});
	}

	public Scene loadFromXML(XMLFile file) {
		System.out.println("D�but du chargement de la sc�ne : ");
	    System.out.println("------------------------------------");
		NodeList nList = file.getDocument().getElementsByTagName("plan");
	    //Vecteurs tampons
	    Vector position;
	    Texture texture;
	    Scene newScene = null;
	    //Chargement de toutes les sc�nes :
	    for (int temp = 0; temp < nList.getLength(); temp++) {
		    System.out.println("------------------------------------");
	    	System.out.println("Nouvelle sc�ne : ");
	        Node nNode = nList.item(temp);
	        Element eElement = (Element) nNode;
	        /*position = new Vector(eElement.getElementsByTagName("position")
	        		.item(0).getTextContent());
	        int hauteur = Integer.parseInt(eElement.getElementsByTagName("hauteur")
	        		.item(0).getTextContent());*/
	        position = new Vector(0,0,0);
	        int hauteur = 200;
	        int xMin = Integer.parseInt(eElement.getElementsByTagName("xMin")
	        		.item(0).getTextContent());
	        int yMin = Integer.parseInt(eElement.getElementsByTagName("yMin")
	        		.item(0).getTextContent());
	        int xMax = Integer.parseInt(eElement.getElementsByTagName("xMax")
	        		.item(0).getTextContent());
	        int yMax = Integer.parseInt(eElement.getElementsByTagName("yMax")
	        		.item(0).getTextContent());
	        System.out.println("Position : " + position +"\n");
	        System.out.println("Hauteur : " + hauteur +"\n");
	        newScene = new Scene(position,hauteur, this);
	        
	        //Chargement du sol et du toit
	        Vector b1 = new Vector(xMin, -10, yMax);
	        Vector b2 = new Vector(xMin, -10, yMin);
	        Vector b3= new Vector(xMax, -10, yMin);
	        Vector b4 = new Vector(xMax, -10, yMax);
	        Texture floorTexture = new Texture("data/textures/floor/floor01");
	        RectangularCuboid floor = new RectangularCuboid(new Vector[] {b1,b2,b3,b4},10, floorTexture, this);
	        Texture skyTexture = new Texture("data/textures/sky/sky01");
	        
	        //Chargement d'une skybox englobant le tout
	        b2 = b2.translated(-101, -100, -100);
	        b1 = b1.translated(-100,-100,+100);
	        b3 = b3.translated(101,-100,-100);
	        b4 = b4.translated(100, -100, 100);
	        Texture skybox = new Texture("TOOLS/TOOLSSKYBOX");
	        RectangularCuboid skyBoxLeft= new RectangularCuboid(b2,b1,hauteur+200, skybox, this);
	        RectangularCuboid skyBoxRight= new RectangularCuboid(b4,b3,hauteur+200, skybox, this);
	        RectangularCuboid skyBoxFront= new RectangularCuboid(b2,b3,hauteur+200, skybox, this);
	        RectangularCuboid skyBoxRear= new RectangularCuboid(b1,b4,hauteur+200, skybox, this);
	        RectangularCuboid skyBoxBottom = new RectangularCuboid(new Vector[] {b1,b2,b3,b4}, 10, skybox, this);
	        RectangularCuboid skyBoxTop = new RectangularCuboid(new Vector[] {
	        		b1.translated(0, hauteur+200, 0),
	        		b2.translated(0, hauteur+200, 0),
	        		b3.translated(0, hauteur+200, 0),
	        		b4.translated(0, hauteur+200, 0)},10, skybox, this);
	        b1 = new Vector(xMin, newScene.getHeight(), yMax);
	        b2 = new Vector(xMin, newScene.getHeight(), yMin);
	        b3= new Vector(xMax, newScene.getHeight(), yMin);
	        b4 = new Vector(xMax, newScene.getHeight(), yMax);
	        RectangularCuboid sky = new RectangularCuboid(new Vector[] {b1,b2,b3,b4},10, skyTexture, this);
	        newScene.addObject(sky);
	        newScene.addObject(floor);
	        //adding the skybox to the array list (to minimize the memory in OpenGL)
	        skyBox.add(skyBoxLeft);
	        skyBox.add(skyBoxRight);
	        skyBox.add(skyBoxFront);
	        skyBox.add(skyBoxRear);
	        skyBox.add(skyBoxBottom);
	        skyBox.add(skyBoxTop);
	        //Chargement de tous les murs
	        int x1,x2,y1,y2;
	        NodeList wallList = eElement.getElementsByTagName("wall");
	        for (int tempWall = 0; tempWall < wallList.getLength(); tempWall++) {
	 	        System.out.println("------------");
		    	System.out.println("Nouveau mur : ");
	            Node nNodeWall = wallList.item(tempWall);
	            Element wElement = (Element) nNodeWall;
	            x1 = (int) Float.parseFloat(wElement.getElementsByTagName("x1")
		        		.item(0).getTextContent());
	            y1 = (int) Float.parseFloat(wElement.getElementsByTagName("y1")
		        		.item(0).getTextContent());
	            x2 = (int) Float.parseFloat(wElement.getElementsByTagName("x2")
		        		.item(0).getTextContent());
	            y2= (int) Float.parseFloat(wElement.getElementsByTagName("y2")
		        		.item(0).getTextContent());
	            System.out.println("x1 : " + x1 +"\n" + "y1 : "+y1+"\n"+"x2 : " + x2 +"\n" + "y2 : "+y2+"\n");
	           /* Chargement de la texture (pour le PAN 4)
	            texture = new Texture(wElement.getElementsByTagName("texture")
	            		.item(0).getTextContent());*/
	            texture = new Texture("data/textures/wall/wall01");
	            RectangularCuboid wall = new RectangularCuboid(new Vector(x1,0,y1), new Vector(x2,0,y2), newScene.getHeight(), texture, this);
	            System.out.println("Les coordonne�s du mur sont :");
	            for (Vector point : wall.getBase()) {
	            	System.out.println("Point : "+point.toString());
	            }
	            Wall realWall = new Wall(this, wall);
	            //Chargement des portes 
	            List<Node> doorElementList = getChildrenByTagName(wElement,"door");
	            for (int carvingElement = 0; carvingElement < doorElementList.size(); carvingElement++) {
		 	        System.out.println("------------");
			    	System.out.println("Nouvelle porte : ");
		            Node nNodeCarvingElement = doorElementList.get(carvingElement);
		            Element cElement = (Element) nNodeCarvingElement;
		            x1 = (int) Float.parseFloat(cElement.getElementsByTagName("x1")
			        		.item(0).getTextContent());
		            y1 = (int) Float.parseFloat(cElement.getElementsByTagName("y1")
			        		.item(0).getTextContent());
		            x2 = (int) Float.parseFloat(cElement.getElementsByTagName("x2")
			        		.item(0).getTextContent());
		            y2 = (int) Float.parseFloat(cElement.getElementsByTagName("y2")
			        		.item(0).getTextContent());
		            System.out.println("x1 : " + x1 +"\n" + "y1 : "+y1+"\n"+"x2 : " + x2 +"\n" + "y2 : "+y2+"\n");
		            /*Chargement de la texture (pour le PAN 4)
		            texture = new Texture(wElement.getElementsByTagName("texture")
		            		.item(0).getTextContent());*/
		            texture = new Texture("data/textures/box/box01");
		            Door door= new Door(new Vector(x1,0,y1), new Vector(x2,0,y2), 90, texture, this);
		            realWall.addElement(door);
		        }
	            //Chargement des fenetres
	            List<Node> windowElementList = getChildrenByTagName(wElement,"window");
	            for (int carvingElement = 0; carvingElement < windowElementList.size(); carvingElement++) {
		 	        System.out.println("------------");
			    	System.out.println("Nouvelle Fen�tre : ");
		            Node nNodeCarvingElement = windowElementList.get(carvingElement);
		            Element cElement = (Element) nNodeCarvingElement;
		            x1 = (int) Float.parseFloat(cElement.getElementsByTagName("x1")
			        		.item(0).getTextContent());
		            y1 = (int) Float.parseFloat(cElement.getElementsByTagName("y1")
			        		.item(0).getTextContent());
		            x2 = (int) Float.parseFloat(cElement.getElementsByTagName("x2")
			        		.item(0).getTextContent());
		            y2 = (int) Float.parseFloat(cElement.getElementsByTagName("y2")
			        		.item(0).getTextContent());
		            System.out.println("x1 : " + x1 +"\n" + "y1 : "+y1+"\n"+"x2 : " + x2 +"\n" + "y2 : "+y2+"\n");
		           /*Chargement de la texture (pour le PAN 4)
		            texture = new Texture(wElement.getElementsByTagName("texture")
		            		.item(0).getTextContent());*/
		            texture = new Texture("glass/offwndwb");
		            Window window = new Window(new Vector(x1,60,y1), new Vector(x2,60,y2), 90, texture, this);
		            realWall.addElement(window);
	            }
	            System.out.println("Nouveau mur : "+realWall.getElements().size());
	            newScene.addObject(realWall);
	         }
	         System.out.println("La sc�ne est charg�e, elle contient "+newScene.getObjects().size() +" �l�ments :"
	         		+ newScene.getObjects().toString());
	    }
	    return newScene;
	}
	
	public static List<Node> getChildrenByTagName(Element parent, String name) {
	    List<Node> nodeList = new ArrayList<Node>();
	    for (Node child = parent.getFirstChild(); child != null; child = child.getNextSibling()) {
	      if (child.getNodeType() == Node.ELEMENT_NODE && name.equals(child.getNodeName())) {
	        nodeList.add(child);
	      }
	    }
	    return nodeList;
	}
	/* -------------------------- Given functions ---------------------------- */
	private static String getLogInfo(int obj) {
        return ARBShaderObjects.glGetInfoLogARB(obj, ARBShaderObjects.glGetObjectParameteriARB(obj, ARBShaderObjects.GL_OBJECT_INFO_LOG_LENGTH_ARB));
    }

	//Display initialization
	private void initDisplay(){
		//taken from lwjgl https://www.lwjgl.org/guide
        try{

		// Setup an error callback. The default implementation
		// will print the error message in System.err.
		GLFWErrorCallback.createPrint(System.err).set();

		// Initialize GLFW. Most GLFW functions will not work before doing this.
		if ( !glfwInit() )
			throw new IllegalStateException("Unable to initialize GLFW");

		// Configure our window
		glfwDefaultWindowHints(); // optional, the current window hints are already the default
		glfwWindowHint(GLFW_VISIBLE, GLFW_FALSE); // the window will stay hidden after creation
		glfwWindowHint(GLFW_RESIZABLE, GLFW_TRUE); // the window will be resizable

		// Create the window
		window = glfwCreateWindow( display_width,  display_height, "Prototype", NULL, NULL);
		if ( window == NULL )
			throw new RuntimeException("Failed to create the GLFW window");

		//Create GUI window with TWL

  

		// Get the resolution of the primary monitor
		GLFWVidMode vidmode = glfwGetVideoMode(glfwGetPrimaryMonitor());

		glfwSetWindowPos(window, 200, 200);

		// Make the OpenGL context current
		glfwMakeContextCurrent(window);
		// Enable v-sync
		glfwSwapInterval(1);

		// Make the window visible
		glfwShowWindow(window);

		glcaps = GL.createCapabilities();

		glClearColor(1.0f, 0.0f, 0.0f, 0.0f);

        }catch(Exception e){
            System.out.println("Error setting up display: "+ e.getMessage());
            System.exit(0);
        }
    }	
	
	// Main
	public static void main(String[] args) {
		Prototype app = new Prototype();
        app.run();
	}	

}
