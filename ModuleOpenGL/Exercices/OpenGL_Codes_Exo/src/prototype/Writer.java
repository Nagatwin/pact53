package prototype;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;

import prototype.objects.Cube;
import prototype.objects.Plane;
import prototype.objects.Point;

public class Writer {
	private String fileName;
	private PrintWriter print;
	private int idWorld;
	private int idSolid;
	private int idSide;
	private int idEntity;
	private int thickness;
	
	public Writer(String fileName) throws FileNotFoundException {
		this.fileName = fileName;
		print = new PrintWriter(new FileOutputStream(new File("data/"+fileName), false)); 
		idWorld = 1;
		idSolid = 1;
		idSide = 1;
		idEntity = 1;
		thickness = 64;
	}
	
	//Getter
	public PrintWriter getPrint() {
		return print;
	}
	public int getIdSide() {
		return idSide;
	}
	public int getIdSolid() {
		return idSolid;
	}
	public int getIdWorld() {
		return idWorld;
	}
	public int getIdEntity() {
		return idEntity;
	}
	//Increments
	public void incrementsIdSide() {
		idSide++;
	}
	public void incrementsIdWorld() {
		idWorld++;
	}
	public void incrementsIdSolid() {
		idSolid++;
	}
	public void incrementsIdEntity() {
		idEntity++;
	}
	
	//Initialization of the map
	public void init(int versionNumber, int prefab) {
		print.println("versioninfo \n {");
		print.println("	\"map version\" \""+ versionNumber +"\"");
		print.println("	\"formatversion\" \"100\"");
		print.println("	\"prefab\" \"" + prefab +"\" \n }");
	}
	
	//End of the map
	public void end() {
		print.close();
	}
	//Initialization of the world 
	public void initWorld(int versionNumber, String skyName, String className){ 
		print.println("world\r\n" + "{");
		print.println("	\"id\" \""+ idWorld + "\"");
		//Increasing the world id
		idWorld++;
		print.println("	\"skyname\" \""+skyName+ "\"");
		print.println("	\"classname\" \""+className+"\"");
		//This is maybe not necessary
		print.println("	\"mapversion\" \""+ versionNumber +"\"");
		print.println("	\"detailmaterial\" \"detail/detailsprites\"");
		print.println("	\"detailvbsp\" \"detail.vbsp\"");
		print.println("	\"maxpropscreenwidth\" \"-1\"");
	}
	
	//Ending of the world
	public void endWorld() {
		print.println("}");
	}
	
	////////////////////////////////// WRITING SOLIDS /////////////////////////////////////////
	
	
	
////////////////////////////////////// ENTITIES //////////////////////////////////////
	public void addPlayerStart(Point origin) {
		print.println("entity\r\n" + "{");
		print.println("	\"id\" \""+idEntity+"\"");
		idEntity++;
		print.println("	\"classname\" \"info_player_start\"");
		print.println("	\"origin\" \""+ origin.toString()+"\" ");
		print.println("}");
	}
	
	public void addCTStart(Point origin) {
		print.println("entity\r\n" + "{");
		print.println("	\"id\" \""+idEntity+"\"");
		idEntity++;
		print.println("	\"classname\" \"info_player_counterterrorist\"");
		print.println("	\"origin\" \""+ origin.toString()+"\" ");
		print.println("}");
	}
	
	public void addTStart(Point origin) {
		print.println("entity\r\n" + "{");
		print.println("	\"id\" \""+idEntity+"\"");
		idEntity++;
		print.println("	\"classname\" \"info_player_terrorist\"");
		print.println("	\"origin\" \""+ origin.toString()+"\" ");
		print.println("}");
	}
	
	public void addDeathmatchStart(Point origin) {
		print.println("entity\r\n" + "{");
		print.println("	\"id\" \""+idEntity+"\"");
		idEntity++;
		print.println("	\"classname\" \"info_player_deathmatch\"");
		print.println("	\"origin\" \""+ origin.toString()+"\" ");
		print.println("}");
	}
	
	//Adding a list of spawns points
	public void addCTStart(Point[] li) {
		for (Point x :li) {
			addCTStart(x);
		}
	}
	public void addTStart(Point[] li) {
		for (Point x : li) {
			addTStart(x);
		}
	}
	public void addPlayerStart(Point[] li) {
		for (Point x : li) {
			addPlayerStart(x);
		}
	}
	public void addDeathmatchStart(Point[] li) {
		for (Point x : li) {
			addDeathmatchStart(x);
		}
	}
	
	//Controlling buyzones
	public void buyZone(Point p1, Point p7, int i) {
		//Computing all the other points
		Point p2 = new Point(p7.getX(), p1.getY(), p1.getZ());
		Point p3 = new Point(p7.getX(), p1.getY(), p7.getZ());
		Point p4 = new Point(p1.getX(), p1.getY(), p7.getZ());
		Point p5 = new Point(p1.getX(), p7.getY(), p1.getZ());
		Point p6 = new Point(p7.getX(), p7.getY(), p1.getZ());
		Point p8 = new Point(p1.getX(), p7.getY(), p7.getZ());
		
		print.println("entity\r\n" + "{");
		print.println("	\"id\" \""+idEntity+"\"");
		idEntity++;
		print.println("	\"classname\" \"func_buyzone\"");
		print.println("	\"TeamNum\" \""+ i+"\" ");
		print.println("	solid\r\n" + "{\r\n" + "\"id\" \""+ idSolid +"\"");
		idSolid++;
		//Drawing a box with no texture
		drawEmptySide(p5,p6,p2);
		drawEmptySide(p4,p3,p7);
		drawEmptySide(p5,p1,p4);
		drawEmptySide(p7,p3,p2);
		drawEmptySide(p3,p4,p1);
		drawEmptySide(p6,p5,p8);
		print.println("	}");
		print.println("}");
	}
	
	public void drawEmptySide(Point p1, Point p2, Point p3) {
		getPrint().println("side\r\n" + "{");
		getPrint().println("	\"id\" \""+idSide + "\"");
		incrementsIdSide();
		getPrint().println("	\"plane\" \"("+p1.toString()+") ("+p2.toString()+") ("+p3.toString()+")\"");
		getPrint().println("}");
	}
	public void buyZoneT(Point p1, Point p7) {
		buyZone(p1,p7, 2);
	}
	public void buyZoneCT(Point p1, Point p7) {
		buyZone(p1,p7,3);
	}
	
}
