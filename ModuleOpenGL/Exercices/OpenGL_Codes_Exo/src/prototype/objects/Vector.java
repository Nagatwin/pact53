package prototype.objects;

public class Vector {
	public int x;
	public int y;
	public int z;
	
	//Constructor
	public Vector(int x, int y, int z) {
		this.x =x;
		this.y =y;
		this.z = z;
	}
	//Construction d'un vecteur depuis une cha�ne de caract�res
	public Vector(String string) {
		String[] list = string.split(",");
		this.x = Integer.parseInt(list[0]);
		this.y = Integer.parseInt(list[1]);
		this.z = Integer.parseInt(list[2]);
	}
	public int getX() {
		return x;
	}
	public void setX(int x) {
		this.x = x;
	}
	public int getY() {
		return y;
	}
	public void setY(int y) {
		this.y = y;
	}
	public int getZ() {
		return z;
	}
	public void setZ(int z) {
		this.z = z;
	}

	public void translate(int tX, int tY, int tZ) {
		x+=tX; y+=tY; z+=tZ;
	}
	public Vector clone() {
		return new Vector(x,y,z);
	}
	
	public Vector translated(int tX, int tY, int tZ) {
		return new Vector(x+tX,y+tY,z+tZ);
	}
	
	public Vector milieu(Vector p) {
		return new Vector((x+p.getX())/2,(y+p.getY())/2,(z+p.getZ())/2);
	}
	

	//Exchange of y and z (for the vmf format)
	public String toString() {
		return Math.round(x)+" "+Math.round(z)+" "+Math.round(y);
	}
	
	public boolean equals(Vector v) {
		return x==v.getX() && y==v.getY() && z==v.getZ();
	}
}