package prototype.objects;

import prototype.Prototype;

public abstract class Object {
	private final Prototype prototype;
	
	public Object(Prototype prototype) {
		this.prototype = prototype;
	}

	public Prototype getPrototype() {
		return prototype;
	}
	
	//Drawing method
	public abstract void drawGL();
	//Writing method
	public abstract void writeVMF();
	
}
