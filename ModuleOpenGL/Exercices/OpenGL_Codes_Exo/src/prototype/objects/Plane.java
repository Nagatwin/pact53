package prototype.objects;

import org.lwjgl.opengl.GL11;

import prototype.Prototype;
import prototype.Texture;


public class Plane {
	//L'ordre est celui horaire
	private Vector[] sommets;
	private Texture texture;
	private Prototype prototype;
	private Vector[] axis;

	
	public Plane(Vector[] sommets, Texture texture, Vector[] axis, Prototype prototype) {
		this.prototype = prototype;
		this.sommets = sommets;
		this.texture = texture;
		this.axis = axis;
	}
	
	public Plane(Vector position, int width, int height, Texture texture, Vector[] axis) {
		//Computing the 4 points
		Vector point1 = position.clone();
		Vector point2 = position.translated(0,height, 0);
		Vector point3 = position.translated(width, height, 0);
		Vector point4 = position.translated(0,width , 0);
		sommets = new Vector[]{point1,point2,point3,point4};
		this.texture = texture;
		this.axis = axis;
	}

	public void draw() {
		//Binding the texture
		texture.bind();
		GL11.glBegin(GL11.GL_QUADS);
		GL11.glTexCoord2f(0, 0); GL11.glVertex3f(sommets[0].getX(),sommets[0].getY(),sommets[0].getZ());	
		GL11.glTexCoord2f(1, 0); GL11.glVertex3f(sommets[1].getX(),sommets[1].getY(),sommets[1].getZ());	
		GL11.glTexCoord2f(1, 1); GL11.glVertex3f(sommets[2].getX(),sommets[2].getY(),sommets[2].getZ());	
		GL11.glTexCoord2f(0, 1); GL11.glVertex3f(sommets[3].getX(),sommets[3].getY(),sommets[3].getZ());	
		GL11.glEnd();
		texture.unbind();
	}
	
	public void draw(int textureRepeat) {
		//Binding the texture
		texture.bind();
		GL11.glBegin(GL11.GL_QUADS);
		GL11.glTexCoord2f(0, 0); GL11.glVertex3f(sommets[0].getX(),sommets[0].getY(),sommets[0].getZ());	
		GL11.glTexCoord2f(textureRepeat, 0); GL11.glVertex3f(sommets[1].getX(),sommets[1].getY(),sommets[1].getZ());	
		GL11.glTexCoord2f(textureRepeat, textureRepeat); GL11.glVertex3f(sommets[2].getX(),sommets[2].getY(),sommets[2].getZ());	
		GL11.glTexCoord2f(0, textureRepeat); GL11.glVertex3f(sommets[3].getX(),sommets[3].getY(),sommets[3].getZ());	
		GL11.glEnd();
		texture.unbind();
	}
	
	public void writeVMF() {
		prototype.getWriter().getPrint().println("side\r\n" + "{");
		prototype.getWriter().getPrint().println("	\"id\" \""+prototype.getWriter().getIdSide() + "\"");
		prototype.getWriter().incrementsIdSide();
		prototype.getWriter().getPrint().println("	\"plane\" \"("+sommets[0].toString()+") ("+sommets[2].toString()+") ("+sommets[1].toString()+")\"");
		prototype.getWriter().getPrint().println("	\"material\" \""+texture.getName()+"\"");
		prototype.getWriter().getPrint().println("	\"uaxis\" \"["+axis[0].toString() +" 0] 1"+"\"\r\n" + "	\"vaxis\" \"["+axis[1].toString()+" 0] 1\"");
		prototype.getWriter().getPrint().println("	\"rotation\" \"0\"");
		prototype.getWriter().getPrint().println("	\"lightmapscale\" \"16\"");
		prototype.getWriter().getPrint().println("	\"smoothing_groups\" \"0\"");
		prototype.getWriter().getPrint().println("}");
	}
	

}
