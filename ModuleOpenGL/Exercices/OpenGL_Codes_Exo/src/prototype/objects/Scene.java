package prototype.objects;

import java.util.ArrayList;

import org.lwjgl.opengl.GL11;

import prototype.Prototype;

//Une scene est un ensemble d'objets
public class Scene extends Object{
	private Vector position;
	private int height = 400;
	private ArrayList<Object> objects = new ArrayList<Object>();
	private ArrayList<Object> entities = new ArrayList<Object>();
	
	public Scene(Vector position, Prototype prototype) {
		super(prototype);
		this.position = position;
	}
	
	public Scene(Vector position, int height, Prototype prototype) {
		super(prototype);
		this.height = height;
		this.position = position;
	}
	
	public Scene(Vector position, int height,  ArrayList<Object> objects, Prototype prototype) {
		this(position, prototype);
		this.height = height;
		this.objects = objects;
	}

	//Par d�faut, la position est (0,0,0), et la hauteur est 400;
	public Scene(Prototype prototype, ArrayList<Object> objects) {
		this(new Vector(0,0,0), 400,objects ,prototype);
	}

	public int getHeight() {
		return height;
	}
	public ArrayList<Object> getObjects(){
		return objects;
	}
	public void addObject(Object object) {
		this.objects.add(object);
	}
	public void removeObject(Object object) {
		this.objects.remove(object);
	}
	public void addEntity(Object entity) {
		this.entities.add(entity);
	}
	
	@Override
	public void drawGL() {
		GL11.glTranslatef(position.getX(),position.getY(), position.getZ());
		for (Object object : objects) {
			object.drawGL();
		}
	}

	@Override
	public void writeVMF() {
		System.out.println("D�but de l'�criture");
		getPrototype().getWriter().getPrint().println("world\r\n" + "{\r\n" + "\"id\" \""+ getPrototype().getWriter().getIdWorld()+"\"");
		getPrototype().getWriter().getPrint().println("\"classname\" \"worldspawn\"\r\n" + 
				"	\"detailmaterial\" \"detail/detailsprites\"\r\n" + 
				"	\"detailvbsp\" \"detail.vbsp\"\r\n" + 
				"	\"maxpropscreenwidth\" \"-1\"\r\n" + 
				"	\"skyname\" \"sky_day01_01\"");
		getPrototype().getWriter().incrementsIdWorld();
		for(Object object : objects) {
			System.out.println("D�but de l'objet : "+object.toString());
			object.writeVMF();
		}
		getPrototype().getWriter().getPrint().println("}");
		for(Object entity : entities) {
			System.out.println("Ecriture de l'entit� : " +entity.toString());
			entity.writeVMF();
		}
	}
	
}
