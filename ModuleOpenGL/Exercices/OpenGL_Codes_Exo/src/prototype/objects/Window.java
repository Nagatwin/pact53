package prototype.objects;

import prototype.Prototype;
import prototype.Texture;

public class Window extends CarvingElement {

	public Window(Vector origin, Vector end, int height, Texture texture, Prototype prototype) {
		super(origin, end, height, texture, prototype);
	}
	
	@Override
	public void drawGL() {
		
	}

	@Override
	public void writeVMF() {
		Vector upperright = getEnd().translated(0,getHeight(),0);
		Vector upperleft = getOrigin().translated(0,getHeight(),0);
		getPrototype().getWriter().getPrint().println("entity\r\n" + 
				"{\r\n" + 
				"	\"id\" \""+getPrototype().getWriter().getIdEntity()+"\"\r\n" + 
				"	\"error\" \"0\"\r\n" + 
				"	\"upperright\" \""+upperright.toString()+"\"\r\n" + 
				"	\"lowerright\" \""+getEnd().toString()+"\"\r\n" + 
				"	\"upperleft\" \""+upperleft.toString()+"\"\r\n" + 
				"	\"lowerleft\" \""+getOrigin().toString()+"\"\r\n" + 
				"	\"surfacetype\" \"0\"\r\n" + 
				"	\"fragility\" \"100\"\r\n" + 
				"	\"disablereceiveshadows\" \"0\"\r\n" + 
				"	\"renderfx\" \"0\"\r\n" + 
				"	\"pressuredelay\" \"0\"\r\n" + 
				"	\"explodemagnitude\" \"0\"\r\n" + 
				"	\"spawnobject\" \"0\"\r\n" + 
				"	\"nodamageforces\" \"0\"\r\n" + 
				"	\"explosion\" \"0\"\r\n" + 
				"	\"material\" \"0\"\r\n" + 
				"	\"PerformanceMode\" \"0\"\r\n" + 
				"	\"ExplodeRadius\" \"0\"\r\n" + 
				"	\"ExplodeDamage\" \"0\"\r\n" + 
				"	\"spawnflags\" \"0\"\r\n" + 
				"	\"propdata\" \"0\"\r\n" + 
				"	\"disableshadows\" \"0\"\r\n" + 
				"	\"gibdir\" \"0 0 0\"\r\n" + 
				"	\"health\" \"1\"\r\n" + 
				"	\"rendermode\" \"0\"\r\n" + 
				"	\"rendercolor\" \"255 255 255\"\r\n" + 
				"	\"renderamt\" \"255\"\r\n" + 
				"	\"classname\" \"func_breakable_surf\"");
		getPrototype().getWriter().incrementsIdEntity();
		Texture nd = new Texture("TOOLS/TOOLSNODRAW");
		RectangularCuboid window = new RectangularCuboid(getOrigin(), getEnd(), getHeight(), new Texture[] {getTexture(), getTexture(), nd, nd, nd, nd}, getPrototype());
		window.writeVMF();
		getPrototype().getWriter().getPrint().println("}");
		
	}
	
	
	

}

