package prototype.objects;

import prototype.Prototype;
import prototype.Texture;

public abstract class CarvingElement extends Object{
	private int height;
	private Vector origin;
	private Vector end;
	private Texture texture;
	
	public CarvingElement(Vector origin, Vector end, int height, Texture texture, Prototype prototype) {
		super(prototype);
		this.origin = origin;
		this.end = end;
		this.height = height;
		this.texture = texture;
	}
	
	public Vector getOrigin() {
		return origin;
	}
	public Vector getEnd() {
		return end;
	}
	public int getHeight() {
		return height;
	}
	public Texture getTexture() {
		return texture;
	}
	
	public abstract void drawGL();

	public abstract void writeVMF();

}
