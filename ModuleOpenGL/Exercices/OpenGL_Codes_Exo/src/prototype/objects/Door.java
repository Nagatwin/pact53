package prototype.objects;

import prototype.Prototype;
import prototype.Texture;

public class Door extends CarvingElement {

	public Door(Vector origin, Vector end, int height, Texture texture, Prototype prototype) {
		super(origin, end, height, texture, prototype);
	}
	
	@Override
	public void drawGL() {
		
	}

	@Override
	public void writeVMF() {
		getPrototype().getWriter().getPrint().println("entity\r\n" + 
				"{\r\n" + 
				"	\"id\" \""+getPrototype().getWriter().getIdEntity()+"\"\r\n" + 
				"	\"classname\" \"func_door_rotating\"\r\n" + 
				"	\"angles\" \"0 0 0\"\r\n" + 
				"	\"closesound\" \"Doors.FullClose4\"\r\n" + 
				"	\"disablereceiveshadows\" \"0\"\r\n" + 
				"	\"disableshadows\" \"0\"\r\n" + 
				"	\"distance\" \"90\"\r\n" + 
				"	\"dmg\" \"0\"\r\n" + 
				"	\"forceclosed\" \"0\"\r\n" + 
				"	\"health\" \"0\"\r\n" + 
				"	\"ignoredebris\" \"0\"\r\n" + 
				"	\"lip\" \"0\"\r\n" + 
				"	\"locked_sentence\" \"0\"\r\n" + 
				"	\"loopmovesound\" \"0\"\r\n" + 
				"	\"noise1\" \"DoorHandles.Unlocked1\"\r\n" + 
				"	\"origin\" \""+getOrigin().toString()+"\"\r\n" + 
				"	\"renderamt\" \"255\"\r\n" + 
				"	\"rendercolor\" \"255 255 255\"\r\n" + 
				"	\"renderfx\" \"0\"\r\n" + 
				"	\"rendermode\" \"0\"\r\n" + 
				"	\"solidbsp\" \"0\"\r\n" + 
				"	\"spawnflags\" \"288\"\r\n" + 
				"	\"spawnpos\" \"0\"\r\n" + 
				"	\"speed\" \"150\"\r\n" + 
				"	\"targetname\" \"swing_door_1.1\"\r\n" + 
				"	\"unlocked_sentence\" \"0\"\r\n" + 
				"	\"wait\" \"-1\"");
		getPrototype().getWriter().incrementsIdEntity();
		Vector p1 = getOrigin().translated(2, -2, -2);
		Vector p2 = getOrigin().translated(-2,-2,-2);
		Vector p3 = getOrigin().translated(-2,-2,2);
		Vector p4 = getOrigin().translated(2,-2,2);
		RectangularCuboid pivot = new RectangularCuboid(new Vector[] {p4,p3,p2,p1}, (getHeight()+4), new Texture("TOOLS/TOOLSORIGIN"), getPrototype());
		RectangularCuboid porte = new RectangularCuboid(getOrigin(), getEnd(), getHeight(), getTexture(), getPrototype());
		pivot.writeVMF();
		porte.writeVMF();
		getPrototype().getWriter().getPrint().println("}");
		
	}
	
	
	

}
