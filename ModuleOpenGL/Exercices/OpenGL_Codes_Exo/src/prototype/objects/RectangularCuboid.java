package prototype.objects;

import java.util.ArrayList;
import java.util.Arrays;

import org.lwjgl.opengl.GL11;

import prototype.Prototype;
import prototype.Texture;

public class RectangularCuboid extends Object{
	private Plane[] planes; //Order rear, front, left, right, up, bottom
	private Vector[] base; //Order : clockwise, starting from front right
	private int height;
	private Texture[] textures; //Order rear, front, left, right, up, bottom
	

	public RectangularCuboid(Vector[] base, int height, Texture[] textures, Prototype prototype) {
		super(prototype);
		this.base = base;
		this.height = height;
		this.textures = textures;
		computePlanes();
	}

	//Si une unique texture, on l'applique sur toutes les faces
	public RectangularCuboid(Vector[] base, int height, Texture texture, Prototype prototype) {
		this(base, height,new Texture[] {texture, texture, texture,texture, texture, texture}, prototype);
	}
	
	//Chargement d'un mur depuis deux points (Utile pour le chargement XML)
	public RectangularCuboid(Vector p1, Vector p2, int height, Texture[] textures, Prototype prototype) {
		super(prototype);
		this.height = height;
		this.textures = textures;
		//Computing the base
		int x = p2.getX()-p1.getX();
		int z = p2.getZ()-p1.getZ();
		float norme = (float) Math.sqrt(x*x+z*z);
		z = (int) (z/norme *5);
		x = (int) (x/norme *5);

		Vector r1 = new Vector(p2.getX()+z,p2.getY(),p2.getZ()-x);
		Vector r2 = new Vector(p1.getX()+z,p2.getY(),p1.getZ()-x);
		Vector r3 = new Vector(p1.getX()-z,p2.getY(),p1.getZ()+x);
		Vector r4 = new Vector(p2.getX()-z,p2.getY(),p2.getZ()+x);
		//Test du sens � donner 
		if (p1.getX()<p2.getX()) this.base = new Vector[]{r4,r3,r2,r1};
		else this.base = new Vector[]{r1,r2,r3,r4};
		computePlanes();
	}
	
	public RectangularCuboid(Vector p1, Vector p2, int height, Texture texture, Prototype prototype) {
		this(p1,p2,height,new Texture[] {texture, texture, texture,texture, texture, texture}, prototype);
	}
			
			
	public int getHeight() {
		return height;
	}
	
	
	//Fonction de chargement des planes 
	public void computePlanes() {
		//Computing the eight points
		//Bottom side clockwise
		Vector p1 = base[0];
		Vector p2 = base[1];
		Vector p3 = base[2];
		Vector p4 =base[3];
		//Up side clockwise
		Vector p5 = base[0].translated(0,height,0);
		Vector p6 =base[1].translated(0,height,0);
		Vector p7 =base[2].translated(0,height,0);
		Vector p8 = base[3].translated(0,height,0);
		
		//Computing the u/v axis
		Vector uaxis1 = new Vector(1,0,0);
		Vector vaxis1 = new Vector(0,0,-1);
		Vector uaxis2 = new Vector(0,0,1);
		Vector vaxis2 = new Vector(0,-1,0);
		
		//Creation of the 6 sides
		Plane rear = new Plane(new Vector[] {p3,p4,p8,p7}, textures[0], new Vector[]{uaxis1,vaxis2}, getPrototype());
		Plane front = new Plane(new Vector[] {p1,p2,p6,p5}, textures[1],new Vector[]{uaxis1,vaxis2},getPrototype());
		Plane left = new Plane(new Vector[] {p2,p3,p7,p6}, textures[2],new Vector[]{uaxis2,vaxis2},getPrototype());
		Plane right = new Plane(new Vector[] {p1,p5,p8,p4}, textures[3],new Vector[]{uaxis2,vaxis2},getPrototype());
		Plane up = new Plane(new Vector[] {p5,p6,p7,p8}, textures[4],new Vector[]{uaxis1,vaxis1},getPrototype());
		Plane bottom = new Plane(new Vector[] {p4,p3,p2,p1}, textures[5],new Vector[]{uaxis1,vaxis1},getPrototype());
		planes = new Plane[] {rear, front, left, right, up, bottom};
	}

	public Vector[] getBase() {
		return base;
	}
	 
	public Texture[] getTextures() {
		return textures;
	}
	
	//Carving the Rectangular Cuboid element
	public ArrayList<RectangularCuboid> carve(CarvingElement element) {
		Vector wallOrigin = base[1].milieu(base[2]);
		Vector wallEnd = base[0].milieu(base[3]);
		Vector elementOrigin = element.getOrigin();
		Vector elementEnd = element.getEnd();
		Vector eO = elementOrigin.translated(-wallOrigin.getX(), -wallOrigin.getY(), -wallOrigin.getZ());
		Vector eE = elementEnd.translated(-wallOrigin.getX(), -wallOrigin.getY(), -wallOrigin.getZ());
		Vector wE = wallEnd.translated(-wallOrigin.getX(), -wallOrigin.getY(), -wallOrigin.getZ());
		float l1,l2;
		if (eO.getX()<5) {
			l1= (float)(eO.getZ())/wE.getZ();
			l2=(float) (eE.getZ())/wE.getZ();
		}
		else {
			l1= (float)(eO.getX())/wE.getX();
			l2=(float)(eE.getX())/wE.getX();
		}
		int h1 = Math.max(element.getOrigin().getY(), wallOrigin.getY());
		int h2 = Math.min(element.getOrigin().getY()+element.getHeight(), wallOrigin.getY()+height);
		System.out.println("h1 : "+h1+", h2 : "+h2+", l1 : "+l1+", l2 : "+l2);
		System.out.println("Coordonn�es de la base du mur :");
		System.out.println("base[0] : "+base[0].toString());
		System.out.println("base[1] : "+base[1].toString());
		System.out.println("base[2] : "+base[2].toString());
		System.out.println("base[3] : "+base[3].toString());
		ArrayList<RectangularCuboid> result = new ArrayList<RectangularCuboid>();
		if (h1 >= h2 || l1>=1 || l2<=0) return new ArrayList<RectangularCuboid>(Arrays.asList(this));
		//Construction du bout du mur inf�rieur
		if (h1 > wallOrigin.getY()) {
			RectangularCuboid down = new RectangularCuboid(wallOrigin, wallEnd, h1-wallOrigin.getY(), textures, getPrototype());
			result.add(down);
		}
		//Construction des deux bouts s�par�s par le carving element
		if (l1>=0) {
			RectangularCuboid midLeft = new RectangularCuboid(wallOrigin.translated(0, h1, 0),elementOrigin.translated(0, h1-elementOrigin.getY(),0), element.getHeight(), textures, getPrototype());
			result.add(midLeft);
		}
		if (l2<=1) {
			RectangularCuboid midRight= new RectangularCuboid(elementEnd.translated(0, h1-elementEnd.getY(), 0),wallEnd.translated(0, h1, 0), element.getHeight(), textures, getPrototype());
			result.add(midRight);
		}
		//Construction du bout du mur sup�rieur
		if (wallOrigin.getY()+height > h2) {
			RectangularCuboid up = new RectangularCuboid(wallOrigin.translated(0,h2,0), wallEnd.translated(0, h2, 0),wallOrigin.getY()+height-h2, textures, getPrototype());
			result.add(up);
		}
		return result;
		}

	@Override
	public void drawGL() {
		//Draw
		for (Plane plane : planes) {
			plane.draw();
		}

	}

	@Override
	public void writeVMF() {
		getPrototype().getWriter().getPrint().println("solid\r\n" + "{\r\n" + "\"id\" \""+ getPrototype().getWriter().getIdSolid()+"\"");
		getPrototype().getWriter().incrementsIdSolid();
		for (Plane plane : planes) {
			plane.writeVMF();
		}
		getPrototype().getWriter().getPrint().println("}");
	}
	
}
