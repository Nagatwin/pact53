package prototype.objects;

import java.util.ArrayList;

import prototype.Prototype;

public class Wall extends Object{
	private ArrayList<RectangularCuboid> walls;
	private ArrayList<CarvingElement> carvingElements;
	
	public Wall(Prototype prototype, RectangularCuboid wall) {
		super(prototype);
		this.walls = new ArrayList<RectangularCuboid>();
		walls.add(wall);
		this.carvingElements = new ArrayList<CarvingElement>();
	}

	public Wall(Prototype prototype,RectangularCuboid wall, ArrayList<CarvingElement> carvingElements) {
		super(prototype);
		this.walls = new ArrayList<RectangularCuboid>();
		walls.add(wall);
		this.carvingElements = carvingElements;
		for(CarvingElement carvingElement : carvingElements) {
			carve(carvingElement);
		}
	}
	
	public void addElement(CarvingElement carvingElement) {
		this.carvingElements.add(carvingElement);
		carve(carvingElement);
	}
	
	public ArrayList<CarvingElement> getElements(){
		return carvingElements;
	}
	
	public void carve(CarvingElement carvingElement) {
		ArrayList<RectangularCuboid> temp = new ArrayList<RectangularCuboid>();
		for (RectangularCuboid wall : walls) {
			temp.addAll(wall.carve(carvingElement));
		}
		walls = temp;
		System.out.println("Size : "+walls.size());
	}
	
	@Override
	public void drawGL() {
		for(RectangularCuboid wall : walls) {
			wall.drawGL();
		}
		//Finalement, on dessine les objets;
		for(CarvingElement element : carvingElements) {
			element.drawGL();
		}
	}

	@Override
	public void writeVMF() {
		for(RectangularCuboid wall : walls) {
			wall.writeVMF();
		}
		for(CarvingElement element : carvingElements) {
			this.getPrototype().getScene().addEntity(element);
		}
	}


	
}
