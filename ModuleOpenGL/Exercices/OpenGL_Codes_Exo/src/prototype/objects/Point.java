package prototype.objects;

public class Point {
	private int x;
	private int y;
	private int z;
	
	//Constructor
	public Point(int x, int y, int z) {
		this.x =x;
		this.y =y;
		this.z = z;
	}
	
	//Getter
	public int getX() {
		return x;
	}
	public int getY() {
		return y;
	}
	public int getZ() {
		return z;
	}
	
	public String toString() {
		return x+" "+y+" "+z;
	}
	
}
