package prototype;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

import org.lwjgl.opengl.GL11;

import de.matthiasmann.twl.utils.PNGDecoder;
import de.matthiasmann.twl.utils.PNGDecoder.Format;
import prototype.objects.Vector;

public class Texture {

	private int textureId;
	private PNGDecoder decoder;
	private ByteBuffer buf;
	private String name;
	private Vector rotation;
	
	
	public Texture(String filename) {
		//Les textures doivent �tre pr�trait�es et plac�es dans le dossier materials
		String[] table = filename.split("/");
		name = "new_map/"+table[table.length-1];
		System.out.println("Texture name : "+name);
		decoder = null;
		InputStream in;
		try {
			in = new FileInputStream(filename+".png");
			decoder = new PNGDecoder(in);
		} catch (Exception e) {
			//On prend une texture par d�faut
			try {
				//Le nom devient le chemin complet
				name = filename;
				in = new FileInputStream("data/textures/default-texture.png");
				decoder = new PNGDecoder(in);
			} catch (Exception e1) {
				//Ne devrait jamais s'activer
				e1.printStackTrace();
			}
		}
		//Store in a buffer
		buf = ByteBuffer.allocateDirect(4 * decoder.getWidth() * decoder.getHeight());
		try {
			decoder.decode(buf, decoder.getWidth() * 4, Format.RGBA);
		} catch (IOException e) {
			e.printStackTrace();
		}
		buf.flip();
		// Create a new OpenGL texture 
		textureId = GL11.glGenTextures();
		this.rotation = new Vector(0,0,0);
	}
	
	public Texture(String filename, Vector rotation) {
		this(filename);
		this.rotation = rotation;
	}
	//bind a texture
	public void bind() {
		//reset the color
		GL11.glColor3f(1f, 1f, 1f);
		GL11.glBindTexture(GL11.GL_TEXTURE_2D, textureId);
		GL11.glTexImage2D(GL11.GL_TEXTURE_2D, 0, GL11.GL_RGBA, decoder.getWidth(),
			    decoder.getHeight(), 0, GL11.GL_RGBA, GL11.GL_UNSIGNED_BYTE, buf);
		GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MIN_FILTER, GL11.GL_NEAREST);
        GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MAG_FILTER, GL11.GL_NEAREST);
	}
	
	//unbind 
	public void unbind() {
        GL11.glBindTexture(GL11.GL_TEXTURE_2D, 0);
	}
	
	public String getName() {
		return name;
	}
	
	public boolean equals(Texture t) {
		return name==t.getName();
	}
}
