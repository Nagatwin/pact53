package prototype;

import org.lwjgl.opengl.GL11;
import prototype.objects.Vector;
import org.lwjgl.util.glu.*;

public class Camera {
	
	private Vector position;
	private Vector rotation;
	
	public Camera() {
		position = new Vector(0,0,0);
		rotation = new Vector(0,0,0);
	}
	//Constructor
	public Camera(Vector position, Vector rotation) {
		this.position = position;
		this.rotation = rotation;
	}
	
	//setter
	public void setPosition(int x, int y, int z) {
		position.x =x;
		position.y = y;
		position.z =z;
	}
	
	public void setRotation(int x, int y, int z) {
		rotation.x =x;
		rotation.y = y;
		rotation.z =z;
	}
	//Rotations
	public void angleX(int amount) {
		rotation.x+= amount;
	}
	
	public void angleY(int amount) {
		rotation.y += amount;
	}

	public void angleZ(int amount) {
		rotation.z+= amount;
	}
	//Walk Functions
	public void walkForward(int d) {
		position.x += d * Math.cos(Math.toRadians(rotation.x))*Math.sin(Math.toRadians(rotation.y));
		position.z -= d * Math.cos(	Math.toRadians(rotation.x))*Math.cos(Math.toRadians(rotation.y));
		position.y -= d* Math.sin(Math.toRadians(rotation.x));
		/*System.out.println("Les angles sont : X : "+rotation.x + " Y : "+rotation.y +" Z : "+rotation.z);
		System.out.println("Déplacement selon x : "+d * Math.cos(Math.toRadians(rotation.x))*Math.sin(Math.toRadians(rotation.y)));
		System.out.println("Déplacement selon y : -" + d* Math.sin(Math.toRadians(rotation.x)));
		System.out.println("Deplacement selon z : -"+ d * Math.cos(Math.toRadians(rotation.x))*Math.cos(Math.toRadians(rotation.y)));
		System.out.println("Les nouvelles postions sont : "+position.toString());*/
	}
	
	public void walkBackward(int d) {
		walkForward(-d);
	}
	
	public void strafeLeft(int d) {
		position.x += d * Math.sin(Math.toRadians(rotation.y+90));;
		position.z -= d * Math.cos(Math.toRadians(rotation.y+90));
	}

	public void strafeRight(int d) {
		strafeLeft(-d);
	}

	//Adjusting the camera
	public void adjust() {
		GL11.glLoadIdentity();
		GL11.glRotatef(rotation.x, 1.0f, 0.0f, 0.0f);
		GL11.glRotatef(rotation.y, 0.0f, 1.0f, 0.0f);
		GL11.glTranslatef(position.x, position.y, position.z);


	}

}
