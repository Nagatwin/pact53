package exercices;

import static org.lwjgl.assimp.Assimp.AI_MATKEY_COLOR_AMBIENT;
import static org.lwjgl.assimp.Assimp.AI_MATKEY_COLOR_DIFFUSE;
import static org.lwjgl.assimp.Assimp.AI_MATKEY_COLOR_SPECULAR;
import static org.lwjgl.assimp.Assimp.aiGetErrorString;
import static org.lwjgl.assimp.Assimp.aiGetMaterialColor;
import static org.lwjgl.assimp.Assimp.aiReleaseImport;
import static org.lwjgl.assimp.Assimp.aiTextureType_NONE;
import static org.lwjgl.glfw.GLFW.GLFW_FALSE;
import static org.lwjgl.glfw.GLFW.GLFW_KEY_B;
import static org.lwjgl.glfw.GLFW.GLFW_KEY_D;
import static org.lwjgl.glfw.GLFW.GLFW_KEY_DOWN;
import static org.lwjgl.glfw.GLFW.GLFW_KEY_ESCAPE;
import static org.lwjgl.glfw.GLFW.GLFW_KEY_LEFT;
import static org.lwjgl.glfw.GLFW.GLFW_KEY_Q;
import static org.lwjgl.glfw.GLFW.GLFW_KEY_RIGHT;
import static org.lwjgl.glfw.GLFW.GLFW_KEY_S;
import static org.lwjgl.glfw.GLFW.GLFW_KEY_UP;
import static org.lwjgl.glfw.GLFW.GLFW_KEY_Z;
import static org.lwjgl.glfw.GLFW.GLFW_PRESS;
import static org.lwjgl.glfw.GLFW.GLFW_RELEASE;
import static org.lwjgl.glfw.GLFW.GLFW_RESIZABLE;
import static org.lwjgl.glfw.GLFW.GLFW_TRUE;
import static org.lwjgl.glfw.GLFW.GLFW_VISIBLE;
import static org.lwjgl.glfw.GLFW.glfwCreateWindow;
import static org.lwjgl.glfw.GLFW.glfwDefaultWindowHints;
import static org.lwjgl.glfw.GLFW.glfwGetPrimaryMonitor;
import static org.lwjgl.glfw.GLFW.glfwGetVideoMode;
import static org.lwjgl.glfw.GLFW.glfwInit;
import static org.lwjgl.glfw.GLFW.glfwMakeContextCurrent;
import static org.lwjgl.glfw.GLFW.glfwPollEvents;
import static org.lwjgl.glfw.GLFW.glfwSetErrorCallback;
import static org.lwjgl.glfw.GLFW.glfwSetKeyCallback;
import static org.lwjgl.glfw.GLFW.glfwSetWindowPos;
import static org.lwjgl.glfw.GLFW.glfwSetWindowShouldClose;
import static org.lwjgl.glfw.GLFW.glfwShowWindow;
import static org.lwjgl.glfw.GLFW.glfwSwapBuffers;
import static org.lwjgl.glfw.GLFW.glfwSwapInterval;
import static org.lwjgl.glfw.GLFW.glfwTerminate;
import static org.lwjgl.glfw.GLFW.glfwWindowHint;
import static org.lwjgl.glfw.GLFW.glfwWindowShouldClose;
import static org.lwjgl.opengl.ARBVertexBufferObject.GL_ARRAY_BUFFER_ARB;
import static org.lwjgl.opengl.ARBVertexBufferObject.GL_ELEMENT_ARRAY_BUFFER_ARB;
import static org.lwjgl.opengl.ARBVertexBufferObject.GL_STATIC_DRAW_ARB;
import static org.lwjgl.opengl.ARBVertexBufferObject.glBindBufferARB;
import static org.lwjgl.opengl.ARBVertexBufferObject.glBufferDataARB;
import static org.lwjgl.opengl.ARBVertexBufferObject.glGenBuffersARB;
import static org.lwjgl.opengl.ARBVertexBufferObject.nglBufferDataARB;
import static org.lwjgl.opengl.GL11.glClearColor;
import static org.lwjgl.system.MemoryUtil.NULL;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.ArrayList;
import java.util.List;

import org.lwjgl.BufferUtils;
import org.lwjgl.PointerBuffer;
import org.lwjgl.assimp.AIColor4D;
import org.lwjgl.assimp.AIFace;
import org.lwjgl.assimp.AIMaterial;
import org.lwjgl.assimp.AIMesh;
import org.lwjgl.assimp.AIScene;
import org.lwjgl.assimp.AIVector3D;
import org.lwjgl.assimp.Assimp;
import org.lwjgl.glfw.GLFWErrorCallback;
import org.lwjgl.glfw.GLFWKeyCallback;
import org.lwjgl.glfw.GLFWVidMode;
import org.lwjgl.opengl.ARBFragmentShader;
import org.lwjgl.opengl.ARBShaderObjects;
import org.lwjgl.opengl.ARBVertexBufferObject;
import org.lwjgl.opengl.ARBVertexShader;
import org.lwjgl.opengl.GL;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GLCapabilities;

import de.matthiasmann.twl.utils.PNGDecoder;
import de.matthiasmann.twl.utils.PNGDecoder.Format;

public class Cube {
	/* Attributes */
	//Window
	private long window;
		
	private boolean do_run = true; // runs until done is set to true
	private int display_width = 640;
	private int display_height = 640;
		
	private int vertex_vbo_id = 0;
	private int color_vbo_id = 0;
	private int index_vbo_id = 0;
	private int nb_indices = 0;
	private boolean use_vbo = false; 
	private boolean use_shader = false; 
	 
	private int program;
	private int vertexShader;
	private int fragmentShader;    
	 
	private Model model;
	//Pour l'animation
	private int vx;
	private int vy;
	private int vz;
	 
	private int rotationX_cam = 0;
	private int rotationY_cam = 0;
	
	//Border
	private boolean borderDraw;
	private GLFWKeyCallback keyCallback;
	static GLCapabilities glcaps;

	//Constructor
	public Cube() {
	}
	
	/* ------------------------------- GRAPHICS	---------------------- */
	
	//Running the program
	public void run() {
		//Initializations
		initDisplay();
		initGL();
		initShaders();
		setFirstView();
		loadModel("data/StormTrooper/StormTrooper.obj");
		//Binding the keyboard
		keyPressed();
		while(do_run) {
			if( glfwWindowShouldClose(window)) do_run = false;
			//On efface tout
			GL11.glClear(GL11.GL_COLOR_BUFFER_BIT| GL11.GL_DEPTH_BUFFER_BIT);
			
			render();
			glfwSwapBuffers(window);
			glfwPollEvents();
		}
		
		//Terminate GLFW and free the error callback
		glfwTerminate();
		glfwSetErrorCallback(null).free();
	}
	
	//Rendering in the window
	private void render() {
		GL11.glRotatef(vx,1,0,0);
		GL11.glRotatef(vy,0,1,0);
		GL11.glRotatef(vz,0,0,1);
		draw_axis();
		GL11.glColor3f(1f, 1f,1f);
		for (Model.Mesh mesh : model.meshes) {
            glBindBufferARB(GL_ARRAY_BUFFER_ARB, mesh.vertexArrayBuffer);
            glBindBufferARB(GL_ARRAY_BUFFER_ARB, mesh.normalArrayBuffer);
            
            Model.Material material = model.materials.get(mesh.mesh.mMaterialIndex());
            
            glBindBufferARB(GL_ELEMENT_ARRAY_BUFFER_ARB, mesh.elementArrayBuffer);
            GL11.glDrawElements(GL11.GL_TRIANGLES, mesh.elementCount, GL11.GL_UNSIGNED_INT, 0);
        }

	}

	private void initGL() {
		//Options
		GL11.glShadeModel(GL11.GL_SMOOTH);
		GL11.glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
		GL11.glClearDepth(1.0f);
		GL11.glEnable(GL11.GL_DEPTH_TEST);
		
		GL11.glDepthFunc(GL11.GL_LEQUAL);
		GL11.glEnable(GL11.GL_BLEND);
		GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
		GL11.glHint(GL11.GL_PERSPECTIVE_CORRECTION_HINT, GL11.GL_NICEST);
		
		GL11.glEnable(GL11.GL_TEXTURE_2D);
		GL11.glEnable(GL11.GL_COLOR_MATERIAL);
		GL11.glColorMaterial(GL11.GL_FRONT_AND_BACK, GL11.GL_AMBIENT_AND_DIFFUSE);
		
		float mat_specular[] = { 1f,1f,1f,1f };
		float ambient[] = {0.35f,0.35f,0.35f,1f};
		float mat_shininess[] = { 20.0f };
		float light_position[] = { 1.0f, 5.0f, 0.0f, 0.0f };
		GL11.glEnable(GL11.GL_LIGHTING);
		GL11.glLightModelfv(GL11.GL_LIGHT_MODEL_AMBIENT, ambient);
		GL11.glLightfv(GL11.GL_LIGHT0,GL11.GL_DIFFUSE, mat_specular);
		GL11.glEnable(GL11.GL_LIGHT0);

	}
	
	private void setFirstView() {
		//Definition de la fenetre
		GL11.glViewport(0, 0, display_width, display_height);
		//Definition de la vue
		GL11.glMatrixMode(GL11.GL_PROJECTION);
		GL11.glLoadIdentity();
		//Using an orthogonal view
		GL11.glOrtho(-1.5f, 2f, -1.5f, 2f, -10f, 10f);
		GL11.glRotated(45,0,1,0);
		GL11.glRotated(25,1,0,0);
		GL11.glRotated(30,0,0,1);
	}
	
	private void setSecondView() {
		GL11.glViewport(display_width/2, 0, display_width/2, display_height);
		GL11.glMatrixMode(GL11.GL_PROJECTION);
		GL11.glLoadIdentity();
		//Using an orthogonal view
		GL11.glOrtho(-1.5f, 2f, -1.5f, 2f, -10f, 10f);
		GL11.glRotated(-45,0,1,0);
		GL11.glRotated(-45,1,0,0);
		GL11.glRotated(-20,0,0,1);
	}

	
	
	//KeyBoard Actions
	private void keyPressed() {
		glfwSetKeyCallback(window, new GLFWKeyCallback() {
			@Override
            public void invoke(long window, int key, int scancode, int action, int mods) {
				if ( key == GLFW_KEY_RIGHT ) {
                	if (action == GLFW_PRESS) vy = 1;
                	if (action == GLFW_RELEASE) vy = 0;
                }
                if ( key == GLFW_KEY_LEFT ) {
	                if (action == GLFW_PRESS) vy = -1;
	                if (action == GLFW_RELEASE) vy = 0;
                }
	            if ( key == GLFW_KEY_UP ) {
	               	if (action == GLFW_PRESS) vz = 1;
	                if (action == GLFW_RELEASE) vz = 0;
	            }
	            if ( key == GLFW_KEY_DOWN ) {
		            if (action == GLFW_PRESS) vz = -1;
		            if (action == GLFW_RELEASE) vz = 0;
	            }
				if ( key == GLFW_KEY_ESCAPE && action == GLFW_RELEASE )
					glfwSetWindowShouldClose(window, true); 
				if ( key == GLFW_KEY_B && action == GLFW_PRESS) {
					borderDraw = !borderDraw;
				}
				if ( key == GLFW_KEY_D) rotationX_cam +=2;
				if ( key == GLFW_KEY_Q) rotationX_cam -=2;
				if ( key == GLFW_KEY_Z) rotationY_cam +=2;
				if ( key == GLFW_KEY_S) rotationY_cam -=2;
			}
		});
	}
	
	/* --------------------- DRAWING FUNCTIONS --------------------- */
	//Points
	private void draw_point() {
		GL11.glBegin(GL11.GL_POINTS);
		GL11.glColor3f(1.0f, 0.0f, 0.0f); 
		GL11.glVertex3f(1.5f,1.5f,1.5f);
		GL11.glEnd();
		//Y axis
		GL11.glBegin(GL11.GL_POINTS);
		GL11.glColor3f(0.0f,1.0f, 0.0f); 
		GL11.glVertex3f(0f, 0f, 0f);
		GL11.glVertex3f(0f,1.5f,0f);
		GL11.glEnd();

	}
	//Axis
	private void draw_axis() {
		//X axis
		GL11.glBegin(GL11.GL_LINES);
		GL11.glColor3f(1.0f, 0.0f, 0.0f); 
		GL11.glVertex3f(0f, 0f, 0f);
		GL11.glVertex3f(1.5f,0f,0f);
		GL11.glEnd();
		//Y axis
		GL11.glBegin(GL11.GL_LINES);
		GL11.glColor3f(0.0f,1.0f, 0.0f); 
		GL11.glVertex3f(0f, 0f, 0f);
		GL11.glVertex3f(0f,1.5f,0f);
		GL11.glEnd();
		//Z axis
		GL11.glBegin(GL11.GL_LINES);
		GL11.glColor3f(0f,0f,1f); 
		GL11.glVertex3f(0f, 0f, 0f);
		GL11.glVertex3f(0f,0f,1.5f);
		GL11.glEnd();
	}
	//Triangle
	private void draw_triangle(float w, float h, float d)
    {
        w/=2;
        h/=2;
        d/=2;
        GL11.glBegin(GL11.GL_TRIANGLES);
        GL11.glNormal3f(0f,0f,1f);
        GL11.glVertex3f(w, h, d);
        GL11.glVertex3f(0, -h, d);
        GL11.glVertex3f(-w, h, d);
        GL11.glEnd();
    }
	
	//Rectangle
	private void draw_quad(float d, float h, float r)
	 {
		 d/=2;
		 h/=2;
	     GL11.glBegin(GL11.GL_QUADS);
	     GL11.glNormal3f(0f,0f,1f);
	     GL11.glVertex3f(-d, h, r);
	     GL11.glVertex3f(d, h, r);
	     GL11.glVertex3f(d, -h, r);
	     GL11.glVertex3f(-d,-h,r);
	     GL11.glEnd();
	 }
	//Square
	private void draw_square(float d, float r)
	 {
		Texture tex = new Texture("data/texture.png");
		tex.bind();
		d/=2;
	    GL11.glBegin(GL11.GL_QUADS);
	    GL11.glNormal3f(0f,0f,1f);
	    GL11.glTexCoord2f(0, 0);
	    GL11.glVertex3f(-d, d, r);
	    GL11.glTexCoord2f(0, 1);
	    GL11.glVertex3f(d, d, r);
	    GL11.glTexCoord2f(1, 1);
	    GL11.glVertex3f(d, -d, r);
	    GL11.glTexCoord2f(1, 0);
	    GL11.glVertex3f(-d,-d,r);
	    GL11.glEnd();
	    tex.unbind();
	 }
	//Square with Triangle_Fan
	private void draw_square_fan(float d, float r)
	 {
		 d/=2;
		 GL11.glBegin(GL11.GL_TRIANGLE_FAN);
		 GL11.glNormal3f(0f,0f,1f);
	     GL11.glVertex3f(d, -d, r);
		 GL11.glVertex3f(d, d, r);
		 GL11.glVertex3f(-d, d, r);
	     GL11.glVertex3f(-d,-d,r);
	     
	     GL11.glEnd();
	 }
	//Square with Triangle_Strip
	private void draw_square_strip(float d, float r)
	 {
		 d/=2;
		 GL11.glBegin(GL11.GL_TRIANGLE_STRIP);
		 GL11.glNormal3f(0f,0f,1f);
		 GL11.glVertex3f(-d, d, r);
		 GL11.glVertex3f(-d, -d, r);
	     GL11.glVertex3f(d, d, r);
	     GL11.glVertex3f(d,-d,r);
	     
	     GL11.glEnd();
	 }
	
	private void draw_cube(float d)
	 {
		Texture up = new Texture("data/up.png");
		Texture front = new Texture("data/front.png");
		Texture left = new Texture("data/left.png");
        d/=2;
		 //face 1 (rear) Color red
		 GL11.glBegin(GL11.GL_QUADS);
		 GL11.glNormal3f(0f,1f,0f);
		 GL11.glColor3f(1f, 1f, 1f);
		 GL11.glVertex3f(-d, d, -d); 
		 GL11.glVertex3f(d, d, -d); 
	     GL11.glVertex3f(d, -d, -d); 
	     GL11.glVertex3f(-d,-d,-d); 
	     GL11.glEnd();
	     //face 2 (left)
	     left.bind();
	     GL11.glBegin(GL11.GL_QUADS);
		 GL11.glNormal3f(-1f,0f,0f);
		 //GL11.glColor3f(1f, 1f, 0.2f);
		 GL11.glTexCoord2f(0,0);
		 GL11.glVertex3f(-d,d , -d); 
		 GL11.glTexCoord2f(0,1);
		 GL11.glVertex3f(-d, -d, -d);
		 GL11.glTexCoord2f(1,1);
	     GL11.glVertex3f(-d, -d, +d); 
	     GL11.glTexCoord2f(1,0);
	     GL11.glVertex3f(-d,d,+d);     
	     GL11.glEnd();
	     left.unbind();
	     //face 3 (right)
	     GL11.glBegin(GL11.GL_QUADS);
		 GL11.glNormal3f(1f,0f,0f);
		 GL11.glColor3f(0.6f, 0.1f, 0.6f);
		 GL11.glVertex3f(d,-d , -d); 
		 GL11.glVertex3f(d, d, -d); 
	     GL11.glVertex3f(d, d, +d); 
	     GL11.glVertex3f(d,-d,+d);     
	     GL11.glEnd();
	     //face 4 (front)
	     front.bind();
		 GL11.glBegin(GL11.GL_QUADS);
		 GL11.glNormal3f(0f,0f,1f);
		 //GL11.glColor3f(1f, 0.1f, 0.2f);
		 GL11.glTexCoord2f(0, 0);
		 GL11.glVertex3f(-d, d, +d);
		 GL11.glTexCoord2f(0, 1);
		 GL11.glVertex3f(d, d, +d);
		 GL11.glTexCoord2f(1, 1);
	     GL11.glVertex3f(d, -d, +d);
	     GL11.glTexCoord2f(1, 0);
	     GL11.glVertex3f(-d,-d,+d); 
	     GL11.glEnd();
	     front.unbind();
	     //face 5 (up)
	     up.bind();
		 GL11.glBegin(GL11.GL_QUADS);
		 GL11.glNormal3f(0f,1f,0.0f);
		 //GL11.glColor3f(0f, 0.4f, 0.8f);
		 GL11.glTexCoord2f(0, 0);
		 GL11.glVertex3f(-d, d, -d); 
		 GL11.glTexCoord2f(0, 1);
		 GL11.glVertex3f(-d, d, +d); 
		 GL11.glTexCoord2f(1, 1);
	     GL11.glVertex3f(d, d, +d); 
	     GL11.glTexCoord2f(1, 0);
	     GL11.glVertex3f(d,d,-d); 
	     GL11.glEnd();
	     up.unbind();
	     //face 6
		 GL11.glBegin(GL11.GL_QUADS);
		 GL11.glNormal3f(0f,-1f,0.0f);
		 GL11.glColor3f(0.1f, 0.9f, 0.2f);
		 GL11.glVertex3f(-d, -d, -d); 
		 GL11.glVertex3f(-d, -d, +d); 
	     GL11.glVertex3f(d, -d, +d); 
	     GL11.glVertex3f(d,-d,-d); 
	     GL11.glEnd();
	}
	
	private void draw_cubeNoTexture(float d)
	 {
       d/=2;
		 //face 1 (rear) Color red
		 GL11.glBegin(GL11.GL_QUADS);
		 GL11.glNormal3f(0f,1f,0f);
		 GL11.glColor3f(1f, 1f, 1f);
		 GL11.glVertex3f(-d, d, -d); 
		 GL11.glVertex3f(d, d, -d); 
	     GL11.glVertex3f(d, -d, -d); 
	     GL11.glVertex3f(-d,-d,-d); 
	     GL11.glEnd();
	     //face 2 (left)
	     GL11.glBegin(GL11.GL_QUADS);
		 GL11.glNormal3f(-1f,0f,0f);
		 GL11.glColor3f(1f, 1f, 0.2f);
		 GL11.glTexCoord2f(0,0);
		 GL11.glVertex3f(-d,d , -d); 
		 GL11.glTexCoord2f(0,1);
		 GL11.glVertex3f(-d, -d, -d);
		 GL11.glTexCoord2f(1,1);
	     GL11.glVertex3f(-d, -d, +d); 
	     GL11.glTexCoord2f(1,0);
	     GL11.glVertex3f(-d,d,+d);     
	     GL11.glEnd();
	     //face 3 (right)
	     GL11.glBegin(GL11.GL_QUADS);
		 GL11.glNormal3f(1f,0f,0f);
		 GL11.glColor3f(0.6f, 0.1f, 0.6f);
		 GL11.glVertex3f(d,-d , -d); 
		 GL11.glVertex3f(d, d, -d); 
	     GL11.glVertex3f(d, d, +d); 
	     GL11.glVertex3f(d,-d,+d);     
	     GL11.glEnd();
	     //face 4 (front)
		 GL11.glBegin(GL11.GL_QUADS);
		 GL11.glNormal3f(0f,0f,1f);
		 GL11.glColor3f(1f, 0.1f, 0.2f);
		 GL11.glTexCoord2f(0, 0);
		 GL11.glVertex3f(-d, d, +d);
		 GL11.glTexCoord2f(0, 1);
		 GL11.glVertex3f(d, d, +d);
		 GL11.glTexCoord2f(1, 1);
	     GL11.glVertex3f(d, -d, +d);
	     GL11.glTexCoord2f(1, 0);
	     GL11.glVertex3f(-d,-d,+d); 
	     GL11.glEnd();
	     //face 5 (up)
		 GL11.glBegin(GL11.GL_QUADS);
		 GL11.glNormal3f(0f,1f,0.0f);
		 GL11.glColor3f(0f, 0.4f, 0.8f);
		 GL11.glTexCoord2f(0, 0);
		 GL11.glVertex3f(-d, d, -d); 
		 GL11.glTexCoord2f(0, 1);
		 GL11.glVertex3f(-d, d, +d); 
		 GL11.glTexCoord2f(1, 1);
	     GL11.glVertex3f(d, d, +d); 
	     GL11.glTexCoord2f(1, 0);
	     GL11.glVertex3f(d,d,-d); 
	     GL11.glEnd();
	     //face 6
		 GL11.glBegin(GL11.GL_QUADS);
		 GL11.glNormal3f(0f,-1f,0.0f);
		 GL11.glColor3f(0.1f, 0.9f, 0.2f);
		 GL11.glVertex3f(-d, -d, -d); 
		 GL11.glVertex3f(-d, -d, +d); 
	     GL11.glVertex3f(d, -d, +d); 
	     GL11.glVertex3f(d,-d,-d); 
	     GL11.glEnd();
	}
	
	//Draw borders
	private void draw_border(float d) {
		d/=2;
		//face1
		GL11.glBegin(GL11.GL_LINES);
		GL11.glVertex3f(-d,-d, -d);
		GL11.glVertex3f(-d,-d, d);
		GL11.glVertex3f(d,-d, d);
		GL11.glVertex3f(d,-d, -d);
		GL11.glVertex3f(-d,-d, -d);
		//face2
		GL11.glVertex3f(-d,d, -d);
		GL11.glVertex3f(d,d, -d);
		GL11.glVertex3f(d,-d, -d);
		GL11.glEnd();
		//face3
		GL11.glBegin(GL11.GL_LINES);
		GL11.glVertex3f(d,d, -d);
		GL11.glVertex3f(d,d, d);
		GL11.glVertex3f(d,-d, d);
		GL11.glEnd();
	}
	
	//Draw a sphere
	private void draw_sphere(float r, int n)
	 {
		 GL11.glBegin(GL11.GL_QUADS);
		 for (int i=0; i<n; i++)
		 {
			 for (int j=0;j<n;j++)
			 {
				 double phi1 = (Math.PI/2)*((double)2*j/n -1);
				 double psi1 = Math.PI*((double)2*i/n -1);
				 double phi2 = (Math.PI/2)*((double)2*(j+1)/n -1);
				 double psi2 = Math.PI*((double)2*(i+1)/n -1);
				 double x0 = r*(Math.cos(phi1))*(Math.cos(psi1));
				 double x1 = r*(Math.cos(phi1))*(Math.cos(psi2));
				 double x2 =r*(Math.cos(phi2))*(Math.cos(psi1));
				 double x3 =r*(Math.cos(phi2))*(Math.cos(psi2));
				 double z0 = r*(Math.cos(phi1))*(Math.sin(psi1));
				 double z1 = r*(Math.cos(phi1))*(Math.sin(psi2));
				 double z2 = r*(Math.cos(phi2))*(Math.sin(psi1));
				 double z3 = r*(Math.cos(phi2))*(Math.sin(psi2));
				 double y0 = r*(Math.sin(phi1));
				 double y1 = r*(Math.sin(phi2));
				 
				 GL11.glColor3f((float)(x0+x1+x2+x3)/4, (float)(y0+y1)/2,(float)(z1+z2)/2);
				 GL11.glVertex3d(x0,y0,z0);
				 GL11.glVertex3d(x1,y0,z1);
				 GL11.glVertex3d(x3,y1,z3);
				 GL11.glVertex3d(x2,y1,z2);
			 }
		 }
		 GL11.glEnd();
	 }

	
	//Import assimp
	
	public void loadModel(String filename){
        // Assimp will be able to find the corresponding mtl file if we call aiImportFile this way.
		AIScene scene = Assimp.aiImportFile(filename, Assimp.aiProcess_JoinIdenticalVertices | Assimp.aiProcess_Triangulate);
        if (scene == null) {
        	System.out.println("Vide");
            //throw new IllegalStateException(Assimp.aiGetErrorString());
        }
        model = new Model(scene);
	}
	
	
	public int load_assimp(FloatBuffer buf) {
		buf.flip();
      //Create a vbo in the GPU's memory
        int vbo_id = createVBOID();
        
        //and fill it with the data
    		if (glcaps.GL_ARB_vertex_buffer_object) {
          ARBVertexBufferObject.glBindBufferARB(ARBVertexBufferObject.GL_ARRAY_BUFFER_ARB, vbo_id);
          ARBVertexBufferObject.glBufferDataARB(ARBVertexBufferObject.GL_ARRAY_BUFFER_ARB,buf, ARBVertexBufferObject.GL_STATIC_DRAW_ARB);
        } else {
          System.out.println("VBOs NOT SUPPORTED !!");
        }
    	vertex_vbo_id = vbo_id;
    	//create a VBO containing the indexes of vertex
  	    int[] indices = new int[] {0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19};
  	    index_vbo_id = load_index_vbo(indices);
        return vbo_id;
      }
       
	/* --------------------- VBO MANIPULATION ------------------------ */
	
	//Triangle
	private void draw_triangle_vbo(float w, float h, float d)
    {
        load_triangle_vbo(w, h, d);      
        draw_vbos();
    }
	private void load_triangle_vbo(float w, float h, float d) {
	      //VBO already created
	      if (vertex_vbo_id != 0) return;
	      //Create a VBO with the 3 vertex
	      w/=2;
	      h/=2;
	      d/=2;
	      float[] vertices = new float[] {
	        -w, h, d,
	        w, h, d,
	        w, -h, d,
	      };
	      vertex_vbo_id = load_float_vbo(vertices);
	      //create a VBO containing the indexes of vertex
	      int[] indices = new int[]
	      {
	        0,1,2
	      };
	      index_vbo_id = load_index_vbo(indices);
	      System.out.println("VBOs Setup - Vertex ID " + vertex_vbo_id + " - index ID " + index_vbo_id + " - nb indices " + nb_indices);
	    }
	
	//Square
	private void draw_square_vbo(float d, float r)
    {
        load_square_vbo(d, r);
        draw_vbos();
    }
	private void load_square_vbo(float d, float r) {
	      if (vertex_vbo_id != 0) return;
	      d/=2;
	      float[] vertices = new float[] {
	        -d, d, r,
	        d, d, r,
	        d, -d, r,
	        -d, -d, r,
	      };
	      vertex_vbo_id = load_float_vbo(vertices);
	      int[] indices = new int[]
	      {
	        0,1,2,3
	      };
	      index_vbo_id = load_index_vbo(indices);
	      System.out.println("VBOs Setup - Vertex ID " + vertex_vbo_id + " - index ID " + index_vbo_id + " - nb indices " + nb_indices);
	    }

	
	/* -------------------------- Given functions ---------------------------- */
	private static String getLogInfo(int obj) {
        return ARBShaderObjects.glGetInfoLogARB(obj, ARBShaderObjects.glGetObjectParameteriARB(obj, ARBShaderObjects.GL_OBJECT_INFO_LOG_LENGTH_ARB));
    }
	public static int createVBOID() {
	      if (glcaps.GL_ARB_vertex_buffer_object) {
	        IntBuffer buffer = BufferUtils.createIntBuffer(1);
	        ARBVertexBufferObject.glGenBuffersARB(buffer);
	        return buffer.get(0);
	      }
	      return 0;
	    }
	private int load_float_vbo(float [] values)
    {
      //load into a FloatBuffer
  		FloatBuffer verticesBuffer = BufferUtils.createFloatBuffer(values.length);
	   	verticesBuffer.put(values);
	   	verticesBuffer.flip(); //NE PAS OUBLIER CETTE LIGNE!! ELLE PEUT CRASHER VOTRE JavaVM
      
      //Create a vbo in the GPU's memory
      int vbo_id = createVBOID();
      
      //and fill it with the data
  		if (glcaps.GL_ARB_vertex_buffer_object) {
        ARBVertexBufferObject.glBindBufferARB(ARBVertexBufferObject.GL_ARRAY_BUFFER_ARB, vbo_id);
        ARBVertexBufferObject.glBufferDataARB(ARBVertexBufferObject.GL_ARRAY_BUFFER_ARB, verticesBuffer, ARBVertexBufferObject.GL_STATIC_DRAW_ARB);
      } else {
        System.out.println("VBOs NOT SUPPORTED !!");
      }
      return vbo_id;
    }
    
	
	
    private int load_index_vbo(int [] values)
    {
      //load data in an IntBuffer
  		IntBuffer indicesBuffer = BufferUtils.createIntBuffer(values.length);
	   	indicesBuffer.put(values);
	   	indicesBuffer.flip(); //NE PAS OUBLIER CETTE LIGNE!! ELLE PEUT CRASHER VOTRE JavaVM
      
      //create a vbo in the GPU's memory
      int vbo_id = createVBOID();
      
      //and fill it with the data
  		if (glcaps.GL_ARB_vertex_buffer_object) {
        ARBVertexBufferObject.glBindBufferARB(ARBVertexBufferObject.GL_ELEMENT_ARRAY_BUFFER_ARB, vbo_id);
        ARBVertexBufferObject.glBufferDataARB(ARBVertexBufferObject.GL_ELEMENT_ARRAY_BUFFER_ARB, indicesBuffer, ARBVertexBufferObject.GL_STATIC_DRAW_ARB);
      } else {
        System.out.println("VBOs NOT SUPPORTED !!");
      }
      
      //remember the number of values
      nb_indices = values.length;      
      return vbo_id;
    }
    private void draw_vbos() {
	      if (vertex_vbo_id==0) return;
	      if (index_vbo_id==0) return;
	      
	      if (use_shader) {
	        ARBShaderObjects.glUseProgramObjectARB(program);
	      }
	      
	      GL11.glEnableClientState(GL11.GL_VERTEX_ARRAY);
	      ARBVertexBufferObject.glBindBufferARB(ARBVertexBufferObject.GL_ARRAY_BUFFER_ARB, vertex_vbo_id);
	      GL11.glVertexPointer(3, GL11.GL_FLOAT, 0, 0);

	      if (color_vbo_id != 0) {
	        GL11.glEnableClientState(GL11.GL_COLOR_ARRAY);
	        ARBVertexBufferObject.glBindBufferARB(ARBVertexBufferObject.GL_ARRAY_BUFFER_ARB, color_vbo_id);
	        GL11.glColorPointer(4, GL11.GL_FLOAT, 0, 0);
	      }

	      
	      ARBVertexBufferObject.glBindBufferARB(ARBVertexBufferObject.GL_ELEMENT_ARRAY_BUFFER_ARB, index_vbo_id);
	  		
	      GL11.glDrawElements(GL11.GL_TRIANGLES, nb_indices, GL11.GL_UNSIGNED_INT, 0);

	      GL11.glDisableClientState(GL11.GL_VERTEX_ARRAY);
	      if (color_vbo_id != 0) GL11.glDisableClientState(GL11.GL_COLOR_ARRAY);

	      if (use_shader) {
	        ARBShaderObjects.glUseProgramObjectARB(0);
	      }
	    }
	//Initialize shader
    private void initShaders() {

		if (! use_shader) return;

        /*initialize openGL shader*/
        program = ARBShaderObjects.glCreateProgramObjectARB();
        if (program==0) {
            System.out.println("Error: OpenGL shaders not supported");
            System.exit(0);
        }
        vertexShader=ARBShaderObjects.glCreateShaderObjectARB(ARBVertexShader.GL_VERTEX_SHADER_ARB);
        String vertexCode=""+
        "void main(void)" + 
        "{" + 
        "	gl_Position = gl_ModelViewProjectionMatrix * gl_Vertex;" +  
        "}";
        
        ARBShaderObjects.glShaderSourceARB(vertexShader, vertexCode);
        ARBShaderObjects.glCompileShaderARB(vertexShader);
        if (ARBShaderObjects.glGetObjectParameteriARB(vertexShader, ARBShaderObjects.GL_OBJECT_COMPILE_STATUS_ARB) == GL11.GL_FALSE)
        {
            System.out.println("Vertex shader not compiled: " + getLogInfo(vertexShader) );
        }
        
        fragmentShader=ARBShaderObjects.glCreateShaderObjectARB(ARBFragmentShader.GL_FRAGMENT_SHADER_ARB);
        String fragmentCode="" +
        "void main(void) {" +
        "	gl_FragColor = vec4 (0.0, 1.0, 0.0, 1.0);" +
        "}";
        
        ARBShaderObjects.glShaderSourceARB(fragmentShader, fragmentCode);
        ARBShaderObjects.glCompileShaderARB(fragmentShader);
        if (ARBShaderObjects.glGetObjectParameteriARB(fragmentShader, ARBShaderObjects.GL_OBJECT_COMPILE_STATUS_ARB) == GL11.GL_FALSE)
        {
            System.out.println("Fragment shader not compiled: " + getLogInfo(fragmentShader) );
        }
        
        
        ARBShaderObjects.glAttachObjectARB(program, vertexShader);
        ARBShaderObjects.glAttachObjectARB(program, fragmentShader);
        ARBShaderObjects.glLinkProgramARB(program);
        ARBShaderObjects.glValidateProgramARB(program);

        System.out.println("Shaders initialized !" );
    }
	//Display initialization
	private void initDisplay(){
		//taken from lwjgl https://www.lwjgl.org/guide
        try{

		// Setup an error callback. The default implementation
		// will print the error message in System.err.
		GLFWErrorCallback.createPrint(System.err).set();

		// Initialize GLFW. Most GLFW functions will not work before doing this.
		if ( !glfwInit() )
			throw new IllegalStateException("Unable to initialize GLFW");

		// Configure our window
		glfwDefaultWindowHints(); // optional, the current window hints are already the default
		glfwWindowHint(GLFW_VISIBLE, GLFW_FALSE); // the window will stay hidden after creation
		glfwWindowHint(GLFW_RESIZABLE, GLFW_TRUE); // the window will be resizable

		// Create the window
		window = glfwCreateWindow( display_width,  display_height, "Hall� PACT!", NULL, NULL);
		if ( window == NULL )
			throw new RuntimeException("Failed to create the GLFW window");

		// Setup a key callback. It will be called every time a key is pressed, repeated or released.
		glfwSetKeyCallback(window, (window, key, scancode, action, mods) -> {
			if ( key == GLFW_KEY_ESCAPE && action == GLFW_RELEASE )
				glfwSetWindowShouldClose(window, true); // We will detect this in our rendering loop
		});

		// Get the resolution of the primary monitor
		GLFWVidMode vidmode = glfwGetVideoMode(glfwGetPrimaryMonitor());

		glfwSetWindowPos(window, 200, 200);

		// Make the OpenGL context current
		glfwMakeContextCurrent(window);
		// Enable v-sync
		glfwSwapInterval(1);

		// Make the window visible
		glfwShowWindow(window);

		glcaps = GL.createCapabilities();

		glClearColor(1.0f, 0.0f, 0.0f, 0.0f);

        }catch(Exception e){
            System.out.println("Error setting up display: "+ e.getMessage());
            System.exit(0);
        }
    }	
	
	//Buffers 
	private static FloatBuffer makeFloatBuffer(float[] values)
	{
		FloatBuffer buffer = BufferUtils.createFloatBuffer(values.length);
		buffer.put(values);
		buffer.flip();
		return buffer;
	}
	
	// Main
	public static void main(String[] args) {
		Cube app = new Cube();
        app.run();
	}
	
	
	
	///////////////////////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////////
	
	static class Model {

        public AIScene scene;
        public List<Mesh> meshes;
        public List<Material> materials;

        public Model(AIScene scene) {

            this.scene = scene;

            int meshCount = scene.mNumMeshes();
            PointerBuffer meshesBuffer = scene.mMeshes();
            meshes = new ArrayList<>();
            for (int i = 0; i < meshCount; ++i) {
                meshes.add(new Mesh(AIMesh.create(meshesBuffer.get(i))));
            }

            int materialCount = scene.mNumMaterials();
            PointerBuffer materialsBuffer = scene.mMaterials();
            materials = new ArrayList<>();
            for (int i = 0; i < materialCount; ++i) {
                materials.add(new Material(AIMaterial.create(materialsBuffer.get(i))));
            }
        }

        public void free() {
            aiReleaseImport(scene);
            scene = null;
            meshes = null;
            materials = null;
        }

        public static class Mesh {

            public AIMesh mesh;
            public int vertexArrayBuffer;
            public int normalArrayBuffer;
            public int elementArrayBuffer;
            public int elementCount;

            public Mesh(AIMesh mesh) {
                this.mesh = mesh;

                vertexArrayBuffer = glGenBuffersARB();
                glBindBufferARB(GL_ARRAY_BUFFER_ARB, vertexArrayBuffer);
                AIVector3D.Buffer vertices = mesh.mVertices();
                nglBufferDataARB(GL_ARRAY_BUFFER_ARB, AIVector3D.SIZEOF * vertices.remaining(),
                        vertices.address(), GL_STATIC_DRAW_ARB);

                normalArrayBuffer = glGenBuffersARB();
                glBindBufferARB(GL_ARRAY_BUFFER_ARB, normalArrayBuffer);
                /*AIVector3D.Buffer normals = mesh.mNormals();
                nglBufferDataARB(GL_ARRAY_BUFFER_ARB, AIVector3D.SIZEOF * normals.remaining(),
                        normals.address(), GL_STATIC_DRAW_ARB);*/

                int faceCount = mesh.mNumFaces();
                elementCount = faceCount * 3;
                IntBuffer elementArrayBufferData = BufferUtils.createIntBuffer(elementCount);
                AIFace.Buffer facesBuffer = mesh.mFaces();
                for (int i = 0; i < faceCount; ++i) {
                    AIFace face = facesBuffer.get(i);
                    if (face.mNumIndices() != 3) {
                        throw new IllegalStateException("AIFace.mNumIndices() != 3");
                    }
                    elementArrayBufferData.put(face.mIndices());
                }
                elementArrayBufferData.flip();
                elementArrayBuffer = glGenBuffersARB();
                glBindBufferARB(GL_ELEMENT_ARRAY_BUFFER_ARB, elementArrayBuffer);
                glBufferDataARB(GL_ELEMENT_ARRAY_BUFFER_ARB, elementArrayBufferData,
                        GL_STATIC_DRAW_ARB);
            }
        }

        public static class Material {

            public AIMaterial mMaterial;
            public AIColor4D mAmbientColor;
            public AIColor4D mDiffuseColor;
            public AIColor4D mSpecularColor;

            public Material(AIMaterial material) {

                mMaterial = material;

                mAmbientColor = AIColor4D.create();
                if (aiGetMaterialColor(mMaterial, AI_MATKEY_COLOR_AMBIENT,
                        aiTextureType_NONE, 0, mAmbientColor) != 0) {
                    throw new IllegalStateException(aiGetErrorString());
                }
                mDiffuseColor = AIColor4D.create();
                if (aiGetMaterialColor(mMaterial, AI_MATKEY_COLOR_DIFFUSE,
                        aiTextureType_NONE, 0, mDiffuseColor) != 0) {
                    throw new IllegalStateException(aiGetErrorString());
                }
                mSpecularColor = AIColor4D.create();
                if (aiGetMaterialColor(mMaterial, AI_MATKEY_COLOR_SPECULAR,
                        aiTextureType_NONE, 0, mSpecularColor) != 0) {
                    throw new IllegalStateException(aiGetErrorString());
                }
            }
        }
    }
}


