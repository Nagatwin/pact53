package exercices;

import static org.lwjgl.glfw.GLFW.*;
import static org.lwjgl.opengl.GL11.glClearColor;
import static org.lwjgl.system.MemoryUtil.NULL;

import java.nio.FloatBuffer;
import java.nio.IntBuffer;

import org.lwjgl.BufferUtils;
import org.lwjgl.glfw.GLFWCursorPosCallback;
import org.lwjgl.glfw.GLFWErrorCallback;
import org.lwjgl.glfw.GLFWKeyCallback;
import org.lwjgl.glfw.GLFWVidMode;
import org.lwjgl.opengl.ARBFragmentShader;
import org.lwjgl.opengl.ARBShaderObjects;
import org.lwjgl.opengl.ARBVertexBufferObject;
import org.lwjgl.opengl.ARBVertexShader;
import org.lwjgl.opengl.GL;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GLCapabilities;

import exercices.Camera;

public class FirstPersonView {
	/* Attributes */
	//Window
	private long window;
		
	private boolean do_run = true; // runs until done is set to true
	private int display_width = 640;
	private int display_height = 640;
		
	private int vertex_vbo_id = 0;
	private int color_vbo_id = 0;
	private int index_vbo_id = 0;
	private int nb_indices = 0;
	private boolean use_vbo = false; 
	private boolean use_shader = false; 
	 
	private int program;
	private int vertexShader;
	private int fragmentShader;    
	 
	//Pour l'animation
	//private Camera camera = new Camera(0,1f,0f);
	private GLFWKeyCallback keyCallback;
	private GLFWCursorPosCallback posCallback;
	private float movementSpeed = 0.1f;
	private float mouseSensitivity = 0.01f;
	private float x_pos = 0.0f;
	private float y_pos = 0.0f;
	private float dx= 0.0f;
	private float dy= 0.0f;
	private float dt= 0.0f; //length of frame
	private float lastTime= 0.0f; // when the last frame was
	private float time = 0.0f;
	static GLCapabilities glcaps;

	//Constructor
	public FirstPersonView() {
	}
	
	/* ------------------------------- GRAPHICS	---------------------- */
	
	//Running the program
	public void run() {
		//Initializations
		initDisplay();
		initGL();
		setFirstView();
		//camera.setPosition(0, 1, 0);
		while(do_run) {
			if( glfwWindowShouldClose(window)) do_run = false;
			render();
			glfwSwapBuffers(window);
			glfwPollEvents();
			//Binding the keyboard
			//keyPressed();
			//mouse();
		}
		
		//Terminate GLFW and free the error callback
		glfwTerminate();
		glfwSetErrorCallback(null).free();
	}
	
	//Rendering in the window
	public void gameLoop()
	{
		FPCameraController camera = new FPCameraController(0, 0, 0);
			

		//hide the mouse
		//glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
		
		// keep looping till the display window is closed the ESC key is down
		while (do_run) {
			//if( glfwWindowShouldClose(window)) do_run = false;
			time = System.currentTimeMillis();
			dt = (time - lastTime);
			lastTime = time;
			
			//mouse
			glfwSetCursorPosCallback(window, posCallback = new GLFWCursorPosCallback() {
				@Override
				public void invoke(long window, double xpos, double ypos) {
						dx = ((float) xpos) -x_pos;
						dy = ((float) ypos) -y_pos;
						x_pos = (float) xpos;
						y_pos = (float) ypos;
				}
			});

			//controll camera yaw from x movement fromt the mouse
			camera.yaw(dx * mouseSensitivity);
			//controll camera pitch from y movement fromt the mouse
			camera.pitch(dy * mouseSensitivity);

			glfwSetKeyCallback(window, keyCallback = new GLFWKeyCallback() {
				@Override
		        public void invoke(long window, int key, int scancode, int action, int mods) {
				//movement
				if ( key == GLFW_KEY_UP) camera.walkForward(movementSpeed*dt);
		        if ( key == GLFW_KEY_DOWN) camera.walkBackwards(movementSpeed*dt);
			    if ( key == GLFW_KEY_RIGHT)  camera.strafeRight(movementSpeed*dt);   
			    if ( key == GLFW_KEY_LEFT) camera.strafeLeft(movementSpeed*dt);
			    //quit
				if ( key == GLFW_KEY_ESCAPE && action == GLFW_RELEASE )
					glfwSetWindowShouldClose(window, true);
				}
			});
			//when passing in the distance to move
			//we times the movementSpeed with dt this is a time scale
			//so if its a slow frame u move more then a fast frame
			//so on a slow computer you move just as fast as on a fast computer
			//set the modelview matrix back to the identity
			GL11.glLoadIdentity();
			//look through the camera before you draw anything
			camera.lookThrough();
			draw_axis();
			draw_floor(10.0f);
			draw_cube(new Vector(5f,0f,5f),1f);
			glfwSwapBuffers(window);
			glfwPollEvents();
		}
		glfwTerminate();
		glfwSetErrorCallback(null).free();
	}
	
	private void render() {
		GL11.glClear(GL11.GL_COLOR_BUFFER_BIT| GL11.GL_DEPTH_BUFFER_BIT);
		GL11.glLoadIdentity();
		//GL11.glOrtho(-2f, 2f, -2f, 2f, -100f, 100f);
		//camera.adjust();
		//draw
		draw_axis();
		draw_floor(10.0f);
		draw_cube(new Vector(5f,0f,5f),1f);

	}
	
	private void initGL() {
		//Options
		GL11.glShadeModel(GL11.GL_SMOOTH);
		GL11.glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
		GL11.glClearDepth(1.0f);
		GL11.glEnable(GL11.GL_DEPTH_TEST);
		
		GL11.glDepthFunc(GL11.GL_LEQUAL);
		GL11.glEnable(GL11.GL_BLEND);
		GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
		GL11.glHint(GL11.GL_PERSPECTIVE_CORRECTION_HINT, GL11.GL_NICEST);
		
		GL11.glEnable(GL11.GL_LIGHTING);
		GL11.glShadeModel(GL11.GL_SMOOTH);
		GL11.glEnable(GL11.GL_COLOR_MATERIAL);
		GL11.glColorMaterial(GL11.GL_FRONT_AND_BACK,GL11.GL_AMBIENT);
		
		GL11.glEnable(GL11.GL_LIGHT0);
		GL11.glLightfv(GL11.GL_LIGHT0, GL11.GL_POSITION, makeFloatBuffer( new float[]{ 0f, 0f	, 1.0f, 0.0f } )  );
		GL11.glLightModelfv(GL11.GL_LIGHT_MODEL_AMBIENT, makeFloatBuffer(new float[]{1f, 0f, 0f,1f}));
		GL11.glEnable(GL11.GL_LIGHTING);
	}
	
	
	private void setFirstView() {
		GL11.glViewport(0, 0, display_width, display_height);
		GL11.glMatrixMode(GL11.GL_PROJECTION);
		GL11.glLoadIdentity();
		//Using an orthogonal view
		GL11.glOrtho(-2f, 2f, -2f, 2f, -100f, 100f);

	}
	
	//KeyBoard Actions
	/*private void keyPressed() {
		glfwSetKeyCallback(window, keyCallback = new GLFWKeyCallback() {
			@Override
            public void invoke(long window, int key, int scancode, int action, int mods) {
				//movement
				if ( key == GLFW_KEY_UP) camera.walkForward(movementSpeed);
                if ( key == GLFW_KEY_DOWN) camera.walkBackward(movementSpeed);
	            if ( key == GLFW_KEY_RIGHT)  camera.strafeRight(movementSpeed);   
	            if ( key == GLFW_KEY_LEFT) camera.strafeLeft(movementSpeed);
	            //quit
				if ( key == GLFW_KEY_ESCAPE && action == GLFW_RELEASE )
					glfwSetWindowShouldClose(window, true);
			}
		});

	}*/
	
	/*private void mouse() {
		glfwSetCursorPosCallback(window, posCallback = new GLFWCursorPosCallback() {
			@Override
			public void invoke(long window, double xpos, double ypos) {
					camera.angleX((float)((x_pos-xpos)) * mouseSpeed);
					camera.angleY((float)((y_pos-ypos)) * mouseSpeed);
					x_pos = xpos;
					y_pos = ypos;
			}
		});
	}*/
	
	
	/* --------------------- DRAWING FUNCTIONS --------------------- */
	//Points
	private void draw_point() {
		GL11.glBegin(GL11.GL_POINTS);
		GL11.glColor3f(1.0f, 0.0f, 0.0f); 
		GL11.glVertex3f(1.5f,1.5f,1.5f);
		GL11.glEnd();
		//Y axis
		GL11.glBegin(GL11.GL_POINTS);
		GL11.glColor3f(0.0f,1.0f, 0.0f); 
		GL11.glVertex3f(0f, 0f, 0f);
		GL11.glVertex3f(0f,1.5f,0f);
		GL11.glEnd();

	}
	//Axis
	private void draw_axis() {
		//X axis
		GL11.glBegin(GL11.GL_LINES);
		GL11.glColor3f(1.0f, 0.0f, 0.0f); 
		GL11.glVertex3f(0f, 0f, 0f);
		GL11.glVertex3f(1.5f,0f,0f);
		GL11.glEnd();
		//Y axis
		GL11.glBegin(GL11.GL_LINES);
		GL11.glColor3f(0.0f,1.0f, 0.0f); 
		GL11.glVertex3f(0f, 0f, 0f);
		GL11.glVertex3f(0f,1.5f,0f);
		GL11.glEnd();
		//Z axis
		GL11.glBegin(GL11.GL_LINES);
		GL11.glColor3f(0.0f,0.0f,1.0f); 
		GL11.glVertex3f(0f, 0f, 0f);
		GL11.glVertex3f(0f,0f,1.5f);
		GL11.glEnd();
		
	}
	//Triangle
	private void draw_triangle(float w, float h, float d)
    {
        w/=2;
        h/=2;
        d/=2;
        GL11.glBegin(GL11.GL_TRIANGLES);
        GL11.glNormal3f(0f,0f,1f);
        GL11.glVertex3f(w, h, d);
        GL11.glVertex3f(w, -h, d);
        GL11.glVertex3f(-w, h, d);
        GL11.glEnd();
    }
	
	//Square
	private void draw_square(float d, float r)
	 {
		 d/=2;
	     GL11.glBegin(GL11.GL_QUADS);
	     GL11.glNormal3f(0f,0f,1f);
	     GL11.glVertex3f(-d, d, r);
	     GL11.glVertex3f(d, d, r);
	     GL11.glVertex3f(d, -d, r);
	     GL11.glVertex3f(-d,-d,r);
	     GL11.glEnd();
	 }
	
	//Floor
	private void draw_floor(float d) {
	     GL11.glBegin(GL11.GL_QUADS);
	     GL11.glNormal3f(0f,0f,0.8f);
	     GL11.glVertex3f(-d, 0, d);
	     GL11.glVertex3f(d, 0, d);
	     GL11.glVertex3f(d, 0, -d);
	     GL11.glVertex3f(-d,0,-d);
	     GL11.glEnd();
	     int di = (int) d;
	     for (int i= -5*di;i<5*di;i++) {
	 		GL11.glBegin(GL11.GL_LINES);
	 		GL11.glNormal3f(0f, 0f, 0.5f);
			GL11.glColor3f(0.0f, 0.0f, 0.0f); 
			GL11.glVertex3f(-i/5 *1f,0.01f, -di*1f);
			GL11.glVertex3f(-i/5*1f,0.01f,di*1f);
			GL11.glEnd();
	 		GL11.glBegin(GL11.GL_LINES);
	 		GL11.glNormal3f(0f, 0f, 0.5f);
			GL11.glColor3f(0.0f, 0.0f, 0.0f); 
			GL11.glVertex3f(-di *1f,0.01f, -i/5*1f);
			GL11.glVertex3f(di*1f,0.01f,-i/5*1f);
			GL11.glEnd();
	     }
	}
	private void draw_cube(float d)
	 {
		 d/=2;
		 //face 1
		 GL11.glBegin(GL11.GL_QUADS);
		 GL11.glNormal3f(0f,0f,0.2f);
		 GL11.glColor3f(1f, 0f, 0f);
		 GL11.glVertex3f(-d, d, -d); 
		 GL11.glVertex3f(d, d, -d); 
	     GL11.glVertex3f(d, -d, -d); 
	     GL11.glVertex3f(-d,-d,-d); 
	     GL11.glEnd();
	     //face 2
	     GL11.glBegin(GL11.GL_QUADS);
		 GL11.glNormal3f(0f,0f,0.4f);
	     GL11.glColor3f(0f,1f,0f);
		 GL11.glVertex3f(-d,d , -d); 
		 GL11.glVertex3f(-d, -d, -d); 
	     GL11.glVertex3f(-d, -d, +d); 
	     GL11.glVertex3f(-d,d,+d);     
	     GL11.glEnd();
	     //face 3
	     GL11.glBegin(GL11.GL_QUADS);
		 GL11.glNormal3f(0f,0f,0.6f);
	     GL11.glColor3ui(255,0,0);
		 GL11.glVertex3f(d,-d , -d); 
		 GL11.glVertex3f(d, d, -d); 
	     GL11.glVertex3f(d, d, +d); 
	     GL11.glVertex3f(d,-d,+d);     
	     GL11.glEnd();
	     //face 4
		 GL11.glBegin(GL11.GL_QUADS);
		 GL11.glNormal3f(0f,0f,0.8f);
		 GL11.glColor3f(1f, 0f, 0f);
		 GL11.glVertex3f(-d, d, +d); 
		 GL11.glVertex3f(d, d, +d); 
	     GL11.glVertex3f(d, -d, +d); 
	     GL11.glVertex3f(-d,-d,+d); 
	     GL11.glEnd();
	     //face 4
		 GL11.glBegin(GL11.GL_QUADS);
		 GL11.glNormal3f(0f,0f,0.8f);
		 GL11.glColor3f(1f, 0f, 0f);
		 GL11.glVertex3f(-d, d, +d); 
		 GL11.glVertex3f(d, d, +d); 
	     GL11.glVertex3f(d, -d, +d); 
	     GL11.glVertex3f(-d,-d,+d); 
	     GL11.glEnd();
	     //face 5
		 GL11.glBegin(GL11.GL_QUADS);
		 GL11.glNormal3f(0f,0f,0.1f);
		 GL11.glColor3f(0.5f, 1f, 0.8f);
		 GL11.glVertex3f(-d, d, -d); 
		 GL11.glVertex3f(-d, d, +d); 
	     GL11.glVertex3f(d, d, +d); 
	     GL11.glVertex3f(d,d,-d); 
	     GL11.glEnd();
	     //face 6
		 GL11.glBegin(GL11.GL_QUADS);
		 GL11.glNormal3f(0f,0f,0.9f);
		 GL11.glColor3f(1f, 0f, 0f);
		 GL11.glVertex3f(-d, -d, -d); 
		 GL11.glVertex3f(-d, -d, +d); 
	     GL11.glVertex3f(d, -d, +d); 
	     GL11.glVertex3f(d,-d,-d); 
	     GL11.glEnd();
	 }

	public void draw_cube(Vector origin, float d){
		GL11.glTranslatef(origin.x+d/2, origin.y+d/2, origin.z+d/2);
		draw_cube(d);
		GL11.glTranslatef(-origin.x+d/2, -origin.y+d/2, -origin.z+d/2);
	}
	
/*--------------------- Given functions ---------------------------- */
	private static String getLogInfo(int obj) {
        return ARBShaderObjects.glGetInfoLogARB(obj, ARBShaderObjects.glGetObjectParameteriARB(obj, ARBShaderObjects.GL_OBJECT_INFO_LOG_LENGTH_ARB));
    }
	
	//Display initialization
	private void initDisplay(){
		//taken from lwjgl https://www.lwjgl.org/guide
        try{

		// Setup an error callback. The default implementation
		// will print the error message in System.err.
		GLFWErrorCallback.createPrint(System.err).set();

		// Initialize GLFW. Most GLFW functions will not work before doing this.
		if ( !glfwInit() )
			throw new IllegalStateException("Unable to initialize GLFW");

		// Configure our window
		glfwDefaultWindowHints(); // optional, the current window hints are already the default
		glfwWindowHint(GLFW_VISIBLE, GLFW_FALSE); // the window will stay hidden after creation
		glfwWindowHint(GLFW_RESIZABLE, GLFW_TRUE); // the window will be resizable

		// Create the window
		window = glfwCreateWindow( display_width,  display_height, "Bonjour PACT!", NULL, NULL);
		if ( window == NULL )
			throw new RuntimeException("Failed to create the GLFW window");

		// Setup a key callback. It will be called every time a key is pressed, repeated or released.
		glfwSetKeyCallback(window, (window, key, scancode, action, mods) -> {
			if ( key == GLFW_KEY_ESCAPE && action == GLFW_RELEASE )
				glfwSetWindowShouldClose(window, true); // We will detect this in our rendering loop
		});

		// Get the resolution of the primary monitor
		GLFWVidMode vidmode = glfwGetVideoMode(glfwGetPrimaryMonitor());

		glfwSetWindowPos(window, 0, 0);

		// Make the OpenGL context current
		glfwMakeContextCurrent(window);
		// Enable v-sync
		glfwSwapInterval(1);

		// Make the window visible
		glfwShowWindow(window);
		glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
		
		glcaps = GL.createCapabilities();

		glClearColor(1.0f, 0.0f, 0.0f, 0.0f);

        }catch(Exception e){
            System.out.println("Error setting up display: "+ e.getMessage());
            System.exit(0);
        }
    }	
	
	//Buffers 
	private static FloatBuffer makeFloatBuffer(float[] values)
	{
		FloatBuffer buffer = BufferUtils.createFloatBuffer(values.length);
		buffer.put(values);
		buffer.flip();
		return buffer;
	}
	
	// Main
	public static void main(String[] args) {
		FirstPersonView app = new FirstPersonView();
        app.gameLoop();
	}

}

