package exercices;

import java.awt.Color;

import org.lwjgl.opengl.GL11;


/**
 * Draws a guy standing on a segway.
 * <p> He will be the one leading us on the solution path.</p>
 * @author Edward Tombre
 *
 */
public class SegwayGuy{
	/**
	 * Position of the elements on the X-axis
	 * @see getPositionX
	 * @see setPositionX
	 */
	private float positionX;
	/**
	 * Position of the elements on the Y-axis
	 * @see getPositionY
	 * @see setPositionY
	 */
	private float positionY;
	/**
	 * Size of the element
	 * @see getSize
	 * @see setSize
	 */
	private float size;

	/**
	 * Constructs a new element
	 * @param positionX The initial X-position
	 * @param positionY The initial Y-position
	 * @param size The initial size
	 */
	/**
	 * His rotation amount following the Z-Axis
	 * @see getRotation
	 * @see setRotation
	 */
	private float rotation;
	/**
	 * Defines a new segway guy
	 * @param x His initial X-Position
	 * @param y His initial Y-Position
	 * @param size His initial size
	 * @param rotation His initial rotation
	 */
	public SegwayGuy(float x, float y, float size, float rotation) {
		this.positionX = positionX;
		this.positionY = positionY;
		this.size = size;
		this.rotation = rotation;
	}
	
	/**
	 * Gives the X-position
	 * @return The X-position
	 */
	public float getPositionX() {
		return positionX;
	}
	/**
	 * Sets a new X-position
	 * @param positionX The new X-position
	 */
	public void setPositionX(float positionX) {
		this.positionX = positionX;
	}
	/**
	 * Gives the Y-position
	 * @return The Y-position
	 */
	public float getPositionY() {
		return positionY;
	}
	/**
	 * Sets a new Y-position
	 * @param positionY The new Y-position
	 */
	public void setPositionY(float positionY) {
		this.positionY = positionY;
	}
	/**
	 * Gives the size of the element
	 * @return The element's size
	 */
	public float getSize() {
		return size;
	}
	/**
	 * Sets a new size for the element
	 * @param size The new size
	 */
	public void setSize(float size) {
		this.size = size;
	}
	
	/**
	 * Gives back the rotation
	 * @return The rotation
	 */
	public float getRotation() {
		return rotation;
	}
	/**
	 * Sets a new rotation amount
	 * @param rotation The new rotation amount
	 */
	public void setRotation(float rotation) {
		this.rotation = rotation;
	}
	
	/**
	 * Rotates the segway guy
	 * @param angle The amount of the rotation
	 */
	public void rotate(float angle) {
		rotation+=angle;
	}
	
	
	/**
	 * Draws the segway
	 * @param size The segway's size
	 */
	public void draw_segway(float size) {
		//Defining the different colors
		Color DARK_GRAY = new Color(0.04f,0.04f,0.04f);
		Color GRAY = new Color(0.15f, 0.15f, 0.15f);
		//Tires
		GL11.glRotatef(-90,0,0,1);
		GL11.glTranslatef(0f, 0.4f*size, 0f);
		Cylinder tire = new Cylinder(0.3f*size, 0.05f*size, 15,DARK_GRAY);
		tire.draw();
		GL11.glTranslatef(0f,-0.4f*size, 0f);
		GL11.glRotatef(180,0,0,1);
		GL11.glTranslatef(0f,0.4f*size, 0f);
		tire.draw();
		GL11.glTranslatef(0f, -0.4f*size, 0f);
		GL11.glRotatef(-90,0,0,1);
		//Plate
		Pave plate = new Pave(0.8f*size, 0.1f*size, 0.3f*size, GRAY);
		plate.draw();
		//Stick
		GL11.glRotatef(22,1,0,0);
		GL11.glTranslatef(0, 0.7f*size,0.1f*size);
		Cylinder stick = new Cylinder(0.05f*size, 0.6f*size, 10, GRAY);
		stick.draw();
		GL11.glTranslatef(0, -0.7f*size,-0.1f*size);
		GL11.glRotatef(-22,1,0,0);
		//Handlebar
		GL11.glRotatef(-90, 0, 0, 1);
		GL11.glTranslatef(-1.2f*size, 0,0.6f*size);
		Cylinder handleBar = new Cylinder(0.06f*size, 0.3f*size, 10, GRAY);
		handleBar.draw();
		GL11.glTranslatef(1.2f*size,0,-0.6f*size);
		GL11.glRotatef(90, 0, 0, 1);
	}
		

	/**
	 * {@inheritDoc}
	 */
	public void draw() {
		//Defining the colors
		 Color BROWN = new Color(0.2f,0.1f,0f);
		 Color BLACK = new Color(10,10,10);
		 Color LIGHT_BROWN = new Color(139,69,19);
		 Color DARK_GREEN =  new Color(0,51,25);
		 Color LIGHT_YELLOW = new Color(255,255,173);
		 Color DARK_GRAY =  new Color(50,50,50);
		 Color PINK = new Color(1f,0.9f,0.74f);
		draw_segway(1f);
		GL11.glTranslatef(0, 0.13f, 0);
		Human human = new Human(1.3f, LIGHT_BROWN, DARK_GREEN, LIGHT_YELLOW, DARK_GRAY,PINK, BLACK, BROWN);
		human.draw();
		GL11.glTranslatef(0, -0.13f, 0);
	}
	
	public void draw2() {
		//Defining the colors
		 Color DARK_BROWN = new Color(0.2f,0.1f,0f);
		 Color BLACK = new Color(10,10,10);
		 Color DARK_RED=  new Color(120,0,25);
		 Color LIGHT_PINK = new Color(255,204,255);
		 Color RED = new Color (0.8f,0f,0f);
		 Color PINK = new Color(1f,0.9f,0.74f);
		 Color BLUE = new Color(65,105,225);
		draw_segway(1f);
		GL11.glTranslatef(0, 0.13f, 0);
		Human human = new Human(1.3f, BLACK, DARK_RED, LIGHT_PINK, RED ,PINK, DARK_BROWN, BLUE);;
		human.draw();
		GL11.glTranslatef(0, -0.13f, 0);
	}

}
