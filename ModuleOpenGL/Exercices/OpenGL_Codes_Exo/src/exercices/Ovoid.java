package exercices;

import java.awt.Color;

import org.lwjgl.opengl.GL11;

/**
 * Draws an ovoid in OpenGL
 * @author Edward Tombre
 *
 */
public final class Ovoid {
	/**
	 * The radius following the X-Axis.
	 * It can't be modified
	 */
	private final float radiusX;
	/**
	 * The radius following the Y-Axis.
	 * It can't be modified
	 */
	private final float radiusY;
	/**
	 * The radius following the Z-Axis.
	 * It can't be modified
	 */
	private final float radiusZ;
	/**
	 * The number of slices. <p> A higher number of slices will improve the quality, but will need more time to be drawn.</p>
	 */
	private final int slices;
	/**
	 * The color of the ovoid
	 */
	private final Color color;
	
	/**
	 * Constructs a new ovoid.
	 * @param radiusX The X-radius
	 * @param radiusY The Y-radius
	 * @param radiusZ The Z-radius
	 * @param slices The number of slices
	 * @param color The color of the ovoid
	 */
	public Ovoid(float radiusX, float radiusY, float radiusZ, int slices, Color color) {
		this.radiusX = radiusX;
		this.radiusY = radiusY;
		this.radiusZ = radiusZ;
		this.slices = slices;
		this.color = color;
	}
	/**
	 * Draws an ovoid in the OpenGL window
	 */
	public void draw(){
		GL11.glBegin(GL11.GL_QUADS);
		GL11.glColor3f((float) color.getRed()/255,(float) color.getGreen()/255,(float)color.getBlue()/255);
		for (int i=0; i<slices; i++){
			for (int j=0;j<slices;j++){
				double phi1 = (Math.PI/2)*((double)2*j/slices -1);
				double psi1 = Math.PI*((double)2*i/slices -1);
				double phi2 = (Math.PI/2)*((double)2*(j+1)/slices -1);
				double psi2 = Math.PI*((double)2*(i+1)/slices -1);
				double x0 = radiusX*(Math.cos(phi1))*(Math.cos(psi1));
				double x1 = radiusX*(Math.cos(phi1))*(Math.cos(psi2));
				double x2 =radiusX*(Math.cos(phi2))*(Math.cos(psi1));
				double x3 =radiusX*(Math.cos(phi2))*(Math.cos(psi2));
				double z0 = radiusY*(Math.cos(phi1))*(Math.sin(psi1));
				double z1 = radiusY*(Math.cos(phi1))*(Math.sin(psi2));
				double z2 = radiusY*(Math.cos(phi2))*(Math.sin(psi1));
				double z3 = radiusY*(Math.cos(phi2))*(Math.sin(psi2));
				double y0 = radiusZ*(Math.sin(phi1));
				double y1 = radiusZ*(Math.sin(phi2));
				GL11.glVertex3d(x0,y0,z0);
				GL11.glVertex3d(x1,y0,z1);
				GL11.glVertex3d(x3,y1,z3);
				GL11.glVertex3d(x2,y1,z2);
			}
		}
		GL11.glEnd();
	}
}
