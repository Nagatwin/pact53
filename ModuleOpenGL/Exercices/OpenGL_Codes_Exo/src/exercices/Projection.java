package exercices;

import static org.lwjgl.glfw.GLFW.GLFW_FALSE;
import static org.lwjgl.glfw.GLFW.*;

import static org.lwjgl.glfw.GLFWKeyCallback.create;
import static org.lwjgl.opengl.GL11.glClearColor;
import static org.lwjgl.system.MemoryUtil.NULL;

import java.nio.FloatBuffer;
import java.nio.IntBuffer;

import org.lwjgl.BufferUtils;
import org.lwjgl.glfw.GLFWErrorCallback;
import org.lwjgl.glfw.GLFWKeyCallback;
import org.lwjgl.glfw.GLFWVidMode;
import org.lwjgl.opengl.ARBFragmentShader;
import org.lwjgl.opengl.ARBShaderObjects;
import org.lwjgl.opengl.ARBVertexBufferObject;
import org.lwjgl.opengl.ARBVertexShader;
import org.lwjgl.opengl.GL;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GLCapabilities;

public class Projection {
	
	// Window
	private long window;
	
	private boolean do_run = true; // runs until done is set to true
	private int display_width = 640;
	private int display_height = 640;
	
	private int vertex_vbo_id = 0;
    private int color_vbo_id = 0;
    private int index_vbo_id = 0;
    private int nb_indices = 0;
    private boolean use_vbo = false; 
    private boolean use_shader = false; 
    
    private int program;
    private int vertexShader;
    private int fragmentShader;    
    
    //Pour l'animation
    private int vx;
    private int vy;
    private int vz;
    
    private GLFWKeyCallback keyCallback;
    static GLCapabilities glcaps;
    
    public Projection() {
    }
    
    
	private static FloatBuffer makeFloatBuffer(float[] values)
	{
		FloatBuffer buffer = BufferUtils.createFloatBuffer(values.length);
		buffer.put(values);
		buffer.flip();
		return buffer;
	}
    
    
	//Running the program
	public void Run(){
        initDisplay();
        
        initGL();
        
        initShaders();
        setFirstView();
        
        keyPressed();

        while(do_run){
            if ( glfwWindowShouldClose(window) )
                do_run=false;
            render();
			glfwSwapBuffers(window);
			glfwPollEvents();
        }
        
		// Terminate GLFW and free the error callback
		glfwTerminate();
		glfwSetErrorCallback(null).free();
    }
	
	//Rendering in the window
	private void render(){
        float w, h;
       GL11.glClear(GL11.GL_COLOR_BUFFER_BIT | GL11.GL_DEPTH_BUFFER_BIT);  
        if (Math.abs(vx)+Math.abs(vy)+Math.abs(vz) != 0) {
        	GL11.glRotatef(vx, 1,0,0);
        	GL11.glRotatef(vy, 0,1,0);
        	GL11.glRotatef(vz, 0,0,1);
        }
        if (use_vbo) {
            draw_triangle_vbo(1f, 1f,1f);
        } else {
            draw_cube(1f,1f);
        }
    }
	
	//Display initialization
	private void initDisplay(){
		//taken from lwjgl https://www.lwjgl.org/guide
        try{

		// Setup an error callback. The default implementation
		// will print the error message in System.err.
		GLFWErrorCallback.createPrint(System.err).set();

		// Initialize GLFW. Most GLFW functions will not work before doing this.
		if ( !glfwInit() )
			throw new IllegalStateException("Unable to initialize GLFW");

		// Configure our window
		glfwDefaultWindowHints(); // optional, the current window hints are already the default
		glfwWindowHint(GLFW_VISIBLE, GLFW_FALSE); // the window will stay hidden after creation
		glfwWindowHint(GLFW_RESIZABLE, GLFW_TRUE); // the window will be resizable

		// Create the window
		window = glfwCreateWindow( display_width,  display_height, "Bonjour PACT!", NULL, NULL);
		if ( window == NULL )
			throw new RuntimeException("Failed to create the GLFW window");

		// Setup a key callback. It will be called every time a key is pressed, repeated or released.
		glfwSetKeyCallback(window, (window, key, scancode, action, mods) -> {
			if ( key == GLFW_KEY_ESCAPE && action == GLFW_RELEASE )
				glfwSetWindowShouldClose(window, true); // We will detect this in our rendering loop
		});

		// Get the resolution of the primary monitor
		GLFWVidMode vidmode = glfwGetVideoMode(glfwGetPrimaryMonitor());

		glfwSetWindowPos(window, 0, 0);

		// Make the OpenGL context current
		glfwMakeContextCurrent(window);
		// Enable v-sync
		glfwSwapInterval(1);

		// Make the window visible
		glfwShowWindow(window);

		glcaps = GL.createCapabilities();

		glClearColor(1.0f, 0.0f, 0.0f, 0.0f);

        }catch(Exception e){
            System.out.println("Error setting up display: "+ e.getMessage());
            System.exit(0);
        }
    }
	
	private void setFirstView() {
		 /*GL viewport: nous utilisons une moiti� de la fenetre pour dessiner*/
        GL11.glViewport(0, 0, display_width, display_height);
        /*Matrice de projection (3D vers 2D): utilisation d'une projection en ortho*/
        GL11.glMatrixMode(GL11.GL_PROJECTION);
        GL11.glLoadIdentity();
        //GL11.glFrustum(-1f, 1f, -1f, 1f, -10, 10);
        GL11.glOrtho(-2f, 2f, -2f, 2f, -10, 10);
        GL11.glTranslated(-0.25f,0.25f,0);
        GL11.glRotated(45,0,1,0);
        GL11.glRotated(25,1,0,0);
        GL11.glRotated(30,0,0,1);
        /*Matrice de modele (e.g. positionnement initial de la "camera" )
        GL11.glMatrixMode(GL11.GL_MODELVIEW);        
        GL11.glLoadIdentity();*/
        
	}
	
	private void setSecondView() {
		//Et une deuxi�me fen�tre de vue
        GL11.glViewport(display_width/2,0, display_width/2, display_height);
       /* Matrice de projection (3D vers 2D): utilisation d'une projection en ortho*/
        GL11.glMatrixMode(GL11.GL_PROJECTION);
        GL11.glLoadIdentity();
        GL11.glFrustum(-2f, 2f, -2f, 2f, -10, 10);
        GL11.glTranslated(0.25f,0.25f,0);
        GL11.glRotated(-65,0,1,0);
        GL11.glRotated(25,1,0,0);
        GL11.glRotated(30,0,0,1);
        /*GL11.glMatrixMode(GL11.GL_MODELVIEW);        
        GL11.glLoadIdentity();*/
	}
	 private void initGL(){    
		       /* Diverses options OpenGL*/
	        GL11.glShadeModel(GL11.GL_SMOOTH);
	        GL11.glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	        GL11.glClearDepth(1.0f);
	        GL11.glEnable(GL11.GL_DEPTH_TEST);
	        
	        GL11.glDepthFunc(GL11.GL_LEQUAL);
	        GL11.glEnable(GL11.GL_BLEND);
	        GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
	        GL11.glHint(GL11.GL_PERSPECTIVE_CORRECTION_HINT, GL11.GL_NICEST);
	        
	        
	        //GL11.glEnable(GL11.GL_LIGHTING);
	        GL11.glShadeModel(GL11.GL_SMOOTH);
	        GL11.glEnable(GL11.GL_COLOR_MATERIAL);
	        GL11.glColorMaterial(GL11.GL_FRONT_AND_BACK,GL11.GL_AMBIENT);
	        
	        /*GL11.glEnable(GL11.GL_LIGHT0);
	    	GL11.glLightfv(GL11.GL_LIGHT0, GL11.GL_POSITION, makeFloatBuffer( new float[]{ 0f, 0f	, 1.0f, 0.0f } )  );
	     	GL11.glLightModelfv(GL11.GL_LIGHT_MODEL_AMBIENT, makeFloatBuffer(new float[]{1f, 0f, 0f,1f}));
	        GL11.glEnable(GL11.GL_LIGHTING);*/
	    
	    }

	 
	 //KeyBoards Actions
	 private void keyPressed() {
			glfwSetKeyCallback(window, keyCallback = new GLFWKeyCallback() {

	            @Override
	            public void invoke(long window, int key, int scancode, int action, int mods) {

	                if ( key == GLFW_KEY_RIGHT ) {
	                	if (action == GLFW_PRESS) vx = 1;
	                	if (action == GLFW_RELEASE) vx = 0;
	                }
	                if ( key == GLFW_KEY_LEFT ) {
		                if (action == GLFW_PRESS) vx = -1;
		                if (action == GLFW_RELEASE) vx = 0;
	                }
		            if ( key == GLFW_KEY_UP ) {
		               	if (action == GLFW_PRESS) vz = 1;
		                if (action == GLFW_RELEASE) vz = 0;

		            }
		            if ( key == GLFW_KEY_DOWN ) {
			            if (action == GLFW_PRESS) vz = -1;
			            if (action == GLFW_RELEASE) vz = 0;

		            }
	            }
			});
	 }
	 
	 //Drawing a triangle
	 private void draw_triangle(float w, float h, float d)
	    {
	        w/=2;
	        h/=2;
	        d/=2;
	        
	        GL11.glBegin(GL11.GL_TRIANGLES);
	        GL11.glNormal3f(0f,0f,1f);
	        GL11.glVertex3f(w, h, d);
	        GL11.glVertex3f(w, -h, d);
	        GL11.glVertex3f(-w, h, d);


	        GL11.glEnd();
	    }
	 
	 //Drawing a square
	 private void draw_square(float d, float r)
	 {
		 d/=2;
	     GL11.glBegin(GL11.GL_QUADS);
	     GL11.glNormal3f(0f,0f,1f);
	     GL11.glVertex3f(-d, d, r);
	     GL11.glVertex3f(d, d, r);
	     GL11.glVertex3f(d, -d, r);
	     GL11.glVertex3f(-d,-d,r);
	     
	     GL11.glEnd();
	 }
	 
	 //Drawing a square using TRIANGLE_FAN
	 private void draw_square_fan(float d, float r)
	 {
		 d/=2;
		 GL11.glBegin(GL11.GL_TRIANGLE_FAN);
		 GL11.glNormal3f(0f,0f,1f);
	     GL11.glVertex3f(d, -d, r);
		 GL11.glVertex3f(d, d, r);
		 GL11.glVertex3f(-d, d, r);
	     GL11.glVertex3f(-d,-d,r);
	     
	     GL11.glEnd();
	 }
	 
	 private void draw_square_strip(float d, float r)
	 {
		 d/=2;
		 GL11.glBegin(GL11.GL_TRIANGLE_STRIP);
		 GL11.glNormal3f(0f,0f,1f);
		 GL11.glVertex3f(-d, d, r);
		 GL11.glVertex3f(-d, -d, r);
	     GL11.glVertex3f(d, d, r);
	     GL11.glVertex3f(d,-d,r);
	     
	     GL11.glEnd();
	 }
	 private void draw_cube(float d, float r)
	 {

		 d/=2;
		 //face 1
		 GL11.glBegin(GL11.GL_QUADS);
		 GL11.glNormal3f(0f,0f,0.2f);
		 GL11.glColor3f(1f, 0f, 0f);
		 GL11.glVertex3f(-d, d, r-d); 
		 GL11.glVertex3f(d, d, r-d); 
	     GL11.glVertex3f(d, -d, r-d); 
	     GL11.glVertex3f(-d,-d,r-d); 
	     GL11.glEnd();
	     //face 2
	     GL11.glBegin(GL11.GL_QUADS);
		 GL11.glNormal3f(0f,0f,0.4f);
	     GL11.glColor3f(0f,1f,0f);
		 GL11.glVertex3f(-d,d , r-d); 
		 GL11.glVertex3f(-d, -d, r-d); 
	     GL11.glVertex3f(-d, -d, r+d); 
	     GL11.glVertex3f(-d,d,r+d);     
	     GL11.glEnd();
	     //face 3
	     GL11.glBegin(GL11.GL_QUADS);
		 GL11.glNormal3f(0f,0f,0.6f);
	     GL11.glColor3ui(255,0,0);
		 GL11.glVertex3f(d,-d , r-d); 
		 GL11.glVertex3f(d, d, r-d); 
	     GL11.glVertex3f(d, d, r+d); 
	     GL11.glVertex3f(d,-d,r+d);     
	     GL11.glEnd();
	     //face 4
		 GL11.glBegin(GL11.GL_QUADS);
		 GL11.glNormal3f(0f,0f,0.8f);
		 GL11.glColor3f(1f, 0f, 0f);
		 GL11.glVertex3f(-d, d, r+d); 
		 GL11.glVertex3f(d, d, r+d); 
	     GL11.glVertex3f(d, -d, r+d); 
	     GL11.glVertex3f(-d,-d,r+d); 
	     GL11.glEnd();
	     //face 4
		 GL11.glBegin(GL11.GL_QUADS);
		 GL11.glNormal3f(0f,0f,0.8f);
		 GL11.glColor3f(1f, 0f, 0f);
		 GL11.glVertex3f(-d, d, r+d); 
		 GL11.glVertex3f(d, d, r+d); 
	     GL11.glVertex3f(d, -d, r+d); 
	     GL11.glVertex3f(-d,-d,r+d); 
	     GL11.glEnd();
	     //face 5
		 GL11.glBegin(GL11.GL_QUADS);
		 GL11.glNormal3f(0f,0f,0.1f);
		 GL11.glColor3f(0.5f, 1f, 0.8f);
		 GL11.glVertex3f(-d, d, r-d); 
		 GL11.glVertex3f(-d, d, r+d); 
	     GL11.glVertex3f(d, d, r+d); 
	     GL11.glVertex3f(d,d,r-d); 
	     GL11.glEnd();
	     //face 6
		 GL11.glBegin(GL11.GL_QUADS);
		 GL11.glNormal3f(0f,0f,0.9f);
		 GL11.glColor3f(1f, 0f, 0f);
		 GL11.glVertex3f(-d, -d, r-d); 
		 GL11.glVertex3f(-d, -d, r+d); 
	     GL11.glVertex3f(d, -d, r+d); 
	     GL11.glVertex3f(d,-d,r-d); 
	     GL11.glEnd();
	 }
	 
	 
	 private void draw_sphere_points(float r, int n)
	 {
		 GL11.glBegin(GL11.GL_POINTS);
		 for (int i=0; i<n; i++)
		 {
			 for (int j=0;j<n;j++)
			 {
				 double phi = (Math.PI/2)*((double)2*j/n -1);
				 double psi = Math.PI*((double)2*i/n -1);
				 double x = r*(Math.cos(phi))*(Math.cos(psi));
				 double z = r*(Math.cos(phi))*(Math.sin(psi));
				 double y = r*(Math.sin(phi));
				 
				 GL11.glVertex3d(x,y,z);
			 }
		 }
		 GL11.glEnd();
	 }
	 //Draw a sphere
	 private void draw_sphere(float r, int n)
	 {
		 GL11.glBegin(GL11.GL_QUADS);
		 for (int i=0; i<n; i++)
		 {
			 for (int j=0;j<n;j++)
			 {
				 double phi1 = (Math.PI/2)*((double)2*j/n -1);
				 double psi1 = Math.PI*((double)2*i/n -1);
				 double phi2 = (Math.PI/2)*((double)2*(j+1)/n -1);
				 double psi2 = Math.PI*((double)2*(i+1)/n -1);
				 double x0 = r*(Math.cos(phi1))*(Math.cos(psi1));
				 double x1 = r*(Math.cos(phi1))*(Math.cos(psi2));
				 double x2 =r*(Math.cos(phi2))*(Math.cos(psi1));
				 double x3 =r*(Math.cos(phi2))*(Math.cos(psi2));
				 double z0 = r*(Math.cos(phi1))*(Math.sin(psi1));
				 double z1 = r*(Math.cos(phi1))*(Math.sin(psi2));
				 double z2 = r*(Math.cos(phi2))*(Math.sin(psi1));
				 double z3 = r*(Math.cos(phi2))*(Math.sin(psi2));
				 double y0 = r*(Math.sin(phi1));
				 double y1 = r*(Math.sin(phi2));
				 
				 GL11.glColor3f((float)(x0+x1+x2+x3)/4, 0,0);
				 GL11.glVertex3d(x0,y0,z0);
				 GL11.glVertex3d(x1,y0,z1);
				 GL11.glVertex3d(x3,y1,z3);
				 GL11.glVertex3d(x2,y1,z2);
			 }
		 }
		 GL11.glEnd();
	 }
	 
	 //
	    //  Fonctions pour la manipulations de VBO
	    //

	    private void draw_triangle_vbo(float w, float h, float d)
	    {
	        load_triangle_vbo(w, h, d);      
	        draw_vbos();
	    }
	    private void draw_square_vbo(float d, float r)
	    {
	        load_square_vbo(d, r);
	        draw_vbos();
	    }
	    
	    private static String getLogInfo(int obj) {
	        return ARBShaderObjects.glGetInfoLogARB(obj, ARBShaderObjects.glGetObjectParameteriARB(obj, ARBShaderObjects.GL_OBJECT_INFO_LOG_LENGTH_ARB));
	    }
	    
	    public static int createVBOID() {
	      if (glcaps.GL_ARB_vertex_buffer_object) {
	        IntBuffer buffer = BufferUtils.createIntBuffer(1);
	        ARBVertexBufferObject.glGenBuffersARB(buffer);
	        return buffer.get(0);
	      }
	      return 0;
	    }
	    
	    private int load_float_vbo(float [] values)
	    {
	      //chargeons les données dans un FloatBuffer
	  		FloatBuffer verticesBuffer = BufferUtils.createFloatBuffer(values.length);
		   	verticesBuffer.put(values);
		   	verticesBuffer.flip(); //NE PAS OUBLIER CETTE LIGNE!! ELLE PEUT CRASHER VOTRE JavaVM
	      
	      //creons un VBO dans la mémoire du GPU (pas encore de données associées))
	      int vbo_id = createVBOID();
	      
	      //et copions les données dans la mémoire du GPU
	  		if (glcaps.GL_ARB_vertex_buffer_object) {
	        ARBVertexBufferObject.glBindBufferARB(ARBVertexBufferObject.GL_ARRAY_BUFFER_ARB, vbo_id);
	        ARBVertexBufferObject.glBufferDataARB(ARBVertexBufferObject.GL_ARRAY_BUFFER_ARB, verticesBuffer, ARBVertexBufferObject.GL_STATIC_DRAW_ARB);
	      } else {
	        System.out.println("VBOs NOT SUPPORTED !!");
	      }
	      return vbo_id;
	    }
	    
	    private int load_index_vbo(int [] values)
	    {
	      //chargeons les données dans un IntBuffer
	  		IntBuffer indicesBuffer = BufferUtils.createIntBuffer(values.length);
		   	indicesBuffer.put(values);
		   	indicesBuffer.flip(); //NE PAS OUBLIER CETTE LIGNE!! ELLE PEUT CRASHER VOTRE JavaVM
	      
	      //creons un VBO dans la mémoire du GPU (pas encore de données associées))
	      int vbo_id = createVBOID();
	      
	      //et copions les données dans la mémoire du GPU
	  		if (glcaps.GL_ARB_vertex_buffer_object) {
	        ARBVertexBufferObject.glBindBufferARB(ARBVertexBufferObject.GL_ELEMENT_ARRAY_BUFFER_ARB, vbo_id);
	        ARBVertexBufferObject.glBufferDataARB(ARBVertexBufferObject.GL_ELEMENT_ARRAY_BUFFER_ARB, indicesBuffer, ARBVertexBufferObject.GL_STATIC_DRAW_ARB);
	      } else {
	        System.out.println("VBOs NOT SUPPORTED !!");
	      }
	      
	      //souvenons nous du nombre d'index dans notre VBO:
	      nb_indices = values.length;      
	      return vbo_id;
	    }
	    
	    
	    private void load_triangle_vbo(float w, float h, float d) {
	      //nous avons déja crée le VBO, inutile de recommencer ...
	      if (vertex_vbo_id != 0) return;

	      //creons un VBO qui contient nos vertex - nous avons besoin de 3 sommets
	      w/=2;
	      h/=2;
	      d/=2;
	      float[] vertices = new float[] {
	        -w, h, d,
	        w, h, d,
	        w, -h, d,
	      };
	      vertex_vbo_id = load_float_vbo(vertices);
	      
	      //creons un VBO qui contient les index des sommets dans les deux triangles de notre cube
	      int[] indices = new int[]
	      {
	        0,1,2
	      };
	      index_vbo_id = load_index_vbo(indices);

	      System.out.println("VBOs Setup - Vertex ID " + vertex_vbo_id + " - index ID " + index_vbo_id + " - nb indices " + nb_indices);
	    }
	    
	    //Square VBO
	    private void load_square_vbo(float d, float r) {
		      //nous avons déja crée le VBO, inutile de recommencer ...
		      if (vertex_vbo_id != 0) return;

		      //creons un VBO qui contient nos vertex - nous avons besoin de 3 sommets
		      d/=2;
		      float[] vertices = new float[] {
		        -d, d, r,
		        d, d, r,
		        d, -d, r,
		        -d, -d, r,
		      };
		      vertex_vbo_id = load_float_vbo(vertices);
		      
		      //creons un VBO qui contient les index des sommets dans les deux triangles de notre cube
		      int[] indices = new int[]
		      {
		        0,1,2,3
		      };
		      index_vbo_id = load_index_vbo(indices);

		      System.out.println("VBOs Setup - Vertex ID " + vertex_vbo_id + " - index ID " + index_vbo_id + " - nb indices " + nb_indices);
		    }
	    private void draw_vbos() {
	      if (vertex_vbo_id==0) return;
	      if (index_vbo_id==0) return;
	      
	      if (use_shader) {
	        ARBShaderObjects.glUseProgramObjectARB(program);
	      }
	      
	      GL11.glEnableClientState(GL11.GL_VERTEX_ARRAY);
	      ARBVertexBufferObject.glBindBufferARB(ARBVertexBufferObject.GL_ARRAY_BUFFER_ARB, vertex_vbo_id);
	      GL11.glVertexPointer(3, GL11.GL_FLOAT, 0, 0);

	      if (color_vbo_id != 0) {
	        GL11.glEnableClientState(GL11.GL_COLOR_ARRAY);
	        ARBVertexBufferObject.glBindBufferARB(ARBVertexBufferObject.GL_ARRAY_BUFFER_ARB, color_vbo_id);
	        GL11.glColorPointer(4, GL11.GL_FLOAT, 0, 0);
	      }

	      //attachons le buffer d'indices comme le buffer 'ELEMENT_ARRAY', i.e. celui utilisé pour glDrawElements
	      ARBVertexBufferObject.glBindBufferARB(ARBVertexBufferObject.GL_ELEMENT_ARRAY_BUFFER_ARB, index_vbo_id);
	  		
	      GL11.glDrawElements(GL11.GL_TRIANGLES, nb_indices, GL11.GL_UNSIGNED_INT, 0);

	      GL11.glDisableClientState(GL11.GL_VERTEX_ARRAY);
	      if (color_vbo_id != 0) GL11.glDisableClientState(GL11.GL_COLOR_ARRAY);

	      if (use_shader) {
	        ARBShaderObjects.glUseProgramObjectARB(0);
	      }
	    }

	    
	    //
	    //  Shader init
	    //
	    private void initShaders() {

			if (! use_shader) return;

	        /*init openGL shaders*/
	        program = ARBShaderObjects.glCreateProgramObjectARB();
	        if (program==0) {
	            System.out.println("Error: OpenGL shaders not supported");
	            System.exit(0);
	        }
	        vertexShader=ARBShaderObjects.glCreateShaderObjectARB(ARBVertexShader.GL_VERTEX_SHADER_ARB);
	        String vertexCode=""+
	        "void main(void)" + 
	        "{" + 
	        "	gl_Position = gl_ModelViewProjectionMatrix * gl_Vertex;" +  
	        "}";
	        
	        ARBShaderObjects.glShaderSourceARB(vertexShader, vertexCode);
	        ARBShaderObjects.glCompileShaderARB(vertexShader);
	        if (ARBShaderObjects.glGetObjectParameteriARB(vertexShader, ARBShaderObjects.GL_OBJECT_COMPILE_STATUS_ARB) == GL11.GL_FALSE)
	        {
	            System.out.println("Vertex shader not compiled: " + getLogInfo(vertexShader) );
	        }
	        
	        fragmentShader=ARBShaderObjects.glCreateShaderObjectARB(ARBFragmentShader.GL_FRAGMENT_SHADER_ARB);
	        String fragmentCode="" +
	        "void main(void) {" +
	        "	gl_FragColor = vec4 (0.0, 1.0, 0.0, 1.0);" +
	        "}";
	        
	        ARBShaderObjects.glShaderSourceARB(fragmentShader, fragmentCode);
	        ARBShaderObjects.glCompileShaderARB(fragmentShader);
	        if (ARBShaderObjects.glGetObjectParameteriARB(fragmentShader, ARBShaderObjects.GL_OBJECT_COMPILE_STATUS_ARB) == GL11.GL_FALSE)
	        {
	            System.out.println("Fragment shader not compiled: " + getLogInfo(fragmentShader) );
	        }
	        
	        
	        ARBShaderObjects.glAttachObjectARB(program, vertexShader);
	        ARBShaderObjects.glAttachObjectARB(program, fragmentShader);
	        ARBShaderObjects.glLinkProgramARB(program);
	        ARBShaderObjects.glValidateProgramARB(program);

	        System.out.println("Shaders initialized !" );
	    }
	    
	    
	    
	    public static void main(String[] args){
	        Projection app = new Projection();
	        /*parse our args*/
	        for (int i=0; i<args.length; i++) {
	            if (args[i].equals("-vbo")) {
	                app.use_vbo = true;
	            }
	            else if (args[i].equals("-shader")) {
	                app.use_shader = true;
	                app.use_vbo = true;
	            }
	        }
	        
	        app.Run();
	    }    
	    

}
