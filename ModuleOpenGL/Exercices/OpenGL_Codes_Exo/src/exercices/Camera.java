package exercices;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GLUtil;
import org.lwjgl.opengl.GLX;


public class Camera {
	
	private Vector position = null;
	private float angleY = 0.0f;
	private float angleX = 0.0f;
	
	//Constructor
	public Camera(float x, float y, float z) {
		position = new Vector(x,y,z);
	}
	
	//setter
	public void setPosition(float x, float y, float z) {
		position.x =x;
		position.y = y;
		position.z =z;
	}
	//Rotations
	public void angleX(float amount) {
		angleX += amount;
	}
	
	public void angleY(float amount) {
		angleY += amount;
	}

	//Walk Functions
	public void walkForward(float d) {
		position.x += d * (float)Math.sin(Math.toRadians(angleX));
		position.z -= d * (float)Math.cos(Math.toRadians(angleX));
	}
	
	public void walkBackward(float d) {
		position.x -= d * (float)Math.sin(Math.toRadians(angleX));
		position.z += d * (float)Math.cos(Math.toRadians(angleX));
	}
	
	public void strafeLeft(float d) {
		position.x -= d* (float)Math.sin(Math.toRadians(angleX-90));
		position.z += d* (float)Math.cos(Math.toRadians(angleX-90));
	}

	public void strafeRight(float d) {
		position.x -= d* (float)Math.sin(Math.toRadians(angleX+90));
		position.z += d* (float)Math.cos(Math.toRadians(angleX+90));
	}

	//Adjusting the camera
	public void adjust() {
		GL11.glTranslated(position.x, position.y, position.z);
		GL11.glRotatef(angleY, 1.0f, 0.0f, 0.0f);
		GL11.glRotatef(angleX, 0.0f, 1.0f, 0.0f);
	}

}
