package exercices;

import java.awt.Color;

import org.lwjgl.opengl.GL11;

/**
 * Draws a pave in OpenGL
 * @author Edward Tombre
 *
 */
public class Pave {
	/**
	 *  The pave's length.
	 * 	It can't be modified
	 */
	private final float length;
	/**
	 *  The pave's width.
	 * 	It can't be modified
	 */
	private final float width;
	/**
	 *  The pave's height.
	 * 	It can't be modified
	 */
	private final float height;
	/**
	 *  The pave's color.
	 * 	It can't be modified
	 */
	private final Color color;
	
	/**
	 * Constructs a new pave
	 * @param length The pave's length
	 * @param width The pave's width
	 * @param height The pave's height
	 * @param color The pave's color
	 */
	public Pave(float length, float width, float height, Color color) {
		super();
		this.length = length;
		this.width = width;
		this.height = height;
		this.color = color;
	}
	/**
	 * Constructs a new plane.
	 * @param length The plane's length
	 * @param width The plane's width
	 * @param color The plane's color
	 */
	public Pave(float length, float width, Color color) {
		this(length, width,0, color);
	}
	/**
	 * Draws the pave in the OpenGL window
	 */
	public void draw() {
		float length = this.length/2;
		float width =this.width/2;
		float height=this.height/2;
		GL11.glBegin(GL11.GL_TRIANGLE_STRIP);
		GL11.glColor3f((float) color.getRed()/255,(float) color.getGreen()/255,(float)color.getBlue()/255);
		GL11.glNormal3f(0f, 0f, 1f);
		GL11.glVertex3f(-length,width,height);
		GL11.glVertex3f(-length,-width,height);
		GL11.glVertex3f(length,width,height);
		GL11.glVertex3f(length,-width,height);
		GL11.glVertex3f(length,width,-height);
		GL11.glVertex3f(length,-width,-height);
		GL11.glVertex3f(-length,width,-height);
		GL11.glVertex3f(-length,-width,-height);
		GL11.glVertex3f(-length,width,height);
		GL11.glVertex3f(-length,-width,height);
		GL11.glEnd();
		GL11.glBegin(GL11.GL_QUADS);
		GL11.glVertex3f(-length,width,height);
		GL11.glVertex3f(length,width,height);
		GL11.glVertex3f(length,width,-height);
		GL11.glVertex3f(-length,width,-height);		
		GL11.glVertex3f(-length,-width,height);
		GL11.glVertex3f(length,-width,height);
		GL11.glVertex3f(length,-width,-height);
		GL11.glVertex3f(-length,-width,-height);
		GL11.glEnd();
	}

}

