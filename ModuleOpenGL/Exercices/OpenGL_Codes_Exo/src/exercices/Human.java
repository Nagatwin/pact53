package exercices;

import java.awt.Color;
import org.lwjgl.opengl.GL11;
/**
 * Draws a human body
 * @author Edward Tombre
 *
 */
public final class Human {
	/**
	 * The size of the human
	 */
	private float size;
	/**
	 * The color of his shoes
	 */
	private Color shoesColor;
	/**
	 * The color of his suit
	 */
	private Color suitColor;
	/**
	 * The color of his shirt
	 */
	private Color shirtColor;
	/**
	 * The color of his tie
	 */
	private Color tieColor;
	/**
	 * The color of his skin
	 */
	private Color skinColor;
	/**
	 * The color of his hair
	 */
	private Color hairColor;
	/**
	 * The color of his eyes
	 */
	private Color eyesColor;
	/**
	 * Defines a new human
	 * @param size The human's size
	 * @param shoesColor His shoes' color
	 * @param suitColor His suit's color
	 * @param shirtColor His shirt's color
	 * @param tieColor His tie's color
	 * @param skinColor His skin's color
	 * @param hairColor His hair's color
	 * @param eyesColor His eyes' color
	 */
	public Human(float size, Color shoesColor, Color suitColor, Color shirtColor, Color tieColor,
			Color skinColor, Color hairColor, Color eyesColor) {
		super();
		this.size = size;
		this.shoesColor = shoesColor;
		this.suitColor = suitColor;
		this.shirtColor = shirtColor;
		this.tieColor = tieColor;
		this.skinColor = skinColor;
		this.hairColor = hairColor;
		this.eyesColor = eyesColor;
	}
	/**
	 * Draws the human body in the OpenGL window
	 */
	public void draw() {
		//Left side
		draw_leg(1);
		draw_arm(1);
		//Right side
		GL11.glRotated(180, 0, 1, 0);
		draw_leg(-1);
		draw_arm(-1);
		GL11.glRotated(-180, 0, 1, 0);
		draw_trunk();
		draw_head();
	}
	
	/**
	 * Draws a leg
	 * @param i The number of the leg. <p> 1 stands for left, -1 stands for right    
	 */
	public void draw_leg(int i) {
		//Defining the colors
		GL11.glTranslatef(0.15f*size,0f, 0f);
		GL11.glTranslatef(0, 0, i*0.07f);
		Ovoid foot = new Ovoid(0.06f*size, 0.1f*size, 0.05f*size, 10, shoesColor);
		foot.draw();
		GL11.glTranslatef(0, 0, -i*0.07f);
		GL11.glRotatef(5,0,0,1);
		GL11.glTranslatef(0,0.25f*size,0);
		Cylinder leg = new Cylinder(0.1f*size, 0.06f*size, 0.25f*size, 10,suitColor);
		leg.draw();
		GL11.glTranslatef(0,-0.25f*size,0);
		GL11.glRotatef(-5,0,0,1);
		GL11.glTranslatef(-0.15f*size,0f, 0f);
	}
	/**
	 * Draws an arm
	 * @param i The number of the leg. <p> 1 stands for left, -1 stands for right    
	 */
	public void draw_arm(int i) {
		GL11.glTranslatef(0.12f*size,0f, 0f);
		GL11.glTranslatef(0f,0.95f*size, i*0.2f*size);
		GL11.glRotatef(-i*60,1, 0, 0);
		Cylinder arm = new Cylinder(0.07f*size,0.04f*size, 0.25f*size,10, suitColor);
		arm.draw();
		GL11.glRotatef(i*60,1, 0, 0);
		GL11.glTranslatef(0f,-0.12f*size,i*0.25f*size);
		Ovoid hand = new Ovoid(0.05f*size, 0.06f*size, 0.05f*size,10, skinColor);
		hand.draw();
		GL11.glTranslatef(0f,0.12f*size,-i*0.25f*size);
		GL11.glTranslatef(0f,-0.95f*size, -i*0.2f*size);
		GL11.glTranslatef(-0.12f*size,0f, 0f);
	}
	/**
	 * Draws the trunk
	 */
	public void draw_trunk() {
		GL11.glTranslatef(0,0.8f*size, 0);
		Ovoid trunk = new Ovoid(0.25f*size, 0.17f*size, 0.4f*size,20,suitColor);
		trunk.draw();
		GL11.glRotatef(-10,1,0,0);
		GL11.glTranslatef(0,0.17f*size,0.18f*size);
		//Drawing the shirt
		GL11.glColor3f(1f, 1f, 1f);
		Triangle shirt = new Triangle(0.17f*size, 0.3f*size, shirtColor);
		shirt.draw();
		GL11.glTranslatef(0,0.12f*size,0.001f*size);
		//Drawing the tie
		GL11.glColor3f(1f, 0f, 0f);
		Triangle tie1=new Triangle(0.05f*size, 0.05f*size, tieColor);
		tie1.draw();
		GL11.glRotatef(180, 0, 0, 1);
		tie1.draw();
		GL11.glRotatef(-180, 0, 0, 1);
		GL11.glTranslatef(0, -0.11f*size,0);
		Pave tie2 = new Pave(0.05f*size, 0.18f*size, tieColor);
		tie2.draw();
		GL11.glTranslatef(0,-0.14f*size,0);
		Triangle tie3 = new Triangle(0.05f*size, 0.1f*size, tieColor);
		tie3.draw();
		GL11.glTranslatef(0,-0.04f*size,-0.181f*size);
		GL11.glRotatef(10,1,0,0);
		GL11.glTranslatef(0,-0.8f*size, 0);
	}
	/**
	 * Draws the head 
	 */
	public void draw_head() {
		GL11.glTranslatef(0, 1.4f*size, 0);
		Ovoid head = new Ovoid(0.2f*size, 0.18f*size, 0.25f*size, 20, skinColor);
		head.draw();
		GL11.glTranslatef(0, -1.4f*size, 0);	
		GL11.glColor3f(0.7f, 0.5f, 0.2f);
		GL11.glTranslatef(0, 1.42f*size, 0);
		Ovoid hair = new Ovoid(0.193f*size, 0.175f*size,0.244f*size,20, hairColor);
		hair.draw();
		GL11.glTranslatef(0, -1.42f*size, 0);
		GL11.glTranslated(-0.07f*size, 1.4f*size,0.16*size);
		Ovoid eye = new Ovoid(0.025f*size, 0.02f*size, 0.035f*size,20, eyesColor);
		eye.draw();
		GL11.glTranslated(0.14f*size,0f,0f);;
		eye.draw();
		GL11.glTranslated(-0.14f*size,0,0);
		GL11.glTranslated(0.07f*size, -1.4f*size,-0.16*size);
	}
}
