package exercices;

import java.awt.Color;

import org.lwjgl.opengl.GL11;

/**
 * Draws a Triangle in OpenGL
 * @author Edward Tombre
 *
 */
public class Triangle {
	/** 
	 * The triangle's width
	 */
	private final float width;
	/**
	 * The triangle's height
	 */
	private final float height;
	/**
	 * The triangle's color
	 */
	private final Color color;
	
	/**
	 * Constructs a new triangle
	 * @param width The initial width
	 * @param height The initial height
	 * @param color The initial color
	 */
	public Triangle(float width, float height, Color color) {
		super();
		this.width = width;
		this.height = height;
		this.color = color;
	}
	
	/**
	 * Draws the triangle in the OpenGL window
	 */
	public void draw()
    {
        float width= this.width/2; 
        float height = this.height/2;
        GL11.glBegin(GL11.GL_TRIANGLES);
        GL11.glColor3f((float) color.getRed()/255,(float) color.getGreen()/255,(float)color.getBlue()/255);
        GL11.glNormal3f(0f,0f,1f);
        GL11.glVertex3f(width, height, 0);
        GL11.glVertex3f(0, -height, 0);
        GL11.glVertex3f(-width, height, 0);
        GL11.glEnd();
    }
	
}
