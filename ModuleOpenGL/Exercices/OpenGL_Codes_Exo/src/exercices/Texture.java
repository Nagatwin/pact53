package exercices;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

import org.lwjgl.opengl.GL11;

import de.matthiasmann.twl.utils.PNGDecoder;
import de.matthiasmann.twl.utils.PNGDecoder.Format;

public class Texture {
	
	private int textureId;
	private PNGDecoder decoder;
	private ByteBuffer buf;
	
	public Texture(String filename) {
		decoder = null;
		try {
			InputStream in = new FileInputStream(filename);
			decoder = new PNGDecoder(in);
		} catch (Exception e) {
			e.printStackTrace();
		}
		//Store in a buffer
		buf = ByteBuffer.allocateDirect(4 * decoder.getWidth() * decoder.getHeight());
		try {
			decoder.decode(buf, decoder.getWidth() * 4, Format.RGBA);
		} catch (IOException e) {
			e.printStackTrace();
		}
		buf.flip();
		// Create a new OpenGL texture 
		textureId = GL11.glGenTextures();
		// Bind the texture
	}
	
	//bind a texture
	public void bind() {
		//reset the color
		GL11.glColor3f(1f, 1f, 1f);
		GL11.glBindTexture(GL11.GL_TEXTURE_2D, textureId);
		GL11.glTexImage2D(GL11.GL_TEXTURE_2D, 0, GL11.GL_RGBA, decoder.getWidth(),
			    decoder.getHeight(), 0, GL11.GL_RGBA, GL11.GL_UNSIGNED_BYTE, buf);
		GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MIN_FILTER, GL11.GL_NEAREST);
        GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MAG_FILTER, GL11.GL_NEAREST);
	}
	
	//unbind 
	public void unbind() {
        GL11.glBindTexture(GL11.GL_TEXTURE_2D, 0);
	}
	
}
