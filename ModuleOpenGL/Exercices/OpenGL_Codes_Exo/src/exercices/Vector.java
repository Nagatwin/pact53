package exercices;

public class Vector {
	public float x;
	public float y;
	public float z;
	
	//Constructor
	public Vector(float x, float y, float z) {
		this.x =x;
		this.y =y;
		this.z = z;
	}
	
	public String toString() {
		return x+" "+y+" "+z;
	}
}