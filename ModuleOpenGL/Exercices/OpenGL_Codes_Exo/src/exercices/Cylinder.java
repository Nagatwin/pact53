package exercices;

import java.awt.Color;

import org.lwjgl.opengl.GL11;

/**
 * Draws a cylinder.
 * @author Edward Tombre
 *
 */
public final class Cylinder {
	/**
	 * The radius of the upper side.
	 * It can't be modified
	 */
	private final float radiusUp;
	/**
	 * The radius of the bottom side.
	 * It can't be modified
	 */
	private final float radiusDown;
	/**
	 * The half-length of the cylinder.
	 * It can't be modified
	 */
	private final float halfLength;
	/**
	 * The number of slices to cut.
	 * <p> A higher amount of slices will improve the quality of the cylinder, but will need more time to be drawn. </p>
	 */
	private final int slices;
	/**
	 * The color of the cylinder
	 */
	private final Color color;
	
	/**
	 * Construcs a new cylinder
	 * @param radiusUp The upper side radius
	 * @param radiusDown The bottom side radius
	 * @param halfLength The half-length of the cylinder
	 * @param slices The number of slices
	 * @param color The color of the cylinder
	 */
	public Cylinder(float radiusUp, float radiusDown, float halfLength, int slices, Color color) {
		super();
		this.radiusUp = radiusUp;
		this.radiusDown = radiusDown;
		this.halfLength = halfLength;
		this.slices = slices;
		this.color = color;
	}

	public Cylinder(float radius, float halfLength, int slices, Color color) {
		this(radius, radius, halfLength, slices, color);
	}

	/**
	 * Draw the cylinder in the OpenGL window
	 */
	public void draw() {
		for (int i=0;i<slices;i++) {
			float theta =(float) (2*Math.PI*i/slices);
			float nextTheta = (float) (2*Math.PI*(i+1)/slices);
			GL11.glBegin(GL11.GL_TRIANGLE_STRIP);
			GL11.glColor3f((float) color.getRed()/255,(float) color.getGreen()/255,(float)color.getBlue()/255);
			//face up
			GL11.glNormal3f(0f, 0f, 1f);
			GL11.glVertex3f(0, halfLength, 0);
			GL11.glVertex3f(radiusUp*(float)Math.cos(theta), halfLength, radiusUp*(float)Math.sin(theta));
			GL11.glVertex3f(radiusUp*(float)Math.cos(nextTheta), halfLength, radiusUp*(float)Math.sin(nextTheta));
			// side
			GL11.glNormal3f((float) Math.cos(theta), (float) Math.sin(theta),0f);
			GL11.glVertex3f(radiusDown*(float)Math.cos(theta), -halfLength, radiusDown*(float)Math.sin(theta));
			GL11.glVertex3f(radiusDown*(float)Math.cos(nextTheta), -halfLength, radiusDown*(float)Math.sin(nextTheta));
			//face down
			GL11.glNormal3f(0, 0,1f);
			GL11.glVertex3f(0, halfLength, 0);
			GL11.glEnd();
		}
	}	 
}
