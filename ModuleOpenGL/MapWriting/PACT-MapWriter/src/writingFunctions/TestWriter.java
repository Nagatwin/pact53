package writingFunctions;

import java.io.IOException;

import elements.Box;

public class TestWriter {

	public static void main(String[] args) throws IOException {

		Writer map = new Writer("map_test.txt");
		map.init(1,0);
		map.initWorld(1, "sky_day01_01", "worldspawn");
		
		//Drawing the room
		Texture ft = new Texture("CS_HAVANA/FLOOR02", 1);
		Texture ct = new Texture ("CONCRETE/CONCRETEFLOOR012A",1);
		Texture st = new Texture("TOOLS/TOOLSSKYBOX", 1);
		
		Room room = new Room(new Point(-640,-640,64),1280,1280,400,16,ft, ct, st);
		map.drawRoom(room);
		
		//Drawing some boxes
		Texture bt = new Texture("PROPS/WOODCRATE001A",1);
		Box box1 = new Box(new Point(-500,-50,64), 128,128,128,bt,16, map, map.getIdSolid());
		box1.draw();
		Box box2 = new Box(new Point(-300,-50,64), 128,128,128,bt,16, map, map.getIdSolid());
		box2.draw();
		Box box3 = new Box(new Point(-100,-50,64), 128,128,128,bt,16, map, map.getIdSolid());
		box3.draw();
		Box box4 = new Box(new Point(100,-50,64), 128,128,128,bt,16, map, map.getIdSolid());
		box4.draw();
		Box box5 = new Box(new Point(300,-50,64), 128,128,128,bt,16, map, map.getIdSolid());
		box5.draw();
		Box box6 = new Box(new Point(500,-50,64), 128,128,128,bt,16, map, map.getIdSolid());
		box6.draw();
		Box box7 = new Box(new Point(-450,120,64), 128,128,128,bt,16, map, map.getIdSolid());
		box7.draw();
		Box box8 = new Box(new Point(-150,120,64), 128,128,128,bt,16, map, map.getIdSolid());
		box8.draw();
		
		map.endWorld();
		
		
		//Entities
		map.addPlayerStart(new Point(400,200,75));
		map.addCTStart(new Point(400,200,75));
		
		map.addPlayerStart(new Point(-400,-400,75));
		map.addTStart(new Point(-400,-400,75));
		
		map.buyZoneCT(new Point(-640,100,300), new Point(640,640,64));
		map.buyZoneT(new Point(-640,-640,300), new Point(640,-300,64));
		
		map.end();
		System.out.println("Done");
	}

}
