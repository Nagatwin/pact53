package writingFunctions;

public class Texture {
	private String name;
	private float size;
	
	public Texture(String name, float size) {
		this.name = name;
		this.size = size;
	}
	
	//Getter
	public String getName() {
		return name;
	}
	public float getSize() {
		return size;
	}
}
