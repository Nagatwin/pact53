package writingFunctions;

public class Room {
	private Point origin;
	private int width;
	private int length;
	private int height;
	private int light;
	private Texture floorTexture;
	private Texture wallFrontTexture;
	private Texture wallRightTexture;
	private Texture wallLeftTexture;
	private Texture wallRearTexture;
	private Texture ceilingTexture;
	
	public Room(Point o, int w, int l, int h,int li, Texture f, Texture wf, Texture wr, Texture wl, Texture wre, Texture c) {
		origin = o;
		width = w;
		length = l;
		height =h;
		light =li;
		floorTexture =f;
		wallFrontTexture = wf;
		wallRightTexture = wr;
		wallLeftTexture = wl;
		wallRearTexture = wre;
		ceilingTexture = c;
	}
	
	public Room(Point o, int w, int l, int h,int li, Texture f, Texture wa, Texture c) {
		origin = o;
		width = w;
		length = l;
		light =li;
		height =h;
		floorTexture =f;
		wallFrontTexture = wa;
		wallRightTexture = wa;
		wallLeftTexture = wa;
		wallRearTexture = wa;
		ceilingTexture = c;
	}

	public Point getOrigin() {
		return origin;
	}

	public int getWidth() {
		return width;
	}

	public int getLength() {
		return length;
	}

	public int getHeight() {
		return height;
	}

	public int getLight() {
		return light;
	}
	public Texture getFloorTexture() {
		return floorTexture;
	}

	public Texture getWallFrontTexture() {
		return wallFrontTexture;
	}

	public Texture getWallRightTexture() {
		return wallRightTexture;
	}

	public Texture getWallLeftTexture() {
		return wallLeftTexture;
	}

	public Texture getWallRearTexture() {
		return wallRearTexture;
	}

	public Texture getCeilingTexture() {
		return ceilingTexture;
	}
	
	//Getter
	
}
