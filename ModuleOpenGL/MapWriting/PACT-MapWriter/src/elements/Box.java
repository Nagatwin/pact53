package elements;

import writingFunctions.Point;
import writingFunctions.Texture;
import writingFunctions.Writer;

public class Box {
	private Side up;
	private Side bottom;
	private Side left;
	private Side right;
	private Side front;
	private Side rear;
	private Writer writer;
	private int id;
	
	
	
	public Box(Side up, Side bottom, Side left, Side right, Side front, Side rear, Writer writer, int id) {
		super();
		this.up = up;
		this.bottom = bottom;
		this.left = left;
		this.right = right;
		this.front = front;
		this.rear = rear;
		this.writer = writer;
		this.id = id;
	}

	public Box(Point p1, Point p7, Texture upT, Texture downT, Texture leftT, Texture rightT, Texture rearT, Texture frontT, int light, Writer writer, int id) {
		//Computing all the other points
		Point p2 = new Point(p7.getX(), p1.getY(), p1.getZ());
		Point p3 = new Point(p7.getX(), p1.getY(), p7.getZ());
		Point p4 = new Point(p1.getX(), p1.getY(), p7.getZ());
		Point p5 = new Point(p1.getX(), p7.getY(), p1.getZ());
		Point p6 = new Point(p7.getX(), p7.getY(), p1.getZ());
		Point p8 = new Point(p1.getX(), p7.getY(), p7.getZ());
		
		//Computing the u/v axis
		Point uaxis1 = new Point(1,0,0);
		Point vaxis1 = new Point(0,-1,0);
		Point uaxis2 = new Point(0,1,0);
		Point vaxis2 = new Point(0,0,-1);
		
		//Computing the sides then
		Side up = new Side (p5,p6,p2, upT,uaxis1, vaxis1, 0, light,0, writer);
		Side bottom = new Side (p4,p3,p7, downT,uaxis1, vaxis1, 0, light,0, writer);
		Side left = new Side (p5,p1,p4, leftT,uaxis2, vaxis2, 0, light,0, writer);
		Side right = new Side (p7,p3,p2, rightT,uaxis2, vaxis2, 0, light,0, writer);
		Side front = new Side (p3,p4,p1, rearT,uaxis1, vaxis2,0, light,0, writer);
		Side rear = new Side (p6,p5,p8, frontT,uaxis1, vaxis2, 0, light,0, writer);
		
		//Finally define the box
		this.up = up;
		this.bottom = bottom;
		this.left = left;
		this.right = right;
		this.front = front;
		this.rear = rear;
		this.writer = writer;
		this.id = id;
	}
	
	public Box(Point p1, Point p7, Texture texture, int light, Writer writer, int id) {
		this(p1,p7,texture, texture, texture, texture, texture, texture, light, writer, id);
	}
	
	// Defines a box from the origin, and width, length, height
	public Box(Point origin, int width, int length, int height, Texture up, Texture down, Texture left, Texture right, Texture rear, Texture front, int light, Writer writer, int id) {
		this(new Point(origin.getX(),origin.getY(), origin.getZ()+height),
			 new Point(origin.getX()+width, origin.getY()+length, origin.getZ()),
			 up, down, left, right, rear, front, light, writer, id);
	}
	public Box(Point origin, int width, int length, int height, Texture texture, int light, Writer writer, int id) {
		this(origin, width, length, height, texture, texture, texture, texture, texture, texture, light, writer, id);
	}

	
	public void draw() {
		writer.getPrint().println("solid\r\n" + "{\r\n" + "\"id\" \""+ id +"\"");
		writer.incrementsIdSolid();
		up.draw();
		bottom.draw();
		left.draw();
		right.draw();
		rear.draw();
		front.draw();
		writer.getPrint().println("}");
	}
	

	
}
