package elements;

import writingFunctions.Point;
import writingFunctions.Texture;
import writingFunctions.Writer;

public class Side {
	private Point p1; 
	private Point p2; 
	private Point p3;
	private Texture texture;
	private Point uaxis;
	private Point vaxis;
	private int rotation;
	private int light;
	private int smoothing;
	private Writer writer;
	private int id;
	//Constructor
	public Side(Point p1, Point p2, Point p3, Texture texture, Point uaxis, Point vaxis, int rotation, int light, int smoothing, Writer writer) {
		this.p1 = p1;
		this.p2 = p2;
		this.p3 = p3;
		this.texture = texture;
		this.uaxis = uaxis;
		this.vaxis = vaxis;
		this.rotation = rotation;
		this.light = light;
		this.smoothing = smoothing;
		this.writer = writer;
		this.id = writer.getIdSide();
	}
	

	//Getter
	public Point getP1() {
		return p1;
	}
	
	public Point getP2() {
		return p2;
	}
	public Point getP3() {
		return p3;
	}
	public Texture getTexture() {
		return texture;
	}
	
	public Point getUaxis() {
		return uaxis;
	}

	public Point getVaxis() {
		return vaxis;
	}
	public int getRotation() {
		return rotation;
	}
	public int getLight() {
		return light;
	}
	public int getSmoothing() {
		return smoothing;
	}
	public Writer getWriter() {
		return writer;
	}
	
	public void draw() {
		writer.getPrint().println("side\r\n" + "{");
		writer.getPrint().println("	\"id\" \""+id + "\"");
		writer.incrementsIdSide();
		writer.getPrint().println("	\"plane\" \"("+p1.toString()+") ("+p2.toString()+") ("+p3.toString()+")\"");
		writer.getPrint().println("	\"material\" \""+texture.getName()+"\"");
		writer.getPrint().println("	\"uaxis\" \"["+uaxis.toString() +" 0] "+texture.getSize()+"\"\r\n" + "	\"vaxis\" \"["+vaxis.toString()+" 0] 0.25\"");
		writer.getPrint().println("	\"rotation\" \""+rotation+"\"");
		writer.getPrint().println("	\"lightmapscale\" \""+ light +"\"");
		writer.getPrint().println("	\"smoothing_groups\" \""+smoothing+"\"");
		writer.getPrint().println("}");
	}
	
	public void drawBasic() {
		writer.getPrint().println("side\r\n" + "{");
		writer.getPrint().println("	\"id\" \""+id + "\"");
		writer.incrementsIdSide();
		writer.getPrint().println("	\"plane\" \"("+p1.toString()+") ("+p2.toString()+") ("+p3.toString()+")\"");
		writer.getPrint().println("}");
		}
}
