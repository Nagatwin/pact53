	solid
	{
		"id" "2"
		side
		{
			"id" "1"
			"plane" "(-640 448 64) (640 448 64) (640 -512 64)"
			"material" "CS_HAVANA/FLOOR02"
			"uaxis" "[1 0 0 0] 0.25"
			"vaxis" "[0 -1 0 0] 0.25"
			"rotation" "0"
			"lightmapscale" "16"
			"smoothing_groups" "0"
		}
		side
		{
			"id" "2"
			"plane" "(-640 -512 0) (640 -512 0) (640 448 0)"
			"material" "CS_HAVANA/FLOOR02"
			"uaxis" "[1 0 0 0] 0.25"
			"vaxis" "[0 -1 0 0] 0.25"
			"rotation" "0"
			"lightmapscale" "16"
			"smoothing_groups" "0"
		}
		side
		{
			"id" "3"
			"plane" "(-640 448 64) (-640 -512 64) (-640 -512 0)"
			"material" "CS_HAVANA/FLOOR02"
			"uaxis" "[0 1 0 0] 0.25"
			"vaxis" "[0 0 -1 0] 0.25"
			"rotation" "0"
			"lightmapscale" "16"
			"smoothing_groups" "0"
		}
		side
		{
			"id" "4"
			"plane" "(640 448 0) (640 -512 0) (640 -512 64)"
			"material" "CS_HAVANA/FLOOR02"
			"uaxis" "[0 1 0 0] 0.25"
			"vaxis" "[0 0 -1 0] 0.25"
			"rotation" "0"
			"lightmapscale" "16"
			"smoothing_groups" "0"
		}
		side
		{
			"id" "5"
			"plane" "(640 448 64) (-640 448 64) (-640 448 0)"
			"material" "CS_HAVANA/FLOOR02"
			"uaxis" "[1 0 0 0] 0.25"
			"vaxis" "[0 0 -1 0] 0.25"
			"rotation" "0"
			"lightmapscale" "16"
			"smoothing_groups" "0"
		}
entity
{
	"id" "56"
	"classname" "info_player_start"
	"origin" "128 -256 64"
}