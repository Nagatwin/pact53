
import java.util.ArrayList;

public interface PlanInterface {
	public ArrayList<SegmentInterface> getAllSegments();

	public void save(String fileName);
}
