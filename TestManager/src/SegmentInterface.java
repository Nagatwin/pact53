
public interface SegmentInterface {
	public PointInterface getPoint1();

	public PointInterface getPoint2();

	public boolean isWall();

	public boolean isDoor();

	public boolean isWindow();
}
