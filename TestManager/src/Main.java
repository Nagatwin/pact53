import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import gui.MainFrame;
import tests.XMLFile;;

public class Main {
	public static void main(String[] args) {
		MainFrame mainFrame = new MainFrame();
		XMLFile file = new XMLFile("data/integration.xml");
		file.loadModules();
		mainFrame.getModel().setModules(file.getModules());
	}

}
