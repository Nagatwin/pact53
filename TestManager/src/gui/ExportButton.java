package gui;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import tests.Module;
import tests.Test;

public class ExportButton extends JButton implements ActionListener{
private MainFrame mainFrame;
	
	public ExportButton(MainFrame mainFrame) {
		super("Export Tests");
		this.mainFrame = mainFrame;
		addActionListener(this);
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		JTextField motive = new JTextField();
		JCheckBox xml = new JCheckBox("Save to XML", true);
		JCheckBox adoc = new JCheckBox("Save to ADOC", true);
		JPanel modulePanel = new JPanel();
		JButton selectAll = new JButton("Select All");
		selectAll.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				for (Component checkbox : modulePanel.getComponents()) {
					((JCheckBox) checkbox).setSelected(true);
				}
			}
		});
		JButton deselectAll = new JButton("Deselect All");
		deselectAll.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				for (Component checkbox : modulePanel.getComponents()) {
					((JCheckBox) checkbox).setSelected(false);
				}
			}
		});
		JPanel buttonPanel = new JPanel();
		buttonPanel.setLayout(new BoxLayout(buttonPanel, BoxLayout.LINE_AXIS));
		buttonPanel.add(new JLabel("Modules � exporter : "));
		buttonPanel.add(selectAll);
		buttonPanel.add(deselectAll);
		modulePanel.setLayout(new BoxLayout(modulePanel, BoxLayout.PAGE_AXIS));
		ArrayList<Module> modules =  mainFrame.getModel().getModules();
		for (Module module : modules) {
			modulePanel.add(new JCheckBox(module.getName(), true));
		}
		Object[] message = { "Commentaire :", motive, xml, adoc, buttonPanel, modulePanel};
		int option = JOptionPane.showConfirmDialog(null, message, "Export", JOptionPane.OK_CANCEL_OPTION,
				JOptionPane.PLAIN_MESSAGE);
		if (option == JOptionPane.OK_OPTION) {
			for (int i=0;i<modules.size();i++) {
				JCheckBox checkbox= (JCheckBox) modulePanel.getComponent(i);
				if (checkbox.isSelected()) {
					modules.get(i).addDate(motive.getText()+"@"+LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd-MM HH:mm")));
					for(Test test : modules.get(i).getAllTest()) {
						test.getData().addDate(test.getState());
					}
				}
			}
			if (adoc.isSelected()) {
				try {
					mainFrame.getModel().exportToAdocFile("data/projet.adoc");
				} catch (IOException e) {
					JOptionPane.showMessageDialog( new JFrame(),
							    "Could not export to ADOC : an error occured.",
							    "Adoc Error",
							    JOptionPane.WARNING_MESSAGE);
					e.printStackTrace();
				}
			}
			
			if (xml.isSelected()) {
				try {
					mainFrame.getModel().exportToXMLFile("data/projet.xml");
				} catch (IOException e) {
					JOptionPane.showMessageDialog( new JFrame(),
						    "Could not export to XML : an error occured.",
						    "Adoc Error",
						    JOptionPane.WARNING_MESSAGE);
					e.printStackTrace();
				}
			}
		}
	}
}
