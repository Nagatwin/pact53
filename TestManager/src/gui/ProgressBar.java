package gui;

import javax.swing.JProgressBar;

public class ProgressBar extends JProgressBar{
	private MainFrame mainFrame;
	
	public ProgressBar(MainFrame mainFrame, int length) {
		super(0, length);
		this.mainFrame = mainFrame;
		setValue(0);
		setStringPainted(true);
	}	
	
	public void update() {
		this.setMaximum(mainFrame.getModel().getTests().size());
		setValue(mainFrame.getModel().getProgress());
	}

	public void reset() {
		mainFrame.getModel().setProgress(0);
	}
	
	public void load() {
		
	}
}

