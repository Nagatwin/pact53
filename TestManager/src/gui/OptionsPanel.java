package gui;

import javax.swing.BoxLayout;
import javax.swing.JPanel;

public class OptionsPanel extends JPanel {

	private AutoTestStart autoTestStart;
	private ManualTestStart manualTestStart;
	private ExportButton export;
	
	public OptionsPanel(MainFrame mainFrame) {
		super();
		this.autoTestStart = new AutoTestStart(mainFrame);
		this.manualTestStart = new ManualTestStart(mainFrame);
		this.export = new ExportButton(mainFrame);
		
		this.setLayout(new BoxLayout(this, BoxLayout.LINE_AXIS));
		this.add(autoTestStart);
		this.add(manualTestStart);
		this.add(export);
	}
	
	public void update() {
		autoTestStart.update();
	}
}
