package gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

import tests.Test;

public class ManualTestStart extends JButton implements ActionListener{
	private MainFrame mainFrame;
	
	public ManualTestStart(MainFrame mainFrame) {
		super("Begin Manual Test");
		this.mainFrame = mainFrame;
		addActionListener(this);
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		if (!mainFrame.getModel().getManualTest()) {
			//Launches the manual test
			setText("Stop Test");
			mainFrame.getModel().setManualTest(true);
			mainFrame.getModel().setProgressInfo("Manual testing");
			//Counting the number of tests that have been auto tested
			int autoTested = 0;
			for (Test test : mainFrame.getModel().getTests()) {
				if (test.getState() < 2) {
					autoTested++;
				}
			}
			mainFrame.getModel().setProgress(autoTested);
			mainFrame.getModel().changed();
		}
		else {
			setText("Begin Manual Test");
			mainFrame.getModel().setManualTest(false);
		}
	}
	
	public void update() {
		
	}
}
