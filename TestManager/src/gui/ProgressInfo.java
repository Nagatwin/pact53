package gui;

import javax.swing.JLabel;

public class ProgressInfo extends JLabel {
	private MainFrame mainFrame;
	
	public ProgressInfo(MainFrame mainFrame) {
		super();
		this.mainFrame = mainFrame;
		this.setAlignmentX(CENTER_ALIGNMENT);
	}
	
	public void update() {
		setText(mainFrame.getModel().getProgressInfo());
	}

}
