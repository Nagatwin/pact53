package gui;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Observable;

import tests.Module;
import tests.Test;

public class Model extends Observable{
	
	private ArrayList<Module> modules;
	private Module selectedModule;
	private String progressInfo;
	private String buttonText;
	private int progress;
	private boolean manualTest = false;

	
	public Model(MainFrame mainFrame) {
		super();
		this.modules = new ArrayList<Module>();
		this.selectedModule = null;
		this.progressInfo = "Aucune t�che en cours";
		this.buttonText = "Start Auto Test";
		this.progress = 0;
		this.addObserver(mainFrame);
	}
	
	protected void changed() {
		setChanged();
		notifyObservers();
	}
	
	public boolean getManualTest() {
		return manualTest;
	}
	public void setManualTest(boolean bool) {
		manualTest = bool;
		changed();
	}
	public String getButtonText() {
		return buttonText;
	}
	
	public void setButtonText(String text) {
		buttonText = text;
	}
	public String getProgressInfo() {
		return progressInfo;
	}
	
	public int getProgress() {
		return progress;
	}
	
	public void setProgressInfo(String progressInfo) {
		this.progressInfo = progressInfo;
		changed();
	}
	
	public void setProgress(int progress) {
		this.progress = progress;
	}
	
	public ArrayList<Module> getModules(){
		return modules;
	}
	
	public void setModules(ArrayList<Module> modules) {
		this.modules = modules;
		changed();
	}
	
	public ArrayList<Test> getTests(){
		ArrayList<Test> result = new ArrayList<Test>();
		for (Module module : modules) {
			result.addAll(module.getAllTest());
		}
		return result;
	}
	
	public Module getSelectedModule() {
		return selectedModule;
	}
	
	public void setSelectedModuleQuiet(Module module) {
		selectedModule = module;
	}
	
	public void setSelectedModule(Module module) {
		selectedModule = module;
		changed();
	}
	public void addModule(Module module) {
		this.modules.add(module);
		setSelectedModule(module);
	}
	
	public void addModules(ArrayList<Module> modules) {
		this.modules.addAll(modules);
		setSelectedModule(modules.get(modules.size()-1));
	}
	
	public void stopAutoTest(String message) {
		progressInfo = message;
		buttonText = "Start Auto Test";
		for (Test test : getTests()) {
			test.setTested(false);
		}
		changed();
	}
	
	////// ADOC
	public final void exportToAdocFile(String fileName) throws IOException {
		File f = new File(fileName);
		if (f.exists())
			f.delete();
		f.createNewFile();
		PrintWriter pw = new PrintWriter(fileName);
		pw.print(toAdocString());
		pw.close();
	}

	public String toAdocString() {
		final String nl = System.getProperty("line.separator");
		String result = "= Tests du projet Map-it"+nl+nl;
		for (Module module : modules)
			result += module.toAdocString() + nl;
		for (Module module : modules){
			result += module.toAdocInformation()+nl;
		}
		return (result.length() != 0) ? result.substring(0, result.length() - 1) : "";
	}
	
	public String toXMLFile() {
		final String nl = System.getProperty("line.separator");
		String result = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" +nl +"<projet>"+nl;
		for(Module module : modules) {
			result+=module.toXMLFile();
		}
		result+="</projet>"+nl;
		return result;
	}
	
	public final void exportToXMLFile(String fileName) throws IOException {
		File f = new File(fileName);
		if (f.exists())
			f.delete();
		f.createNewFile();
		PrintWriter pw = new PrintWriter(fileName);
		pw.print(toXMLFile());
		pw.close();
	}
	
}
