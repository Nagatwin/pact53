package gui;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.lang.reflect.Method;
import java.util.ArrayList;

import javax.swing.BoxLayout;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JTextField;

import tests.AutoTest;
import tests.ManualTest;
import tests.Module;
import tests.Test;
import tests.TestData;

public class AddTestMenu extends JPopupMenu implements ActionListener {
	private static final long serialVersionUID = 1L;
	private MainFrame mainFrame;
	private JMenuItem addTestItem;
	private JMenuItem addSubTestItem;
	private JMenuItem removeItem;
	private Module module;
	private Test test;
	
	public AddTestMenu(	MainFrame mainFrame, Module module, Test test) {
		super();
		this.mainFrame = mainFrame;
		this.module = module;
		this.test = test;
        addTestItem = new JMenuItem("Add test");
        addTestItem.addActionListener(this);
        add(addTestItem);
        addSubTestItem = new JMenuItem("Add sub test");
        addSubTestItem.addActionListener(this);
        add(addSubTestItem);
        removeItem = new JMenuItem("Remove Test");
        removeItem.addActionListener(this);
        add(removeItem);
	}
	
	public AddTestMenu(MainFrame mainFrame, Module module) {
		super();
		this.mainFrame = mainFrame;
		this.module = module;
		this.test = null;
        addTestItem = new JMenuItem("Add test");
        addTestItem.addActionListener(this);
        add(addTestItem);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource().equals(addTestItem)) showConfirmation(false);
		else if (e.getSource().equals(addSubTestItem))showConfirmation(true);
		else askConfirmation();
	}

	private void askConfirmation() {
		int option = JOptionPane.showConfirmDialog(null, "Remove this test ?", "Remove Test", JOptionPane.OK_CANCEL_OPTION,
				JOptionPane.PLAIN_MESSAGE);
		if (option == JOptionPane.OK_OPTION) {
			test.remove();
			mainFrame.getModel().changed();
		}
	}

	public void showConfirmation(boolean bool) {
		JTextField name = new JTextField();
		JTextField description = new JTextField();
		JTextField data = new JTextField();
		JTextField output = new JTextField();
		JPanel autoPanel = new JPanel();
		JTextField classe = new JTextField();
		JTextField method = new JTextField();
		autoPanel.setLayout(new BoxLayout(autoPanel, BoxLayout.PAGE_AXIS));
		autoPanel.setAlignmentX(LEFT_ALIGNMENT);
		autoPanel.add(new JLabel("Class:"));
		autoPanel.add(classe);
		autoPanel.add(new JLabel("Method:"));
		autoPanel.add(method);
		JCheckBox auto = new JCheckBox("Automatic Test", true);
		auto.addItemListener(new java.awt.event.ItemListener() {
            @Override
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                if (evt.getStateChange() == ItemEvent.SELECTED) {
                	for (Component element : autoPanel.getComponents()) {
                    	element.setEnabled(true);
                    }

                } else {
                    for (Component element : autoPanel.getComponents()) {
                    	element.setEnabled(false);
                    }
                }
            }
        });
		Object[] message = { "Name:", name, "Description:", description, "Data:", data, "Output:", output, auto, autoPanel};
		String text = "Add " + (bool ? "sub " : "") + "test";
		int option = JOptionPane.showConfirmDialog(null, message, text, JOptionPane.OK_CANCEL_OPTION,
				JOptionPane.PLAIN_MESSAGE);
		if (option == JOptionPane.OK_OPTION) {
			//maj dates
			ArrayList<Integer> dates = new ArrayList<Integer>();
			for (String date : module.getDates()) {
				dates.add(2);
			}
			TestData dataTest = new TestData(name.getText(), description.getText(), data.getText(), output.getText(), dates);
			Test result = null;
			//Testing if the test is auto
			if (auto.isSelected()) {
				String methodName = method.getText();
				Class<?> obj;
				try {
					obj = Class.forName(classe.getText());
					Method testMethod = obj.newInstance().getClass().getMethod(methodName);
					result = new AutoTest(dataTest, testMethod, obj);
				} catch (NoSuchMethodException | SecurityException | ClassNotFoundException | InstantiationException | IllegalAccessException e) {
					JOptionPane.showMessageDialog( new JFrame(),
						    "Could not create an auto Test : creating a Manual Test instead.",
						    "AutoTest Error",
						    JOptionPane.WARNING_MESSAGE);
					result = new ManualTest(dataTest);
				}
			}
			else {
				result = new ManualTest(dataTest);
			}
			//Testing if subTest
			if (bool) {
				result.setModule(module);
				result.setParent(test);
				test.addSubTest(result);
			}
			else {
				if (test == null || test.getParent() == null) {
					result.setModule(module);
					module.addTest(result,module.getTests().indexOf(test)+1);
				}
				else {
					result.setModule(module);
					result.setParent(test.getParent());
					test.getParent().addSubTest(result, test.getParent().getSubTests().indexOf(test)+1);
				}
			}
			mainFrame.getModel().changed();
		}
	}
}
