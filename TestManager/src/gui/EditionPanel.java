package gui;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JPanel;

public class EditionPanel extends JPanel {
	private static final long serialVersionUID = 1L;
	
	private MainFrame mainFrame;
	private AddModuleButton addModuleButton;
	
	
	public EditionPanel(MainFrame mainFrame) {
		super();
		this.mainFrame = mainFrame;
		this.addModuleButton = new AddModuleButton(mainFrame);
		
		this.setLayout(new BoxLayout(this, BoxLayout.LINE_AXIS));
		add(addModuleButton);
	}
	
	public void update() {
		repaint();
	}

}
