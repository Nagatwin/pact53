package gui;

import java.beans.PropertyChangeEvent;
import java.util.ArrayList;

import javax.swing.SwingWorker;

import tests.Test;

public class TestTask extends SwingWorker {
    private MainFrame mainFrame;
    
    public TestTask(MainFrame mainFrame) {
    	super();
    	this.mainFrame = mainFrame;
    }
	
    @Override
    public Void doInBackground() {
		setProgress(0);
		mainFrame.getModel().setButtonText("Annuler");
		//Fais la t�che
		ArrayList<Test> testList = mainFrame.getModel().getTests();
		int length = testList.size();
		for (int i=0;i<length;i++) {
			if (isCancelled()) return null;
			//mise � jour du test 
			mainFrame.getModel().setProgressInfo( "Execution du test : " +testList.get(i).getData().getName());
			if (!testList.get(i).isTested()) {
				//Maj icone
				testList.get(i).setState(3);
				mainFrame.getModel().changed();
				int passed = testList.get(i).doTest();
				testList.get(i).setState(passed);
				testList.get(i).setTested(true);	
			}
			setProgress(i+1);
			mainFrame.getModel().setProgress(i+1);
		}
		//Finis la t�che
		return null;
    }

    @Override
    public void done() {
    	setProgress(0);
    	mainFrame.getModel().stopAutoTest(isCancelled() ? "Annulation" : "Les tests sont termin�s");
    }
    

}

