package gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

public class AutoTestStart extends JButton implements ActionListener{
	private MainFrame mainFrame;
	private TestTask testTask;
	
	public AutoTestStart(MainFrame mainFrame) {
		super("Start Auto Test");
		this.mainFrame = mainFrame;
		this.testTask = new TestTask(mainFrame);
		addActionListener(this);
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		//Launch the task if it is not launch
		if (testTask.getProgress()==0)
		{
			testTask = new TestTask(mainFrame);
			testTask.execute();
		}
		else 
		{
			testTask.cancel(true);
		}
	}
	
	public void update() {
		setText(mainFrame.getModel().getButtonText());
		if (mainFrame.getModel().getManualTest()) this.setEnabled(false);
		else this.setEnabled(true);
	}
}
