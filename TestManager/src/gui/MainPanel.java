package gui;

import java.util.ArrayList;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;

import tests.Module;
import tests.TestBidon;
import tests.TestData;

public class MainPanel extends JPanel {
	private static final long serialVersionUID = 7300329368602187776L;
	
	private MainFrame mainFrame;
	private TabbedPanel tabbedPanel;
	private EditionPanel editionPanel;
	private ProgressBar progressBar;
	private ProgressInfo progressInfo;
	private OptionsPanel optionsPanel;
	
	public MainPanel(MainFrame mainFrame) {
		super();
		this.mainFrame = mainFrame;
		
		tabbedPanel = new TabbedPanel(mainFrame);
		editionPanel = new EditionPanel(mainFrame);
		progressBar = new ProgressBar(mainFrame, mainFrame.getModel().getTests().size());
		progressInfo = new ProgressInfo(mainFrame);
		optionsPanel = new OptionsPanel(mainFrame);
		
		this.setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
		
		this.add(Box.createVerticalStrut(10));
		this.add(tabbedPanel);
		this.add(Box.createVerticalStrut(10));
		this.add(editionPanel);
		this.add(Box.createVerticalStrut(10));
		this.add(progressBar);
		this.add(Box.createVerticalStrut(10));
		this.add(progressInfo);
		this.add(Box.createVerticalStrut(30));
		this.add(optionsPanel);
	}
	
	public void update() {
		tabbedPanel.update();
		editionPanel.update();
		progressBar.update();
		progressInfo.update();
		optionsPanel.update();
	}
}
