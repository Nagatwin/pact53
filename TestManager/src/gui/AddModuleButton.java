package gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JOptionPane;

import tests.Module;

public class AddModuleButton extends JButton implements ActionListener {
	private static final long serialVersionUID = 1L;
	private final MainFrame mainFrame;

	protected AddModuleButton(MainFrame mainFrame) {
		super("Add module");
		this.mainFrame = mainFrame;
		addActionListener(this);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		String name = JOptionPane.showInputDialog(this, "Module name");
		
		if (name == null) return;
		if(name.replaceAll(" ", "").length()== 0 ) JOptionPane.showMessageDialog(mainFrame,	
				    "Impossible de cr�er le module : le nom n'est pas valide.",
				    "Erreur Module",
				    JOptionPane.ERROR_MESSAGE);
		else mainFrame.getModel().addModule(new Module(name));
	
		
	}
}
