package gui;

import java.awt.Dimension;
import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JFrame;

import tests.Module;
import tests.TestBidon;
import tests.TestData;

public class MainFrame extends JFrame implements Observer{
	private static final long serialVersionUID = 1L;

	private final Model model;
	private MainPanel mainPanel;
	
	public MainFrame() {
		super("Test Manager");
	

		
		//Model
		model = new Model(this);
		mainPanel = new MainPanel(this);
		
		//Creation d'un module
		/*Module module = new Module("Module bidon");
		TestBidon test1 = new TestBidon( new TestData("Test bidon", "Il ne sert � rien", "rien", "pas grand chose"), module);
		TestBidon sousTest1 = new TestBidon (new TestData("Sous test bidon", "Il ne sert pas � plus", "rien", "tout"), module, test1);
		test1.addSubTest(sousTest1);
		module.addTest(test1);
		model.addModule(module);*/
		
		//Content
		setContentPane(mainPanel);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setLocation(100, 150);
		setPreferredSize(new Dimension(1200,800));
		pack();
		setVisible(true);	
	}
	
	//Getters and Setters
	
	@Override
	public void update(Observable arg0, Object arg1) {
		mainPanel.update();
	}

	public Model getModel() {
		return model;
	}	
}
