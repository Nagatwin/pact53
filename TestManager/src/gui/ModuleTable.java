package gui;

import java.awt.Dimension;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingUtilities;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;

import tests.Module;
import tests.Test;

public class ModuleTable extends JTable {
	private static final long serialVersionUID = 1L;
	private final static String[] columnNames = {"Module", "Auto", "Test","Result"};
	
	private MainFrame mainFrame;
	private final Module module;
	private final JScrollPane scrollPane;
	
	public ModuleTable(MainFrame mainFrame, Module module) {
		super();
		//Changing the model for icons
		DefaultTableModel model = new DefaultTableModel(columnNames, 0) {
			@Override
			public boolean isCellEditable(int row, int column)
			   {
			     return false;
			   }
		     @Override
		     public Class<?> getColumnClass(int column) {
		        if (getRowCount() > 0) {
		           Object value = getValueAt(0, column);
		           if (value != null) {
		              return getValueAt(0, column).getClass(); 
		           }
		        }
		        return super.getColumnClass(column);
		     }
		};
		for (Object[] line : module.toTable()) { 
			model.addRow(line);
		}
		this.setModel(model);
		this.setRowHeight(30);
		this.mainFrame = mainFrame;
		this.module = module;
		this.scrollPane = new JScrollPane(this);
		this.setPreferredScrollableViewportSize(new Dimension(600,300));
		this.setFillsViewportHeight(true);
		//Mouse Listener
		this.addMouseListener(new MouseAdapter() {
		    @Override
		    public void mouseClicked(MouseEvent evt) {
		    	//Si on est en test manuel
		    	if (mainFrame.getModel().getManualTest()) {
		    		int row = rowAtPoint(evt.getPoint());
		    		if (row>=0) {
		    			Test currentTest = (Test) getValueAt(row,2);
		    			if (currentTest.getState()>=2) {
		    				mainFrame.getModel().setProgress(mainFrame.getModel().getProgress()+1);
		    			}
		    			currentTest.setState((currentTest.getState()+1)%2);
		    			mainFrame.getModel().changed();
		    		}
		    	}
		    	//Si click droit
		    	else if(SwingUtilities.isRightMouseButton(evt)) {
		   
			        int row = rowAtPoint(evt.getPoint());
			        if (row >= 0) {
			        	Module currentModule = (Module) getValueAt(row,0);
			        	Test currentTest = (Test) getValueAt(row,2);
			        	AddTestMenu addTestMenu = new AddTestMenu(mainFrame,currentModule, currentTest);
			        	addTestMenu.show(evt.getComponent(), evt.getX(), evt.getY());
			        }
			        else {
			        	AddTestMenu addTestMenu = new AddTestMenu(mainFrame, module);
			        	addTestMenu.show(evt.getComponent(), evt.getX(), evt.getY());
			        }
		        }
		    }
		});
	}

	public JScrollPane getScrollPane() {
		return scrollPane;
	}
	
	public Module getModule() {
		return module;
	}
	
}
