package gui;

import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import tests.Module;

public class TabbedPanel extends JTabbedPane implements ChangeListener{
	private MainFrame mainFrame;
	private boolean isBuilding = false;
	
	public TabbedPanel(MainFrame mainFrame) {
		this.mainFrame = mainFrame;
		this.build();
		this.addChangeListener(this);
	}
	
	public void build() {
		isBuilding = true;
		for (Module tab : mainFrame.getModel().getModules() ) {
			ModuleTable moduleTable = new ModuleTable(mainFrame, tab);
			addTab(tab.getName(), moduleTable.getScrollPane());
			if(tab == mainFrame.getModel().getSelectedModule()) setSelectedComponent(moduleTable.getScrollPane());
		}
		isBuilding = false;
	}
	
	public void update() {
		removeAll();
		build();
	}
	
	@Override
	public void stateChanged(ChangeEvent e) {
		JScrollPane scroll = (JScrollPane) getSelectedComponent();
		if (scroll != null && !isBuilding) {
			ModuleTable tab = (ModuleTable) scroll.getViewport().getView();
			mainFrame.getModel().setSelectedModuleQuiet(tab.getModule());   
		}
	}
}
