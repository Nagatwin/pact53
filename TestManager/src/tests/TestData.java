package tests;

import java.util.ArrayList;

public class TestData {
	private static final String nl = System.getProperty("line.separator");
	private final String name, description, data, output;
	private ArrayList<Integer> dates = new ArrayList<Integer>();
	
	public TestData(String name, String description, String data, String output) {
		super();
		this.name = name;
		this.description = description;
		this.data = data;
		this.output = output;
	}
	
	public TestData(String name, String description, String data, String output, ArrayList<Integer> dates) {
		this(name, description, data, output);
		this.dates = dates;
	}
	
	public String getName() {
		return name;
	}
	
	public String getDescription() {
		return description;
	}

	public String getData() {
		return data;
	}

	public String getOutput() {
		return output;
	}
	
	public ArrayList<Integer> getDates() {
		return dates;
	}
	
	public String getDatesString() {
		String result = "";
		for (Integer date : dates) {
			result+= date+",";
		}
		return result.substring(0, result.length()-1);
	}
	
	public void addDate(Integer date) {
		dates.add(date);
	}
	public String toSaveString() {
		return name + "�" + description + "�" + data + "�" + output +"�";
	}

	protected String toExportString(String indent) {
		return name + nl + indent + "Description : " + description + nl + indent + "Data: " + data + nl + indent
				+ "Output : " + output;
	}
}
