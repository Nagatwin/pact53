package tests;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;

public class AutoTest extends Test {
	private Method testMethod;
	private Class<?> object;
	
	public AutoTest() {
		super();
		this.testMethod = null;
		this.object = null;
	}
	public AutoTest(TestData data, Module module, Test parentTest, Method testMethod, Class<?> object) {
		super(data, module, parentTest);
		this.testMethod = testMethod;
		this.object = object;
	}
	public AutoTest(TestData data, Module module) {
		super(data, module);
		// TODO Auto-generated constructor stub
	}
	public AutoTest(TestData data, Method testMethod, Class<?> object) {
		super(data);
		this.testMethod = testMethod;
		this.object = object;
	}
	
	public AutoTest(TestData data) {
		super(data);
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public int doTest() {
		Object result;
		try {
			result = testMethod.invoke(object.newInstance());
		} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException | InstantiationException e) {
			return 2;
		}
		Integer resultInt = (Integer) result;
		return resultInt;
	}
	
	@Override
	public String toXMLFile() {
		final String nl = System.getProperty("line.separator");
		String result = "<test name = \""+getData().getName()+"\">"+nl;
		result += "<description>"+getData().getDescription()+"</description>"+nl
				+"<data>"+getData().getData()+"</data>"+nl
				+"<output>"+getData().getOutput()+"</output>"+nl
				+"<dates>"+getData().getDatesString()+"</dates>"+nl
				+"<class>"+object.getName()+"</class>"+nl
				+"<method>"+testMethod.getName()+"</method>"+nl;
		for (Test test : getSubTests()) {
			result += test.toXMLFile();
		}
		result+= "</test>"+nl;
		return result;
	}

}
