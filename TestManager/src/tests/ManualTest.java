package tests;


import java.util.ArrayList;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class ManualTest extends Test{

	public ManualTest() {
		super();
	}
	
	public ManualTest(TestData data, Module module, ArrayList<Test> subTests, Test parentTest) {
		super(data, module, subTests, parentTest);
	}


	public ManualTest(TestData data, Module module, ArrayList<Test> subTests) {
		super(data, module, subTests);
	}


	public ManualTest(TestData data, Module module, Test parentTest) {
		super(data, module, parentTest);
	}


	public ManualTest(TestData data, Module module) {
		super(data, module);
	}


	public ManualTest(TestData data) {
		super(data);
	}


	@Override
	public int doTest() {
		return 2;
	}

	@Override
	public String toXMLFile() {
		final String nl = System.getProperty("line.separator");
		String result = "<test name = \""+getData().getName()+"\">"+nl;
		result += "<description>"+getData().getDescription()+"</description>"+nl
				+"<data>"+getData().getData()+"</data>"+nl
				+"<output>"+getData().getOutput()+"</output>"+nl
				+"<dates>"+getData().getDatesString()+"</dates>"+nl
				+"<class></class>"+nl;
		for (Test test : getSubTests()) {
			result += test.toXMLFile();
		}
		result+= "</test>"+nl;
		return result;
	}
}
