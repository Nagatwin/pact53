package tests;

import java.util.ArrayList;

import javax.swing.ImageIcon;


public abstract class Test {
	private static final String nl = System.getProperty("line.separator");
	public final static ImageIcon notTested = new ImageIcon("data/icons/notTested.png");
	public final static ImageIcon hasPassed = new ImageIcon("data/icons/passed.png");
	public final static ImageIcon notImplemented = new ImageIcon("data/icons/notImplemented.png");
	public final static ImageIcon notPassed = new ImageIcon("data/icons/notpassed.png");
	public final static ImageIcon testing = new ImageIcon("data/icons/testing.png");
	public final static ImageIcon[] states = {notPassed, hasPassed, notImplemented, testing, notTested};
	public final static String[] colors = {"red", "green", "gray", "blue", "black"};
	
	private TestData data;
	private int state;
	private boolean tested;
	private Test parentTest;
	private Module module;
	private ArrayList<Test> subTests; 
	
	public Test() {
		this.data = null;
		state = 4;
		tested = false;
		this.subTests = new ArrayList<Test>();
		this.parentTest = null;
		this.module = null;
	}
	public Test(TestData data) {
		this();
		this.data = data;
	}
	
	public Test(TestData data, Module module) {
		this(data);
		this.module = module;
	}
	
	public Test(TestData data, Module module, ArrayList<Test> subTests) {
		this(data, module);
		this.subTests = subTests;
	}
	
	public Test(TestData data, Module module, Test parentTest) {
		this(data, module);
		this.parentTest = parentTest;
	}
	
	public Test(TestData data, Module module, ArrayList<Test> subTests, Test parentTest) {
		this(data, module, subTests);
		this.parentTest = parentTest;
	}	
	//Getter et Setters
	public String indent() {
		String result = "";
		Test tampon = this;
		while (tampon.parentTest != null) {
			result += "         ";
			tampon = tampon.parentTest;
		}
		return result;
	}
	public void setParent(Test parent) {
		this.parentTest = parent;
	}
	
	public void setModule(Module module) {
		this.module = module;
	}
	
	public void setData(TestData data) {
		this.data = data;
	}
	
	public Test getParent() {
		return parentTest;
	}
	
	public Module getModule() {
		return module;
	}
	
	public ArrayList<Test> getSubTests(){
		return subTests;
	}
	
	public ArrayList<Test> getAllSubTests(){
		ArrayList<Test> result = new ArrayList<Test>();
		for (Test subtest : subTests) {
			result.add(subtest);
			result.addAll(subtest.getAllSubTests());
		}
		return result;
	}
	
	public void addSubTest(Test test) {
		this.subTests.add(test);
	}
	
	public void addSubTest(Test test, int index) {
		this.subTests.add(index, test);
	}
	
	public void setSubTests( ArrayList<Test> subTests){
		this.subTests = subTests;
	}
	
	public TestData getData() {
		return data;
	}
	
	public int getState() {
		return state;
	}
	
	public void setState(int state) {
		this.state = state;
	}
	
	public ImageIcon getIcon() {
		return states[state];
	}
	
	public String getAdocColor(Integer i) {
		return colors[i];
	}
	public void setTested(boolean tested) {
		this.tested = tested;
	}
	
	public boolean isTested() {
		return tested;
	}
	
	public void remove() {
		if (parentTest == null) {
			module.removeTest(this);
		}
		else {
			parentTest.getSubTests().remove(this);
		}
	}
	
	public abstract int doTest();
	
	//toString pour la liste Swing
	@Override
	public String toString() {
		return indent() + data.getName();
	}
	
	public Object[] toLine() {
		return new Object[] {this.getModule(), !(this instanceof ManualTest), this ,states[this.getState()]};
	}
	
	public Object[][] toTable() {
		ArrayList<Test> array = getAllSubTests();
		Object[][] result = new Object[array.size()+1][3];;
		result[0] = toLine();
		for(int i=0;i<array.size();i++) {
			Test subTest = array.get(i);
			result[i+1] = subTest.toLine();
		}
		return result;
	}
	
	//ADOC
	public String toAdocString(String number) {
		String result =  "| {set:cellbgcolor:none} <<anchor-"+number+"," + number + " - " + 
				data.getName() + ">> ";
		for(Integer date : data.getDates()) {
			result += "|{set:cellbgcolor:"+ getAdocColor(date)+"}";
		}
		result += nl;
		int i = 0;
		for (Test subTest : subTests)
			result += subTest.toAdocString(number + "." + (++i));
		return result;
	}
	
	public String toAdocInformationString(String number) {
		String result = "";
		result += "| [[anchor-"+number+"]]" + number + " - " + data.getName() + "| "
				+data.getDescription() +"| "+data.getData()+"| "+data.getOutput()+nl;
		int i = 0;
		for (Test subTest : subTests)
			result += subTest.toAdocInformationString(number + "." + (++i));
		return result;
	}
	
	//SAVE TO FILE
	public abstract String toXMLFile();
}
