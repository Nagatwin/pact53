package tests;

import java.util.ArrayList;

public class Module {
	private static final String nl = System.getProperty("line.separator");
	private String name;
	private final ArrayList<Test> tests = new ArrayList<Test>();
	private final ArrayList<String> dates = new ArrayList<String>();
	
	public Module(String name) {
		super();
		this.name = name;
	}

	public void addDates(ArrayList<String> dates) {
		this.dates.addAll(dates);
	}
	
	public void addDate(String date) {
		this.dates.add(date);
	}
	public ArrayList<String> getDates(){
		return dates;
	}
	
	public void addTest(Test test) {
		tests.add(test);
	}
	
	public void addTest(Test test, int index) {
		tests.add(index, test);
	}
	
	protected void removeTest(Test test) {
		tests.remove(test);
	}
	
	public ArrayList<Test> getTests() {
		return tests;
	}

	public ArrayList<Test> getAllTest() {
		ArrayList<Test> result = new ArrayList<Test>();
		for (Test test : tests) {
			result.add(test);
			result.addAll(test.getAllSubTests());
		}
		return result;
	}
	
	public String getName() {
		return name;
	}

	public Object[][] toTable(){
		Object[][] result = new Object[][] {};
		for (Test test : tests) {
			Object[][] matrix = test.toTable();
			Object[][] tampon = new Object[matrix.length+result.length][5] ;
			System.arraycopy(result, 0, tampon, 0, result.length );
			System.arraycopy(matrix, 0, tampon, result.length, matrix.length);
			result = tampon;
		}
		return result;
	}
	@Override
	public String toString() {
		return name;
	}

	public String toAdocString() {
		String result = "";
		// Title
		result += "=== " + name + nl + nl;
		// Beginning of the tabular
		//result += "[cols=\""+(dates.size()+1)+"\",options=\"header\",]" + nl;
		result += "|================================================================================================"
				+ nl;
		result += ".2+^.^| {set:cellbgcolor:none} *Nom du test*";
		//Ajout des dates
		ArrayList<String> motives = new ArrayList<String>();
		ArrayList<String> days = new ArrayList<String>();
		for (String date : dates) {
			if (!date.equals("")) {
			motives.add(date.split("@")[0]);
			days.add(date.split("@")[1]);
		}
		}
		for (String motive : motives) {
			result += "|*" + motive +"*";
		}
		result+=nl;
		for (String day : days) {
			result+="|*" + day + "*";
		}
		result+=nl;
		// Completing the tabular
		int i = 0;
		for (Test test : tests)
			result += test.toAdocString("" + (++i));
		// Endinf the tabular
		result += "|================================================================================================"
				+ nl;
		return result;
	}
	
	public String toAdocInformation() {
		String result = "";
		//Title
		result += "=== Informations " + name + nl +nl;
		//Beginning of the tabular
		result +="[cols=\"4\",options=\"header\",]" + nl;
		result += "|================================================================================================"
				+ nl;
		result += "| {set:cellbgcolor:none} Nom du test| Description | Input data | Output data " + nl;
		//Completion
		int i = 0;
		for (Test test : tests){
			result += test.toAdocInformationString(""+(++i));
		}
		//Ending the tabular
		result += "|================================================================================================"
				+ nl;
		return result;
	}
	
	//SAVE TO FILE

	public String getDatesString() {
		String result = "";
		for (String date : dates) {
			if (!date.equals("")) {
				result+= date+",";
			}
		}
		return result.substring(0, result.length()-1);
	}
	
	public String toXMLFile() {
		String result = "<module name = \"" +name+"\">"+nl;
		result +="<dates>"+getDatesString()+"</dates>"+nl;
		for(Test test : tests) {
			result+=test.toXMLFile();
		}
		result+="</module>"+nl;
		return result;
	}
}
