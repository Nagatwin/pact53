package tests;

import java.io.File;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPathConstants;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;


public class XMLFile {
	private static final Test[] testTypes = {
			new ManualTest(), new TestBidon()};
	private Document document = null;
	private ArrayList<Module> modules = null;
	
	public XMLFile(String filename) {
		try {
			File fXmlFile = new File(filename);
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			document = dBuilder.parse(fXmlFile);
			document.getDocumentElement().normalize();
	    } catch (Exception e) {
		e.printStackTrace();
	    }
	}

	public ArrayList<Module> getModules(){
		return modules;
	}
	
	public void loadModules(){
		ArrayList<Module> result = new ArrayList<Module>();
		//Chargement de tous les modules
		NodeList moduleList = document.getElementsByTagName("module");
		for (int tempModule = 0; tempModule < moduleList.getLength(); tempModule++) {
			Node moduleNode = moduleList.item(tempModule);
			Element mElement = (Element) moduleNode;
			Module module = new Module(mElement.getAttribute("name"));
			//chargement dates de test
			String[] dates = mElement.getElementsByTagName("dates").item(0).getTextContent().split(",");
			for (String date : dates) {
				module.addDate(date);
			}
			//Chargement des tests
			List<Node> testList =getChildrenByTagName(mElement, "test");
			for (int tempTest = 0; tempTest < testList.size();tempTest++) {
				Node testNode = testList.get(tempTest);
				module.addTest(loadTest(testNode, module, null));
			}
			result.add(module);
		}
		modules = result;
	}
	
	public Test loadTest(Node testNode, Module module, Test test) {
		Element tElement = (Element) testNode;
		tElement.getAttribute("name");
		String name = tElement.getAttribute("name");
		String description = tElement.getElementsByTagName("description").item(0).getTextContent();
		String data = tElement.getElementsByTagName("data").item(0).getTextContent();
		String output = tElement.getElementsByTagName("output").item(0).getTextContent();
		String className = tElement.getElementsByTagName("class").item(0).getTextContent();
		//Loading the dates
		String[] dates = tElement.getElementsByTagName("dates").item(0).getTextContent().split(",");
		ArrayList<Integer> datesInt = new ArrayList<Integer>();
		for (String date : dates) {
			if (date !="") datesInt.add(Integer.parseInt(date));
		}
		//Creation of the test
		TestData dataTest = new TestData(name, description, data, output, datesInt);
		Test result;
		//Testing if the className is ManualTest
		if (className.equals("")) {
			result = new ManualTest(dataTest, module, test);
		}
		else {
			String methodName = tElement.getElementsByTagName("method").item(0).getTextContent();
			Class<?> obj;
			try {
				obj = Class.forName(className);
				Method method = obj.newInstance().getClass().getMethod(methodName);
				result = new AutoTest(dataTest, module, test, method, obj);
			} catch (NoSuchMethodException | SecurityException | ClassNotFoundException | InstantiationException | IllegalAccessException e) {
				JOptionPane.showMessageDialog( new JFrame(),
					    "Could not create an auto Test : creating a Manual Test instead.",
					    "AutoTest Error",
					    JOptionPane.WARNING_MESSAGE);
				result = new ManualTest(dataTest, module, test);
			}
		}
		List<Node> subTestList = getChildrenByTagName(tElement, "test");
		ArrayList<Test> subTests = new ArrayList<Test>();
		for(Node sub : subTestList) {
			subTests.add(loadTest(sub, module, result));
		}
		result.setSubTests(subTests);
		return result;
	}

	public static List<Node> getChildrenByTagName(Element parent, String name) {
	    List<Node> nodeList = new ArrayList<Node>();
	    for (Node child = parent.getFirstChild(); child != null; child = child.getNextSibling()) {
	      if (child.getNodeType() == Node.ELEMENT_NODE && name.equals(child.getNodeName())) {
	        nodeList.add(child);
	      }
	    }
	    return nodeList;
	}
}