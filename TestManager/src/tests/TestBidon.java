package tests;

import java.util.Random;

public class TestBidon extends Test{

	public TestBidon() {
		super();
	}
	
	public TestBidon(TestData data) {
		super(data);
	}

	public TestBidon(TestData data, Module module) {
		super(data, module);
	}
	
	public TestBidon(TestData data, Module module, Test parent) {
		super(data, module, parent);
	}
	
	public int doTestBidon() {
		Random random = new Random();
		try { Thread.sleep(500);} catch (InterruptedException e) {e.printStackTrace();}
		return random.nextInt(2);
	}

	@Override
	public int doTest() {
		return 2;
	}
	
	@Override
	public String toXMLFile() {
		final String nl = System.getProperty("line.separator");
		String result = "<test name = \""+getData().getName()+"\">"+nl;
		result += "<description>"+getData().getDescription()+"</description>"+nl
				+"<data>"+getData().getData()+"</data>"+nl
				+"<output>"+getData().getOutput()+"</output>"+nl
				+"<dates>"+getData().getDatesString()+"</dates>"+nl
				+"<class></class>"+nl
				+"<method>"+getData().getDatesString()+"</method>"+nl;
		for (Test test : getSubTests()) {
			result += test.toXMLFile();
		}
		result+= "</test>"+nl;
		return result;
	}
}
