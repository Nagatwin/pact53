set temp=%cd%
if exist "C:\Program Files (x86)\Steam\steamapps\common\Counter-Strike Source\bin\vbsp.exe" (
	cd "C:\Program Files (x86)\Steam\steamapps\common\Counter-Strike Source\bin"
) else (
	if exist "%UserProfile%\Documents\Counter Strike\Counter-Strike Source\bin\vbsp.exe" (
		cd "%UserProfile%\Documents\Counter Strike\Counter-Strike Source\bin"
	)
)
vbsp.exe -game "..\cstrike" "%temp%\data\maps\new_map.vmf"
