cd "%cd%\data\script\VTFCmd\"
VTFCmd.exe -folder "%cd%\data\textures" -output "%cd%\data\materials\new_map" -recurse -resize

cd "../../materials/new_map"
for %%i in (*.vtf) do (
    echo "LightmappedGeneric" { "$basetexture" "new_map\%%~ni" } > %%~ni.vmt
)
