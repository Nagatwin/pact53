package com.example.tarik.map_it;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;

public class TakePlan extends AppCompatActivity {
    ImageView mImageView;
Bitmap image;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_take_plan);


        mImageView = (ImageView) findViewById(R.id.imageView3);
        Button browse = findViewById(R.id.browse);
        Button confirm = findViewById(R.id.confirm);
        Button takePicture = findViewById(R.id.takePicture);
        TextView text = findViewById(R.id.textView5);
        text.setText(NewProject.name);


        browse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mediaChooser = new Intent(Intent.ACTION_GET_CONTENT);
                mediaChooser.setType("image/*");
                startActivityForResult(mediaChooser, 1);
            }

        });

        confirm.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick (View view){
                    if(image != null)

                    {
                        image.setHasAlpha(false);
                        PlanImage pi = new PlanImage(image);

                        try {
                            Bitmap imageBitmap = (pi.getFinished3CImage());
                            //pi.doAnalysis();
                            writeToFile(pi.getXMLText());
                            mImageView.setImageBitmap(imageBitmap);
                            AlertDialog.Builder builder1 = new AlertDialog.Builder(TakePlan.this);
                            builder1.setMessage("Plan sauvegardé !\nVous pouvez désormais l'importer sur le PC.");
                            builder1.setCancelable(true);

                            builder1.setPositiveButton(
                                    "Ok",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            dialog.cancel();
                                        }
                                    });

                            AlertDialog alert11 = builder1.create();
                            alert11.show();
                        } catch (Exception ex) {
                            AlertDialog.Builder builder1 = new AlertDialog.Builder(TakePlan.this);
                            builder1.setMessage("Qualité insuffisante !\nChangez de fichier.");
                            builder1.setCancelable(true);

                            builder1.setPositiveButton(
                                    "Ok",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            dialog.cancel();
                                        }
                                    });

                            AlertDialog alert11 = builder1.create();
                            alert11.show();
                        }
//                startActivity(new Intent(TakePlan.this, PlanView.class));
            }
            }
        });

        takePicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(TakePlan.this, TakeAPicture.class));
            }
        });
    }


    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (resultCode == Activity.RESULT_OK) {
            if (data != null && data.getData() != null) {
                Uri uri = data.getData();
                mImageView.setImageURI(uri);
                try {
                    image = MediaStore.Images.Media.getBitmap(this.getContentResolver(), uri);
                } catch (IOException e) {
                    //e.printStackTrace();
                }
            }
        }
    }

    public void writeToFile(String data) {
        try {
            File file = new File(this.getExternalFilesDir(null), "plan.xml");

            FileOutputStream fileOutput = new FileOutputStream(file);
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(fileOutput);
            outputStreamWriter.write(data);
            outputStreamWriter.flush();
            fileOutput.getFD().sync();
            outputStreamWriter.close();
            MediaScannerConnection.scanFile(
                    this,
                    new String[]{file.getAbsolutePath()},
                    null,
                    null);
        } catch (Exception ex) {
        }
    }
}
