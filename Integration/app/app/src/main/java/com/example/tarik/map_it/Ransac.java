package com.example.tarik.map_it;

import android.util.Log;

import java.util.ArrayList;
import java.util.Random;

public class Ransac {
    // Plus petit = cherche plus de droites

    public static ArrayList<Segment> ransac(int[][] matrix) throws PhotoDegueuException {
        ArrayList<IntPoint> ransacPoints = new ArrayList<>();

        int width = matrix.length;
        int height = matrix[0].length;
        Log.i("MPA_RANSAC", "Width: " + width);
        Log.i("MPA_RANSAC", "Height: " + height);

        //Repere d'écartement des points aux droites
        int seuil = chanfrein(matrix);
        if (seuil > Math.max(width, height)/5)
            throw new PhotoDegueuException();
        seuil = (seuil == 0) ? 1 : seuil;
        Log.i("MPA_RANSAC", "Chanfrein : " + seuil);

        Random r = new Random();

        for (int i = 0; i < width; i++)
            for (int j = 0; j < height; j++)
                if (matrix[i][j] == 1)
                    ransacPoints.add(new IntPoint(i, j));

        ArrayList<IntPoint> startPoints = (ArrayList<IntPoint>) ransacPoints.clone();
        Log.i("MPA_RANSAC", "Points totaux : " + ransacPoints.size());

        ArrayList<Droite> droitesFound = new ArrayList<>();

        boolean terminated = false;
        while (!terminated) { //On trouve 1 droite par passage
            Log.i("MPA_RANSAC", "Recherche de droite : ");
            float maxVotes = 0;
            Droite maxDroite = null;
            ArrayList<IntPoint> votingPoints = null;

            for (int l = 0; l < ransacPoints.size() * CONST.PERCENT_OF_POINTS; l++) {// On teste 1 droite par passage; condition d'arret à revoir
                Log.i("MPA_RANSAC", "Droite de test " + l);
                int r1 = r.nextInt(ransacPoints.size());
                IntPoint p1 = ransacPoints.get(r1);
                int r2 = r.nextInt(ransacPoints.size() - 1);
                IntPoint p2 = ransacPoints.get((r2 < r1) ? r2 : r2 + 1);

                try {
                    Droite calculedDroite = new Droite(p1.toFloatPoint(), p2.toFloatPoint());
                    float votes = 0;
                    ArrayList<IntPoint> newVotingPoints = new ArrayList<IntPoint>();
                    for (IntPoint p : ransacPoints) {
                        double delta = 1.5 * seuil - calculedDroite.distToPoint(p.toFloatPoint());//Condition de distance à revoir
                        if (0 < delta) {
                            votes += delta;
                            newVotingPoints.add(p);
                        }
                    }
                    if (votes > maxVotes) {
                        Log.i("MPA_RANSAC", "Droite meilleure");
                        maxDroite = calculedDroite;
                        maxVotes = votes;
                        votingPoints = newVotingPoints;
                    }
                } catch (Exception e) {// Never happens
                    e.printStackTrace();
                }
            }
            if (votingPoints == null || (float) votingPoints.size() / (float) ransacPoints.size() < CONST.PERCENT_STOP_TOTAL)//Condition d'arret à revoir
                terminated = true;

            // On a une droite
            if (votingPoints != null) {
                for (IntPoint p : votingPoints)
                    ransacPoints.remove(p);
                maxDroite.project(votingPoints.get(0).toFloatPoint());
                droitesFound.add(maxDroite);
            }
        }
        Log.i("MPA_RANSAC", "Droites trouvées : " + droitesFound.size());

        for (Droite d : droitesFound)
            for (IntPoint p : startPoints)
                if (0 < 1.5 * seuil - d.distToPoint(p.toFloatPoint())) // Distance à revoir
                    d.addPointToProject(p.toFloatPoint());

/*Redressement fait de la merde
        for (Droite d1 : droitesFound)
			for (Droite d2 : droitesFound)
				if (!d1.equalsTo(d2)) {
					double diff = d1.angleDiff(d2);
					if (CONST.DOUBLE_MIN_VALUE < diff && diff < CONST.ANGLE_DIFF_MIN) {
						Droite.average(d1, d2);
					}
				}//*/

        for (Droite d : droitesFound) {
            for (DoublePoint p : d.getPointsToProject())
                d.project(p);
        }

        ArrayList<Droite> droitesBords = new ArrayList<>();
        droitesBords.add(new Droite(0, 1, 0));
        droitesBords.add(new Droite(0, 1, -height + 1));
        droitesBords.add(new Droite(1, 0, 0));
        droitesBords.add(new Droite(1, 0, -width + 1));


        ArrayList<Segment> segmentsFound = new ArrayList<>();
        for (Droite d : droitesFound) {
            Log.i("MPA_RANSAC", "----------NOUVELLE DROITE------------");
            ArrayList<DoublePoint> bordsPoints = new ArrayList<>();
            for (Droite f : droitesBords) {
                DoublePoint p = d.intersect(f);
                if (p != null) {
                    Log.i("MPA_RANSAC", "INTERSECTION BORD : X=" + p.getX() + "; Y=" + p.getY());
                    if ((int) p.getX() >= 0 && (int) p.getX() <= width - 1 && (int) p.getY() >= 0 && (int) p.getY() <= height - 1) //condition sur les coord pour etre dans l'image
                        bordsPoints.add(p);
                }

            }

            if (bordsPoints.size() != 2) {
                Log.i("MPA_RANSAC", "Mauvais nb de pts au bord: " + bordsPoints.size());
            } else {
                Log.i("MPA_RANSAC", "Points au bord :" + bordsPoints.size());

                double longueurDroite = bordsPoints.get(0).distanceTo(bordsPoints.get(1));
                Log.i("MPA_RANSAC", "longueurDroite=" + longueurDroite);

                int projetesTotal = d.getProjetes().size();

                ArrayList<DoublePoint> intersectPoints = new ArrayList<>();
                for (Droite p : droitesFound) {
                    if (d != p) {
                        DoublePoint ip = d.intersect(p);
                        if (ip != null && 0 <= (int) ip.getX() && (int) ip.getX() < width && 0 <= (int) ip.getY()
                                && (int) ip.getY() < height) {
                            int k = 0;
                            while (k < intersectPoints.size() && intersectPoints.get(k).compareTo(ip) < 0)
                                k++;
                            intersectPoints.add(k, ip);
                        }

                    }
                }
                for (DoublePoint p : intersectPoints) {
                    Log.i("MPA_RANSAC", "INTERSECTION X=" + p.getX() + "; Y=" + p.getY());
                }

                int depart = 0;
                int arrivee = 1;
                boolean lastValide = true;
                //if assez de pts (>4)
                Log.i("MPA_RANSAC", intersectPoints.size() + " INTERSECTIONS TROUVEES");
                Log.i("MPA_RANSAC", d.getProjetes().size() + " PROJETES TROUVES");
                while (arrivee < intersectPoints.size()) {
                    Segment currentSegment = new Segment(intersectPoints.get(arrivee - 1), intersectPoints.get(arrivee), d);

                    double longueurSegment = intersectPoints.get(arrivee - 1).distanceTo(intersectPoints.get(arrivee));

                    int nombreDeProjetes = 0;
                    for (DoublePoint p : d.getProjetes())
                        if (currentSegment.isOnSegment(p))
                            nombreDeProjetes++;

                    Log.i("MPA_RANSAC", "longueurSegment = " + longueurSegment);
                    Log.i("MPA_RANSAC", "nombreDeProjetes = " + nombreDeProjetes);
                    if (longueurSegment / (longueurDroite) < ((double) nombreDeProjetes / (double) projetesTotal)) {//segment valide
                        arrivee++;
                        lastValide = true;
                        Log.i("MPA_RANSAC", "SEGMENT VALIDE");
                    } else {
                        if (lastValide) {
                            if (1 < intersectPoints.get(depart).distanceTo(intersectPoints.get(arrivee - 1))) {
                                Segment newSegment = new Segment(intersectPoints.get(depart), intersectPoints.get(arrivee - 1), d);
                                segmentsFound.add(newSegment);
                                Log.i("MPA_RANSAC", "Segment " + newSegment.getA().getX() + ";" + newSegment.getA().getY() + " " + newSegment.getB().getX() + ";" + newSegment.getB().getY());
                            }
                        }
                        depart = arrivee;
                        arrivee++;
                        lastValide = false;
                    }

                }
                if (lastValide) {
                    if (1 < intersectPoints.get(depart).distanceTo(intersectPoints.get(arrivee - 1))) {
                        Segment newSegment = new Segment(intersectPoints.get(depart), intersectPoints.get(arrivee - 1), d);
                        Log.i("MPA_RANSAC", "Segment " + newSegment.getA().getX() + ";" + newSegment.getA().getY() + " " + newSegment.getB().getX() + ";" + newSegment.getB().getY());
                        segmentsFound.add(newSegment);
                    }
                }
            }
        }
        return segmentsFound;
    }

    public static int chanfrein(int[][] matrix) {
        int width = matrix.length;
        int height = matrix[0].length;
        boolean[][] checked = new boolean[width][height];
        ArrayList<IntPoint> points = new ArrayList<IntPoint>();

        for (int i = 0; i < width; i++)
            for (int j = 0; j < height; j++) {
                if (matrix[i][j] == 0) {
                    points.add(new IntPoint(i, j));
                    checked[i][j] = true;
                } else
                    checked[i][j] = false;
            }
        int dist = 0;
        while (points.size() != 0) {
            ArrayList<IntPoint> newPoints = new ArrayList<IntPoint>();
            for (IntPoint p : points) {
                int i = p.getX();
                int j = p.getY();
                if (i > 0 && !checked[i - 1][j]) {
                    newPoints.add(new IntPoint(i - 1, j));
                    checked[i - 1][j] = true;
                }
                if (j > 0 && !checked[i][j - 1]) {
                    newPoints.add(new IntPoint(i, j - 1));
                    checked[i][j - 1] = true;
                }
                if (i < width - 1 && !checked[i + 1][j]) {
                    newPoints.add(new IntPoint(i + 1, j));
                    checked[i + 1][j] = true;
                }
                if (j < height - 1 && !checked[i][j + 1]) {
                    newPoints.add(new IntPoint(i, j + 1));
                    checked[i][j + 1] = true;
                }
            }
            dist++;
            points = newPoints;
        }
        return dist;
    }

    private static int[][] scale(int[][] matrix, int scale) {
        int width = matrix.length;
        int height = matrix[0].length;
        int newWidth = width / scale;
        int newHeight = height / scale;
        int[][] result = new int[newWidth][newHeight];

        for (int i = 0; i < newWidth; i++)
            for (int j = 0; j < newHeight; j++) {
                boolean found = false;
                int k = 0;
                while (!found && k < scale) {
                    int l = 0;
                    while (!found && l < scale) {
                        if (matrix[i * scale + k][j * scale + l] == 1) {
                            result[i][j] = 1;
                            found = true;
                        }
                        l++;
                    }
                    k++;
                }
            }
        return result;
    }
}
