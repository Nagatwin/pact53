﻿Les projections & homographies:

Cours projection 3D sur espace 2D
https://inst.eecs.berkeley.edu/~cs184/sp04/handouts/03-ProjectionAnd3DTransforms-LectureNotes.pdf

Cours Homographie et vision par ordinateur de l'université de Montréal
https://www.iro.umontreal.ca/~roys/ift6145H06/calib4.pdf

Page wikipedia de projection 3D
https://en.wikipedia.org/wiki/3D_projection


Histogrammes et méthode d'Otsu:

Page wikiedia des histogramme
https://fr.wikipedia.org/wiki/Histogramme_(imagerie_num%C3%A9rique)

Page wikipedia de la methode d'Otsu
https://fr.wikipedia.org/wiki/M%C3%A9thode_d%27Otsu
https://en.wikipedia.org/wiki/Otsu%27s_method


morphismes:
https://homepages.inf.ed.ac.uk/rbf/HIPR2/dilate.htm