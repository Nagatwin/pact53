package traitementImages;

import java.io.File;
import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Random;

import javax.imageio.ImageIO;

public class Ransac {
	// Plus petit = cherche plus de droites
	private static final float PERCENT_STOP = 0.05f;
	// Plus petit = plus rapide mais moin de r�solution
	private static final int SCALE_FACTOR = 5;

	public static final double ANGLE_DIFF_MIN = Math.PI / 20;

	public static void TestFromSamples() {
		// Bloc de test ransac
		File folderPlans = new File("data/Samples/Plans");
		File[] listOfFilesPlans = folderPlans.listFiles();

		for (int i = 0; i < listOfFilesPlans.length; i++)
			if (listOfFilesPlans[i].isFile()) {
				String outPath = "data/Output/Plans/"
						+ listOfFilesPlans[i].getName().substring(0, listOfFilesPlans[i].getName().length() - 4);
				File out = new File(outPath + "_RANSAC.jpg");
				if (!out.exists())
					applyRansac(listOfFilesPlans[i].getPath(), outPath);
			}
	}

	protected static void applyRansac(String inputFile, String outputFile) {
		try {
			LocalDateTime startDate = LocalDateTime.now();
			System.out.println("Computing RANSAC on " + inputFile);

			// On lit l'matrix source
			PlanImage planImage = new PlanImage(ImageIO.read(new File(inputFile)));

			// On ecrit dans le fichier
			ImageIO.write(planImage.getRansacWallBinImage(), "jpg", new File(outputFile + "_RANSAC.jpg"));

			System.out.println("Done in " + Duration.between(startDate, LocalDateTime.now()).toMillis() + "ms");
		} catch (IOException e) {
			System.out.println("Error");
		}
	}

	private static int[][] scale(int[][] matrix, int scale) {
		int width = matrix.length;
		int height = matrix[0].length;
		int newWidth = width / scale;
		int newHeight = height / scale;
		int[][] result = new int[newWidth][newHeight];

		for (int i = 0; i < newWidth; i++)
			for (int j = 0; j < newHeight; j++) {
				boolean found = false;
				int k = 0;
				while (!found && k < scale) {
					int l = 0;
					while (!found && l < scale) {
						if (matrix[i * scale + k][j * scale + l] == 1) {
							result[i][j] = 1;
							found = true;
						}
						l++;
					}
					k++;
				}
			}
		return result;
	}

	public static int[][] ransac(int[][] matrix) {
		ArrayList<IntPoint> ransacPoints = new ArrayList<IntPoint>();

		int seuil = chanfrein(matrix);

		matrix = scale(matrix, seuil / SCALE_FACTOR);
		seuil = SCALE_FACTOR;
		int width = matrix.length;
		int height = matrix[0].length;
		Random r = new Random();

		for (int i = 0; i < width; i++)
			for (int j = 0; j < height; j++)
				if (matrix[i][j] == 1)
					ransacPoints.add(new IntPoint(i, j));

		ArrayList<IntPoint> startPoints = (ArrayList<IntPoint>) ransacPoints.clone();

		ArrayList<Droite> droitesFound = new ArrayList<Droite>();

		int[][] result = new int[width][height];

		boolean terminated = false;
		while (!terminated) {// On trouve 1 droite par passage dans la boucle
			float maxVotes = 0;
			Droite maxDroite = null;
			ArrayList<IntPoint> votingPoints = null;

			for (int l = 0; l < ransacPoints.size() * PERCENT_STOP; l++) {// On teste 1 droite par passage
				int r1 = r.nextInt(ransacPoints.size());
				IntPoint p1 = ransacPoints.get(r1);
				int r2 = r.nextInt(ransacPoints.size() - 1);
				IntPoint p2 = ransacPoints.get((r2 < r1) ? r2 : r2 + 1);

				try {
					Droite calculedDroite = new Droite(p1.toFloatPoint(), p2.toFloatPoint());
					float votes = 0;
					ArrayList<IntPoint> newVotingPoints = new ArrayList<IntPoint>();
					for (IntPoint p : ransacPoints) {
						double delta = 2 * seuil - calculedDroite.distToPoint(p.toFloatPoint());
						if (0 < delta) {
							votes += delta;
							newVotingPoints.add(p);
						}
					}
					if (votes > maxVotes) {
						maxDroite = calculedDroite;
						maxVotes = votes;
						votingPoints = newVotingPoints;
					}
				} catch (Exception e) {// Never happens
					e.printStackTrace();
				}
			}
			if (votingPoints == null || (float) votingPoints.size() / (float) ransacPoints.size() < PERCENT_STOP)
				terminated = true;

			// On a une droite
			if (votingPoints != null) {
				for (IntPoint p : votingPoints)
					ransacPoints.remove(p);
				maxDroite.project(votingPoints.get(0).toFloatPoint());
				droitesFound.add(maxDroite);
			}
		}

		for (Droite d : droitesFound)
			for (IntPoint p : startPoints)
				if (0 < seuil - d.distToPoint(p.toFloatPoint()))
					d.addPointToProject(p.toFloatPoint());

		for (Droite d1 : droitesFound)
			for (Droite d2 : droitesFound)
				if (!d1.equalsTo(d2)) {
					double diff = d1.angleDiff(d2);
					if (Droite.FLOAT_MIN_VALUE < diff && diff < ANGLE_DIFF_MIN) {
						Droite.average(d1, d2);
					}
				}

		for (Droite d : droitesFound) {
			for (DoublePoint p : d.getPointsToProject())
				d.project(p);
			for (DoublePoint res : d.getProjetes())
				if (0 < (int) res.getX() && (int) res.getX() < width && 0 < (int) res.getY()
						&& (int) res.getY() < height)
					result[(int) res.getX()][(int) res.getY()] = 1;
		}
		// Intersecter des droites ?
		// Faire un histogramme et appliquer otsu
		// R�cuperer segments

		return result;
	}

	public static int chanfrein(int[][] matrix) {
		int width = matrix.length;
		int height = matrix[0].length;
		boolean[][] checked = new boolean[width][height];
		ArrayList<IntPoint> points = new ArrayList<IntPoint>();

		for (int i = 0; i < width; i++)
			for (int j = 0; j < height; j++) {
				if (matrix[i][j] == 0) {
					points.add(new IntPoint(i, j));
					checked[i][j] = true;
				} else
					checked[i][j] = false;
			}
		int dist = 0;
		while (points.size() != 0) {
			ArrayList<IntPoint> newPoints = new ArrayList<IntPoint>();
			for (IntPoint p : points) {
				int i = p.getX();
				int j = p.getY();
				if (i > 0 && !checked[i - 1][j]) {
					newPoints.add(new IntPoint(i - 1, j));
					checked[i - 1][j] = true;
				}
				if (j > 0 && !checked[i][j - 1]) {
					newPoints.add(new IntPoint(i, j - 1));
					checked[i][j - 1] = true;
				}
				if (i < width - 1 && !checked[i + 1][j]) {
					newPoints.add(new IntPoint(i + 1, j));
					checked[i + 1][j] = true;
				}
				if (j < height - 1 && !checked[i][j + 1]) {
					newPoints.add(new IntPoint(i, j + 1));
					checked[i][j + 1] = true;
				}
			}
			dist++;
			points = newPoints;
		}
		return dist;
	}
}
