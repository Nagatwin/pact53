package traitementImages;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;

public class Homographie {
	// Les parametres de l'homographie
<<<<<<< HEAD:ModuleTI/src/TraitementImage/Homographie.java
	private float _a;
	private float _b;
	private float _c;
	private float _d;
	private float _e;
	private float _f;
	private float _g;
	private float _h;
	private float _i;
=======
	private double _a;
	private double _b;
	private double _c;
	private double _d;
	private double _e;
	private double _f;
	private double _g;
	private double _h;
	private double _i;
>>>>>>> Developpement:ModuleTI/src/traitementImages/Homographie.java

	/**
	 * Cr�e un objet Homographie
	 */
	public Homographie() {
	}

	/**
	 * Cr�e un objet Homographie
<<<<<<< HEAD:ModuleTI/src/TraitementImage/Homographie.java
=======
	 * 
>>>>>>> Developpement:ModuleTI/src/traitementImages/Homographie.java
	 * @param a
	 * @param b
	 * @param c
	 * @param d
	 * @param e
	 * @param f
	 * @param g
	 * @param h
	 * @param i
	 */
<<<<<<< HEAD:ModuleTI/src/TraitementImage/Homographie.java
	public Homographie(float a, float b, float c, float d, float e, float f, float g, float h, float i) {
=======
	public Homographie(double a, double b, double c, double d, double e, double f, double g, double h, double i) {
>>>>>>> Developpement:ModuleTI/src/traitementImages/Homographie.java
		_a = a;
		_b = b;
		_c = c;
		_d = d;
		_e = e;
		_f = f;
		_g = g;
		_h = h;
		_i = i;
	}
<<<<<<< HEAD:ModuleTI/src/TraitementImage/Homographie.java
	
	/**
	 * Parametre l'homographie pour qu'elle fasse correller les points
	 * @param entrees Points d'entr�e
	 * @param sorties Points de sortie
	 * @throws Exception Erreur si pas le m�me nombre de points
	 */
	public void GetFromPointSample(ArrayList<Point> entrees, ArrayList<Point> sorties) throws Exception {
=======

	/**
	 * Parametre l'homographie pour qu'elle fasse correller les points
	 * 
	 * @param entrees
	 *            Points d'entr�e
	 * @param sorties
	 *            Points de sortie
	 * @throws Exception
	 *             Erreur si pas le m�me nombre de points
	 */
	public void GetFromPointSample(ArrayList<DoublePoint> entrees, ArrayList<DoublePoint> sorties) throws Exception {
>>>>>>> Developpement:ModuleTI/src/traitementImages/Homographie.java
		LocalDateTime startDate = LocalDateTime.now();
		System.out.println("Finding homography");
		// On definit la matrice de syst�me
		double[][] systemA = new double[9][9];

		// Et le vecteur
		double[] systemB = new double[9];

		// Ligne actuelle
		int ligneActuelle = 0;

		// Nombre total de points
		int totalPoints = entrees.size();
		if (totalPoints != sorties.size())
			throw new Exception("Pas le m�me nombre d'entr�es et de sorties");

		// Indice du point actuel
		int pointActuel = -1;

		// Tant que on a pas fait tout les points ou qu'il ne reste pas que une seule
		// ligne
		while (pointActuel < totalPoints && ligneActuelle < 7) {
			pointActuel++;

			// On recupere les 2 points
			DoublePoint ptIn = entrees.get(pointActuel);
			DoublePoint ptOut = sorties.get(pointActuel);

			double x = ptIn.getX();
			double y = ptIn.getY();

			double xPrim = ptOut.getX();
			double yPrim = ptOut.getY();

			// On remplit 2 lignes de conditions dans le syst�me
			systemA[ligneActuelle][0] = x;
			systemA[ligneActuelle][1] = y;
			systemA[ligneActuelle][2] = 1;
			systemA[ligneActuelle][3] = 0;
			systemA[ligneActuelle][4] = 0;
			systemA[ligneActuelle][5] = 0;
			systemA[ligneActuelle][6] = -xPrim * x;
			systemA[ligneActuelle][7] = -xPrim * y;
			systemA[ligneActuelle][8] = -xPrim;

			systemA[ligneActuelle + 1][0] = 0;
			systemA[ligneActuelle + 1][1] = 0;
			systemA[ligneActuelle + 1][2] = 0;
			systemA[ligneActuelle + 1][3] = x;
			systemA[ligneActuelle + 1][4] = y;
			systemA[ligneActuelle + 1][5] = 1;
			systemA[ligneActuelle + 1][6] = -yPrim * x;
			systemA[ligneActuelle + 1][7] = -yPrim * y;
			systemA[ligneActuelle + 1][8] = -yPrim;

			ligneActuelle += 2;
		}

		// Si il reste 1 ligne et au moins un point
		if (ligneActuelle == 8 && pointActuel + 1 < totalPoints) {
			pointActuel++;// inutile..

			// On recupere les 2 points
			DoublePoint ptIn = entrees.get(pointActuel);
			DoublePoint ptOut = sorties.get(pointActuel);

			double x = ptIn.getX();
			double y = ptIn.getY();

			double xPrim = ptOut.getX();

			// On remplit 2 lignes de conditions dans le syst�me
			systemA[ligneActuelle][0] = x;
			systemA[ligneActuelle][1] = y;
			systemA[ligneActuelle][2] = 1;
			systemA[ligneActuelle][3] = 0;
			systemA[ligneActuelle][4] = 0;
			systemA[ligneActuelle][5] = 0;
			systemA[ligneActuelle][6] = -xPrim * x;
			systemA[ligneActuelle][7] = -xPrim * y;
			systemA[ligneActuelle][8] = -xPrim;
			ligneActuelle++;
		} else {
			// Sinon on remplit les lignes restantes avec des conditions (i=1; h=1.....)
			while (ligneActuelle < 9) {
				systemA[ligneActuelle][ligneActuelle] = 1;
				systemB[ligneActuelle] = 1;
				ligneActuelle++;
			}
		}

		// On r�soud le syst�me
		double[] result = MathLinear.pivotDeGauss(systemA, systemB);

		// On assigne les valeurs
		_a = result[0];
		_b = result[1];
		_c = result[2];
		_d = result[3];
		_e = result[4];
		_f = result[5];
		_g = result[6];
		_h = result[7];
		_i = result[8];
		System.out.println("Done in " + Duration.between(startDate, LocalDateTime.now()).toMillis() + "ms");
	}

	/**
	 * Teste avec les valeurs donn�es dans le mail du 28/11
	 */
	public static void TestFromSamples() {
		try {
			ArrayList<DoublePoint> entrees = new ArrayList<DoublePoint>();
			ArrayList<DoublePoint> sorties = new ArrayList<DoublePoint>();

			entrees.add(new DoublePoint(100, 100));
			sorties.add(new DoublePoint(50, 100));

			entrees.add(new DoublePoint(300, 100));
			sorties.add(new DoublePoint(350, 100));

			entrees.add(new DoublePoint(300, 400));
			sorties.add(new DoublePoint(150, 400));

			entrees.add(new DoublePoint(100, 400));
			sorties.add(new DoublePoint(250, 400));

			Homographie h = new Homographie();

			h.GetFromPointSample(entrees, sorties);

			System.out.println(h.toString());

			System.out.println("Au total, pr�cis � " + h.Test(entrees, sorties));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
<<<<<<< HEAD:ModuleTI/src/TraitementImage/Homographie.java
	 * Calcule la somme des distances entre tout les points calcules en appliquant homographie � entr�es et les points dans sorties
	 * @param entrees Points de d�part
	 * @param sorties Points d'arriv�e
	 * @return Somme des distances
	 * @throws Exception Si pas le m�me nombre d'entr�es et sortie
	 */
	public float Test(ArrayList<Point> entrees, ArrayList<Point> sorties) throws Exception {
=======
	 * Calcule la somme des distances entre tout les points calcules en appliquant
	 * homographie � entr�es et les points dans sorties
	 * 
	 * @param entrees
	 *            Points de d�part
	 * @param sorties
	 *            Points d'arriv�e
	 * @return Somme des distances
	 * @throws Exception
	 *             Si pas le m�me nombre d'entr�es et sortie
	 */
	public double Test(ArrayList<DoublePoint> entrees, ArrayList<DoublePoint> sorties) throws Exception {
>>>>>>> Developpement:ModuleTI/src/traitementImages/Homographie.java
		// Nombre total de points
		int totalPoints = entrees.size();
		if (totalPoints != sorties.size())
			throw new Exception("Pas le m�me nombre d'entr�es et de sorties");

		double somme = 0;
		for (int i = 0; i < totalPoints; i++) {
			double c = Correlation(entrees.get(i), sorties.get(i));
			System.out.println("Point " + i + " pr�cis � " + c);
			somme += c;
		}

		return somme;
	}

	/**
<<<<<<< HEAD:ModuleTI/src/TraitementImage/Homographie.java
	 * Donne la distance entre l'homographie appliqu�e point donn� in et out (pour le calcul d'erreur)
	 * @param in Point d'entr�e
	 * @param out Image th�orique du point
	 * @return
	 */
	private float Correlation(Point in, Point out) {
		Point outPratique = AppliquerA(in);
		return (float) Math
=======
	 * Donne la distance entre l'homographie appliqu�e point donn� in et out (pour
	 * le calcul d'erreur)
	 * 
	 * @param in
	 *            Point d'entr�e
	 * @param out
	 *            Image th�orique du point
	 * @return
	 */
	private double Correlation(DoublePoint in, DoublePoint out) {
		DoublePoint outPratique = AppliquerA(in);
		return (double) Math
>>>>>>> Developpement:ModuleTI/src/traitementImages/Homographie.java
				.sqrt(Math.pow(out.getX() - outPratique.getX(), 2) + Math.pow(out.getY() - outPratique.getY(), 2));
	}

	/**
	 * Applique l'Homographie au point p
<<<<<<< HEAD:ModuleTI/src/TraitementImage/Homographie.java
	 * @param p
	 * @return
	 */
	public Point AppliquerA(Point p) {
		float x = p.getX();
		float y = p.getY();
		return new Point((_a * x + _b * y + _c) / (_g * x + _h * y + _i),
=======
	 * 
	 * @param p
	 * @return
	 */
	public DoublePoint AppliquerA(DoublePoint p) {
		double x = p.getX();
		double y = p.getY();
		return new DoublePoint((_a * x + _b * y + _c) / (_g * x + _h * y + _i),
>>>>>>> Developpement:ModuleTI/src/traitementImages/Homographie.java
				(_d * x + _e * y + _f) / (_g * x + _h * y + _i));
	}

	@Override
	public String toString() {
		return "Homographie [_a=" + _a + ", _b=" + _b + ", _c=" + _c + ", _d=" + _d + ", _e=" + _e + ", _f=" + _f
				+ ", _g=" + _g + ", _h=" + _h + ", _i=" + _i + "]";
	}

	public double get_a() {
		return _a;
	}

	public double get_b() {
		return _b;
	}

	public double get_c() {
		return _c;
	}

	public double get_d() {
		return _d;
	}

	public double get_e() {
		return _e;
	}

	public double get_f() {
		return _f;
	}

	public double get_g() {
		return _g;
	}

	public double get_h() {
		return _h;
	}

	public double get_i() {
		return _i;
	}

	public void set_a(double _a) {
		this._a = _a;
	}

	public void set_b(double _b) {
		this._b = _b;
	}

	public void set_c(double _c) {
		this._c = _c;
	}

	public void set_d(double _d) {
		this._d = _d;
	}

	public void set_e(double _e) {
		this._e = _e;
	}

	public void set_f(double _f) {
		this._f = _f;
	}

	public void set_g(double _g) {
		this._g = _g;
	}

	public void set_h(double _h) {
		this._h = _h;
	}

	public void set_i(double _i) {
		this._i = _i;
	}
}
