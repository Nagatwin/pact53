package traitementImages;

import java.awt.Color;
import java.awt.image.BufferedImage;

public class PlanImage {
	private final BufferedImage bufferedImage;
	private final float[][][] HSBMatrix;
	private final int height, width;
	private static final float S_FLOOR_TEST = 0.6f;
	private static final float MIN_GREEN_ANGLE = 0.1f;
	private static final float MAX_GREEN_ANGLE = 0.5f;
	private static final float MIN_RED_ANGLE = 0.6f;
	private static final float MAX_RED_ANGLE = 0.2f;

	protected PlanImage(BufferedImage bufferedImage) {
		this.bufferedImage = bufferedImage;
		this.height = bufferedImage.getHeight();
		this.width = bufferedImage.getWidth();
		this.HSBMatrix = new float[this.width][this.height][3];
		buildHSB();
	}

	private void buildHSB() {
		for (int i = 0; i < width; i++) {
			for (int j = 0; j < height; j++) {
				Color pixel = new Color(bufferedImage.getRGB(i, j));
				float[] pixelHSB = new float[3];
				Color.RGBtoHSB(pixel.getRed(), pixel.getGreen(), pixel.getBlue(), pixelHSB);
				HSBMatrix[i][j][0] = pixelHSB[0];
				HSBMatrix[i][j][1] = pixelHSB[1];
				HSBMatrix[i][j][2] = pixelHSB[2];
			}
		}
	}

	/**
	 * Renvoie la matrice de l'image en niveau de gris en moyennant RGB
	 * 
	 * @param image
	 *            Image de d�part (peut �tre en rgb)
	 * @return Matrice de niveaux de gris correspondante
	 */
	private int[][] getAverageRGBMatrix() {
		int[][] result = new int[bufferedImage.getWidth()][bufferedImage.getHeight()];
		for (int i = 0; i < bufferedImage.getWidth(); i++)
			for (int j = 0; j < bufferedImage.getHeight(); j++) {
				Color pixel = new Color(bufferedImage.getRGB(i, j));
				result[i][j] = 255 - (pixel.getBlue() + pixel.getRed() + pixel.getGreen()) / 3;// On moyenne rgb
			}
		return result;
	}

	protected BufferedImage getBufferedImage() {
		return bufferedImage;
	}

	private int[][] getMatrixFromSValue(float sFloor) {
		int[][] result = new int[width][height];
		for (int i = 0; i < width; i++)
			for (int j = 0; j < height; j++)
				result[i][j] = (HSBMatrix[i][j][1] <= sFloor) ? (int) (255 - HSBMatrix[i][j][2] * 255) : -1;
		return result;
	}

	private int[][] getMatrixFromHValue(float sFloor, float minAngle, float maxAngle) {
		int[][] result = new int[width][height];
		for (int i = 0; i < width; i++)
			for (int j = 0; j < height; j++)

				result[i][j] = (HSBMatrix[i][j][1] > sFloor
						&& ((HSBMatrix[i][j][0] < maxAngle && HSBMatrix[i][j][0] > minAngle) || (maxAngle < minAngle
								&& (HSBMatrix[i][j][0] < maxAngle || HSBMatrix[i][j][0] > minAngle))))
										? Math.round(255 - HSBMatrix[i][j][1] * 255)
										: -1;
		return result;
	}

	private static int[][] getBinMatrix(int floor, int[][] matrix) {
		int matWidth = matrix.length;
		int matHeight = matrix[0].length;
		int[][] result = new int[matWidth][matHeight];
		for (int i = 0; i < matWidth; i++)
			for (int j = 0; j < matHeight; j++)
				result[i][j] = (matrix[i][j] <= floor) ? 0 : 1;

		return result;
	}

	private static BufferedImage getBinImage(int floor, int[][] matrix) {
		int matWidth = matrix.length;
		int matHeight = matrix[0].length;
		BufferedImage result = new BufferedImage(matWidth, matHeight, BufferedImage.TYPE_INT_RGB);

		for (int i = 0; i < matWidth; i++)
			for (int j = 0; j < matHeight; j++)
				result.setRGB(i, j, ((matrix[i][j] <= floor) ? Color.WHITE : Color.BLACK).getRGB());

		return result;
	}

	private static BufferedImage getBinImage(int[][] matrix) {
		int matWidth = matrix.length;
		int matHeight = matrix[0].length;
		BufferedImage result = new BufferedImage(matWidth, matHeight, BufferedImage.TYPE_INT_RGB);
		for (int i = 0; i < matWidth; i++)
			for (int j = 0; j < matHeight; j++)
				result.setRGB(i, j, ((matrix[i][j] == 0) ? Color.WHITE : Color.BLACK).getRGB());
		return result;
	}

	protected int[][] getGreyBinMatrix() {
		int[][] matrix = this.getAverageRGBMatrix();
		return PlanImage.getBinMatrix(Otsu.getOtsuFloor(matrix), matrix);
	}

	protected BufferedImage getGreyBinImage() {
		int[][] matrix = this.getAverageRGBMatrix();
		return PlanImage.getBinImage(Otsu.getOtsuFloor(matrix), matrix);
	}

	protected int[][] getWallBinMatrix() {
		int[][] matrix = this.getMatrixFromSValue(S_FLOOR_TEST);
		return PlanImage.getBinMatrix(Otsu.getOtsuFloor(matrix), matrix);
	}

	protected BufferedImage getWallBinImage() {
		int[][] matrix = this.getMatrixFromSValue(S_FLOOR_TEST);
		return PlanImage.getBinImage(Otsu.getOtsuFloor(matrix), matrix);
	}

	protected int[][] getDoorBinMatrix() {
		int[][] matrix = this.getMatrixFromHValue(S_FLOOR_TEST, MIN_RED_ANGLE, MAX_RED_ANGLE);
		return PlanImage.getBinMatrix(Otsu.getOtsuFloor(matrix), matrix);
	}

	protected BufferedImage getDoorBinImage() {
		int[][] matrix = this.getMatrixFromHValue(S_FLOOR_TEST, MIN_RED_ANGLE, MAX_RED_ANGLE);
		return PlanImage.getBinImage(Otsu.getOtsuFloor(matrix), matrix);
	}

	protected int[][] getWindowBinMatrix() {
		int[][] matrix = this.getMatrixFromHValue(S_FLOOR_TEST, MIN_GREEN_ANGLE, MAX_GREEN_ANGLE);
		return PlanImage.getBinMatrix(Otsu.getOtsuFloor(matrix), matrix);
	}

	protected BufferedImage getWindowBinImage() {
		int[][] matrix = this.getMatrixFromHValue(S_FLOOR_TEST, MIN_GREEN_ANGLE, MAX_GREEN_ANGLE);
		return PlanImage.getBinImage(Otsu.getOtsuFloor(matrix), matrix);
	}

	protected BufferedImage getErodedWallBinImage() {
		return getBinImage(Morphismes.test(getWallBinMatrix()));
	}

	protected void getIntWallBinImage() {
		System.out.println(Ransac.chanfrein(Morphismes.test(getWallBinMatrix())));
	}

	protected BufferedImage getRansacWallBinImage() {
		return getBinImage(Ransac.ransac(Morphismes.test(getGreyBinMatrix())));
	}
}
