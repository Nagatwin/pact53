package traitementImages;

public class DoublePoint {
	private double _x;
	private double _y;

	public DoublePoint(double x, double y) {
		_x = x;
		_y = y;
	}

	public double getX() {
		return _x;
	}

	public double getY() {
		return _y;
	}

	public double distanceTo(DoublePoint p) {
		return (double) Math.sqrt(Math.pow((_x - p.getX()), 2) + Math.pow((_y - p.getY()), 2));
	}
}
