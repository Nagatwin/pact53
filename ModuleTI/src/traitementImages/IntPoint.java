package traitementImages;

public class IntPoint {
	private final int x, y;

	public IntPoint(int x, int y) {
		this.x = x;
		this.y = y;
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public DoublePoint toFloatPoint() {
		return new DoublePoint(x, y);
	}
}
