package traitementImages;

import java.util.ArrayList;
import java.util.HashMap;

public class Droite {
	public static final double FLOAT_MIN_VALUE = 0.0001f;
	private HashMap<DoublePoint, Float> distances = new HashMap<DoublePoint, Float>();
	private double _a;
	private double _b;
	private double _c;
	private double _angle;
	private ArrayList<DoublePoint> projetes = new ArrayList<DoublePoint>();
	private ArrayList<DoublePoint> toProject = new ArrayList<DoublePoint>();

	public ArrayList<DoublePoint> getPointsToProject() {
		return toProject;
	}

	public void addPointToProject(DoublePoint p) {
		toProject.add(p);
	}

	public ArrayList<DoublePoint> getProjetes() {
		return projetes;
	}

	/**
	 * ax+by+c=0
	 * 
	 * @param a
	 * @param b
	 * @param c
	 */
	public Droite(double a, double b, double c) {
		_a = a;
		_b = b;
		_c = c;
		setAngle();
	}

	/**
	 * y=ax+b
	 * 
	 * @param a
	 * @param b
	 */
	public Droite(double a, double b) {
		_b = 1;
		_a = -a;
		_c = -b;
		setAngle();
	}

	/**
	 * @param a
	 * @param b
	 * @throws Exception
	 */
	public Droite(DoublePoint a, DoublePoint b) throws Exception {
		double x1 = a.getX();
		double x2 = b.getX();
		double y1 = a.getY();
		double y2 = b.getY();
		
			try {
				double[][] matrix = new double[3][3];
				matrix[0][0] = x1;
				matrix[0][1] = y1;
				matrix[0][2] = 1;
				matrix[1][0] = x2;
				matrix[1][1] = y2;
				matrix[1][2] = 1;
				matrix[2][0] = 0;
				matrix[2][1] = 1;
				matrix[2][2] = 0;

				double[] vector = new double[3];
				vector[0] = 0;
				vector[1] = 0;
				vector[2] = 1;

				double[] result = MathLinear.pivotDeGauss(matrix, vector);

				_a = result[0];
				_b = result[1];
				_c = result[2];
			} catch (Exception ex) {
				double[][] matrix = new double[3][3];
				matrix[0][0] = x1;
				matrix[0][1] = -y1;
				matrix[0][2] = 1;
				matrix[1][0] = x2;
				matrix[1][1] = -y2;
				matrix[1][2] = 1;
				matrix[2][0] = 1;
				matrix[2][1] = 0;
				matrix[2][2] = 0;

				double[] vector = new double[3];
				vector[0] = 0;
				vector[1] = 0;
				vector[2] = 1;

				double[] result;
				try {
					result = MathLinear.pivotDeGauss(matrix, vector);

					_a = result[0];
					_b = result[1];
					_c = result[2];
				} catch (Exception e) {
					throw new Exception("Erreur de calcul de droite (points superposés)");
				}
			}
		setAngle();
	}

	public boolean equalsTo(Droite d) {
		double coeff = (Math.abs(_a) > FLOAT_MIN_VALUE) ? d.get_a() / _a : d.get_b() / _b;

		return _a * coeff == d.get_a() && _b * coeff == d.get_b() && _c * coeff == d.get_c();
	}

	public double get_a() {
		return _a;
	}

	public double get_b() {
		return _b;
	}

	public double get_c() {
		return _c;
	}

	@Override
	public String toString() {
		return "Droite [_a=" + _a + ", _b=" + _b + ", _c=" + _c + "]";
	}

	public double distToPoint(DoublePoint point) {
		if (!distances.containsKey(point)) {
			if (Math.abs(_a) < FLOAT_MIN_VALUE)
				distances.put(point, new Float(Math.abs(point.getY() + _c / _b)));
			else if (Math.abs(_b) < FLOAT_MIN_VALUE)
				distances.put(point, new Float(Math.abs(point.getX() + _c / _a)));
			else {
				double m = -_a / _b;
				double p = -_c / _b;
				distances.put(point,
						new Float((double) (Math.abs(m * point.getX() - point.getY() + p) / Math.sqrt(1 + m * m))));
			}
		}
		return distances.get(point);
	}

	public void project(DoublePoint p) {
		if (Math.abs(get_a()) < FLOAT_MIN_VALUE)
			projetes.add(new DoublePoint(p.getX(), -get_c() / get_b()));
		else if (Math.abs(get_b()) < FLOAT_MIN_VALUE)
			projetes.add(new DoublePoint(-get_c() / get_a(), p.getY()));
		else {
			double sq = (double) Math.sqrt(get_b() * get_b() + get_a() * get_a());
			double bh = (double) ((-p.getX() * get_b() + (p.getY() + get_c() / get_b()) * get_a()) / sq);
			double xh = -bh * get_b() / sq;
			double yh = -get_c() / get_b() + bh * get_a() / sq;
			projetes.add(new DoublePoint(xh, yh));
		}
	}

	public DoublePoint getProject(DoublePoint p) {
		if (Math.abs(get_a()) < FLOAT_MIN_VALUE)
			return new DoublePoint(p.getX(), -get_c() / get_b());
		else if (Math.abs(get_b()) < FLOAT_MIN_VALUE)
			return new DoublePoint(-get_c() / get_a(), p.getY());
		else {
			double sq = (double) Math.sqrt(get_b() * get_b() + get_a() * get_a());
			double bh = (double) ((-p.getX() * get_b() + (p.getY() + get_c() / get_b()) * get_a()) / sq);
			double xh = -bh * get_b() / sq;
			double yh = -get_c() / get_b() + bh * get_a() / sq;
			return new DoublePoint(xh, yh);
		}
	}

	protected double getAngle() {
		return _angle;
	}

	private void setAngle() {
		_angle = Math.atan(get_a() / get_b());
	}

	public double angleDiff(Droite d) {
		double diff = this.getAngle() - d.getAngle();
		// System.out.println(this.getAngle()+":" +d.getAngle()+":"
		// +Math.min(Math.abs(diff), Math.min(Math.abs(diff + Math.PI), Math.abs(diff -
		// Math.PI))));
		return Math.min(Math.abs(diff), Math.min(Math.abs(diff + Math.PI), Math.abs(diff - Math.PI)));
	}

	public DoublePoint intersect(Droite d) throws Exception {
		double xp;
		double yp;
		if (Math.abs(get_a()) < FLOAT_MIN_VALUE)
			if (Math.abs(d.get_a()) < FLOAT_MIN_VALUE)
				return null;
			else {
				yp = -get_c() / get_b();
				xp = (-d.get_c() - d.get_b() * yp) / d.get_a();
			}
		else if (Math.abs(d.get_a()) < FLOAT_MIN_VALUE) {
			yp = -d.get_c() / d.get_b();
			xp = (-get_c() - get_b() * yp) / get_a();
		} else if (Math.abs(d.get_b() - d.get_a() * get_b() / get_a()) < FLOAT_MIN_VALUE)
			return null;
		else {
			yp = -(d.get_c() - d.get_a() * get_c() / get_a()) / d.get_b() - d.get_a() * get_b() / get_a();
			xp = (-d.get_b() * yp - d.get_c()) / d.get_a();
		}

		return new DoublePoint(xp, yp);
	}

	public static void average(Droite d1, Droite d2) {
		System.out.println("Angles : " + d1.getAngle() + ":" + d2.getAngle());
		try {
			DoublePoint proj1 = d1.getProjetes().get(0);
			DoublePoint proj2 = d2.getProjetes().get(0);

			if(1.5 < Math.abs(d1.getAngle()))
				System.out.println("X : " + -d1.get_c()/d1.get_a() + ";" + -d2.get_c()/d2.get_a());
			
			double avgAngle;
			if (Math.abs(d1.getAngle() - d2.getAngle()) < FLOAT_MIN_VALUE)
				return;
			if (Math.abs(d1.getAngle() - d2.getAngle()) < Ransac.ANGLE_DIFF_MIN)
				avgAngle = (d1.getAngle() + d2.getAngle()) / 2;
			else
				avgAngle = (Math.PI + d1.getAngle() + d2.getAngle()) / 2;
			Droite d = new Droite(new DoublePoint(0, 0), new DoublePoint(Math.cos(avgAngle), Math.sin(avgAngle)));
			DoublePoint p1 = d.getProject(new DoublePoint(-d1.get_b(), d1.get_a()));
			DoublePoint p2 = d.getProject(new DoublePoint(-d2.get_b(), d2.get_a()));

			System.out.println(" AvgAngle : " +avgAngle);
			
			d1.set_a(p1.getY());
			d1.set_b(-p1.getX());
			d1.set_c(-d1.get_a() * proj1.getX() - d1.get_b() * proj1.getY());
			d1.setAngle();
			d2.set_a(p2.getY());
			d2.set_b(-p2.getX());
			d2.set_c(-d2.get_a() * proj2.getX() - d2.get_b() * proj2.getY());
			d2.setAngle();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("to" + d1.getAngle() + ":" + d2.getAngle());

		if(1.5 < Math.abs(d1.getAngle()))
			System.out.println(-d1.get_c()/d1.get_a() + ";" + -d2.get_c()/d2.get_a());
		/*
		 * 
		 * 
		 * double k1 = Math.sqrt(Math.pow(d1.get_b(), 2) + Math.pow(d1.get_a(), 2));
		 * double k2 = Math.sqrt(Math.pow(d2.get_b(), 2) + Math.pow(d2.get_a(), 2));
		 * double bTot = -d1.get_b()/k1 -d2.get_b()/k2; double aTot = d1.get_a()/k1 +
		 * d2.get_a()/k2; double kTot = Math.sqrt(Math.pow(bTot, 2) + Math.pow(aTot,
		 * 2)); bTot /=kTot; aTot /=kTot; d1.set_a((double) (aTot*k1));
		 * d1.set_b((double) (bTot*k1)); d2.set_a((double) (aTot*k2)); d2.set_b((double)
		 * (bTot*k2));
		 */
	}

	public void set_a(double _a) {
		this._a = _a;
	}

	public void set_b(double _b) {
		this._b = _b;
	}

	public void set_c(double _c) {
		this._c = _c;
	}
}
