package traitementImages;

import java.io.File;
import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;

import javax.imageio.ImageIO;

public abstract class Otsu {
	/**
	 * Applique Otsu � toutes les images du dossier Samples (qui n'ont pas �t�
	 * trait�es) La sortie est dans le dossier Output
	 */
	public static void TestFromSamples() {
		// Bloc de test Otsu
		File folderOtsu = new File("data/Samples/Otsu");
		File[] listOfFilesOtsu = folderOtsu.listFiles();

		for (int i = 0; i < listOfFilesOtsu.length; i++)
			if (listOfFilesOtsu[i].isFile()) {
				File out = new File("data/Output/Otsu/" + listOfFilesOtsu[i].getName());
				if (!out.exists())
					applyOtsu(listOfFilesOtsu[i].getPath(), "data/Output/Otsu/" + listOfFilesOtsu[i].getName());
			}

		// Bloc de test s�paration des couleurs
		File folderPlans = new File("data/Samples/Plans");
		File[] listOfFilesPlans = folderPlans.listFiles();

		for (int i = 0; i < listOfFilesPlans.length; i++)
			if (listOfFilesPlans[i].isFile()) {
				String outPath = "data/Output/Plans/"
						+ listOfFilesPlans[i].getName().substring(0, listOfFilesPlans[i].getName().length() - 4);
				File out = new File(outPath + "_M.jpg");
				if (!out.exists())
					applyOtsuCouleurs(listOfFilesPlans[i].getPath(), outPath);
			}
	}

	protected static void applyOtsuCouleurs(String inputFile, String outputFile) {
		try {
			LocalDateTime startDate = LocalDateTime.now();
			System.out.println("Computing HSB separation on " + inputFile);

			// On lit l'matrix source
			PlanImage planImage = new PlanImage(ImageIO.read(new File(inputFile)));

			// On ecrit dans le fichier
			ImageIO.write(planImage.getWallBinImage(), "jpg", new File(outputFile + "_M.jpg"));
			ImageIO.write(planImage.getWindowBinImage(), "jpg", new File(outputFile + "_F.jpg"));
			ImageIO.write(planImage.getDoorBinImage(), "jpg", new File(outputFile + "_P.jpg"));
			ImageIO.write(planImage.getErodedWallBinImage(), "jpg", new File(outputFile + "_EM.jpg"));
			
			System.out.println("Done in " + Duration.between(startDate, LocalDateTime.now()).toMillis() + "ms");
		} catch (IOException e) {
			System.out.println("Error");
		}
	}

	/**
	 * Applique l'algorithme d'Otsu � une matrix stock�e dans un fichier
	 * 
	 * @param inputFile
	 *            Chemin d'acc�s au fichier d'entr�e
	 * @param outputFile
	 *            Chemin d'acces au fichier de sortie
	 */
	protected static void applyOtsu(String inputFile, String outputFile) {
		try {
			LocalDateTime startDate = LocalDateTime.now();
			System.out.println("Computing OTSU on" + inputFile);

			PlanImage planImage = new PlanImage(ImageIO.read(new File(inputFile)));

			// On applique Otsu et on �crit dans le fichier
			ImageIO.write(planImage.getGreyBinImage(), "jpg", new File(outputFile));
			System.out.println("Done in " + Duration.between(startDate, LocalDateTime.now()).toMillis() + "ms");
		} catch (IOException e) {
			System.out.println("Error");
		}
	}

	protected static int getOtsuFloor(int[][] matrix) {
		return getOtsuFloorFromHistogram(getHistogram(matrix));
	}

	protected static int getOtsuFloor(int[] tab) {
		return getOtsuFloorFromHistogram(getHistogram(tab));
	}

	/**
	 * Calcule l'histogram d'une matrix stock�e dans une matrice en niveaux de gris
	 * 
	 * @param matrix
	 *            Matrice de l'matrix en niveaux de gris
	 * @return histogram de l'matrix
	 */
	private static int[] getHistogram(int[][] matrix) {
		// Recherche du niveau de gris max
		int maxlevel = 0;
		int width = matrix.length;
		int height = matrix[0].length;

		for (int i = 0; i < width; i++)
			for (int j = 0; j < height; j++)
				if (matrix[i][j] != -1 && matrix[i][j] > maxlevel)
					maxlevel = matrix[i][j];

		// Cr�ation du tableau d'histogramme
		int[] result = new int[maxlevel + 1];

		// Construction de l'histogramme
		for (int i = 0; i < width; i++)
			for (int j = 0; j < height; j++)
				if (matrix[i][j] != -1)
					result[matrix[i][j]]++;

		// Renvoi du r�sultat
		return result;
	}
	private static int[] getHistogram(int[] tab) {
		// Recherche du niveau de gris max
		int maxlevel = 0;
		int width = tab.length;

		for (int i = 0; i < width; i++)
				if (tab[i] != -1 && tab[i] > maxlevel)
					maxlevel = tab[i];

		// Cr�ation du tableau d'histogramme
		int[] result = new int[maxlevel + 1];

		// Construction de l'histogramme
		for (int i = 0; i < width; i++)
					result[tab[i]]++;

		// Renvoi du r�sultat
		return result;
	}

	/**
	 * Applique l'algorithme d'Otsu a un histogram donn�
	 * 
	 * @param histogram
	 *            histogram de l'image
	 * @return Seuil de d�coupe calcul� par l'algorithme d'Otsu
	 */
	private static int getOtsuFloorFromHistogram(int[] histogram) {
		int nombreTotal = 0;// Nombre total de pixels
		int sommeTotale = 0;// Somme des poids totale
		for (int j = 0; j < histogram.length; j++) {
			sommeTotale += j * histogram[j];
			nombreTotal += histogram[j];
		}

		int nombreClasse1 = 0;// Nombre de pixels dans la classe 1
		int sommeClasse1 = 0;// Somme des poids de la classe 1

		double maxVariances = 0;// Max des variances
		int maxSeuil = 0;// Seuil max

		for (int t = 0; t < histogram.length; t++) {
			nombreClasse1 += histogram[t];// Nouveau nombre dans la classe 1
			sommeClasse1 += t * histogram[t];// Nouvelle somme de poids dans la classe 1

			float pbClasse1 = (float) nombreClasse1 / (float) nombreTotal;// Pb d'etre dans la classe 1
			float pbClasse2 = 1 - pbClasse1;// Pb d'etre dans la classe 2

			float moyenneClasse1 = (float) sommeClasse1 / (float) nombreClasse1;// Moyenne de la classe 1
			float moyenneClasse2 = (float) (sommeTotale - sommeClasse1) / (float) (nombreTotal - nombreClasse1);// Moyenne
			// de la classe 2
			float variance = pbClasse1 * pbClasse2 * (moyenneClasse1 - moyenneClasse2)
					* (moyenneClasse1 - moyenneClasse2);// Variance au seuil t

			// On maximise la variance
			if (variance > maxVariances) {
				maxVariances = variance;
				maxSeuil = t;
			}
		}

		// On renvoie le seuil qui correspond � la variance maximale
		return maxSeuil;
	}
}