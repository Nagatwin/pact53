package TraitementImage;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;

import javax.imageio.ImageIO;

public abstract class Otsu {
	/**
	 * Applique Otsu � toutes les images du dossier Samples (qui n'ont pas �t� trait�es)
	 * La sortie est dans le dossier Output
	 */
	public static void TestFromSamples() {
		File folder = new File("data/Samples");
		File[] listOfFiles = folder.listFiles();

		for (int i = 0; i < listOfFiles.length; i++)
			if (listOfFiles[i].isFile()) {
				File out = new File("data/Output/" + listOfFiles[i].getName());
				if (!out.exists())
					applyOtsu(listOfFiles[i].getPath(), "data/Output/" + listOfFiles[i].getName());
			}
	}

	/**
	 * Applique l'algorithme d'Otsu � une image stock�e dans un fichier
	 * @param inputFile Chemin d'acc�s au fichier d'entr�e
	 * @param outputFile Chemin d'acces au fichier de sortie
	 */
	public static void applyOtsu(String inputFile, String outputFile) {
		try {
			LocalDateTime startDate = LocalDateTime.now();
			System.out.println("Computing " + inputFile);
			
			// On lit l'image source
			BufferedImage img = ImageIO.read(new File(inputFile));

			// On definit le fichier source
			File output = new File(outputFile);

			// On calcule l'image resultante
			BufferedImage resultat = getResultOtsu(img);

			// On ecrit dans le fichier
			ImageIO.write(resultat, "jpg", output);
			System.out.println("Done in " + Duration.between(startDate, LocalDateTime.now()).toMillis() + "ms");
		} catch (IOException e) {
			System.out.println("Error");
		}
	}


	/**
	 * Renvoie l'image r�sultate de l'algorithme d'Otsu
	 * @param img Image de d�part, �ventuellement en couleurs
	 * @return Image de sortie
	 */
	public static BufferedImage getResultOtsu(BufferedImage img) {
		// On r�cup�re la matrice en niveaux de gris
		int[][] matrix = getImageMatrix(img);

		// On calcule l'histogramme
		int[] hist = calculHistogramme(matrix);
		
		// On r�cup�re le seuil avec Otsu
		int seuil = OtsuMethod(hist);

		// On calcule la matrice binaire
		int[][] bin = binImage(matrix, seuil);
		
		// On r�cup�re l'image avec 2 niveaux de gris
		BufferedImage res = image2Niveaux(bin);

		return res;
	}

	/**
	 * Renvoie la matrice de l'image en niveau de gris en moyennant RGB
	 * @param image Image de d�part (peut �tre en rgb)
	 * @return Matrice de niveaux de gris correspondante
	 */
	private static int[][] getImageMatrix(BufferedImage image) {
		//On cr�e la matrice r�sultat
		int[][] result = new int[image.getWidth()][image.getHeight()];

		//On calculela moyenne pour tout les pixels
		for (int i = 0; i < image.getWidth(); i++)
			for (int j = 0; j < image.getHeight(); j++) {
				Color pixel = new Color(image.getRGB(i, j));
				result[i][j] = (pixel.getBlue() + pixel.getRed() + pixel.getGreen()) / 3;// On moyenne rgb
			}
		
		return result;
	}


	/**
	 * Calcule l'histogramme d'une image stock�e dans une matrice en niveaux de gris
	 * @param image Matrice de l'image en niveaux de gris
	 * @return Histogramme de l'image
	 */
	private static int[] calculHistogramme(int[][] image) {
		// Recherche du niveau de gris max
		int maxlevel = 0;
		for (int i = 0; i < image.length; i++)
			for (int j = 0; j < image[0].length; j++)
				if (image[i][j] > maxlevel)
					maxlevel = image[i][j];

		// Cr�ation du tableau d'histogramme
		int[] resultat = new int[maxlevel + 1];

		// Construction de l'histogramme
		for (int i = 0; i < image.length; i++)
			for (int j = 0; j < image[0].length; j++)
				resultat[image[i][j]]++;

		// Renvoi du r�sultat
		return resultat;
	}

	
	/**
	 * Applique l'algorithme d'Otsu a un histogramme donn�
	 * @param histogramme Histogramme de l'image
	 * @return	Seuil de d�coupe calcul� par l'algorithme d'Otsu
	 */
	private static int OtsuMethod(int[] histogramme) {
		int nombreTotal = 0;// Nombre total de pixels
		int sommeTotale = 0;// Somme des poids totale
		for (int j = 0; j < histogramme.length; j++) {
			sommeTotale += j * histogramme[j];
			nombreTotal += histogramme[j];
		}

		int nombreClasse1 = 0;// Nombre de pixels dans la classe 1
		int sommeClasse1 = 0;// Somme des poids de la classe 1

		double maxVariances = 0;// Max des variances
		int maxSeuil = 0;// Seuil max

		for (int t = 0; t < histogramme.length; t++) {
			nombreClasse1 += histogramme[t];// Nouveau nombre dans la classe 1
			sommeClasse1 += t * histogramme[t];// Nouvelle somme de poids dans la classe 1

			float pbClasse1 = (float) nombreClasse1 / (float) nombreTotal;// Pb d'etre dans la classe 1
			float pbClasse2 = 1 - pbClasse1;// Pb d'etre dans la classe 2

			float moyenneClasse1 = (float) sommeClasse1 / (float) nombreClasse1;// Moyenne de la classe 1
			float moyenneClasse2 = (float) (sommeTotale - sommeClasse1) / (float) (nombreTotal - nombreClasse1);// Moyenne
			// de la classe 2
			float variance = pbClasse1 * pbClasse2 * (moyenneClasse1 - moyenneClasse2)
					* (moyenneClasse1 - moyenneClasse2);// Variance au seuil t

			// On maximise la variance
			if (variance > maxVariances) {
				maxVariances = variance;
				maxSeuil = t;
			}
		}

		// On renvoie le seuil qui correspond � la variance maximale
		return maxSeuil;
	}

	/**
	 * Renvoie la matrice binaire pour le seuil donn�
	 * @param image Matrice des pixels de l'image en niveaux de gris
	 * @param seuil Seuil de d�coupe
	 * @return Matrice binaire (0 et 1)
	 */
	private static int[][] binImage(int[][] image, int seuil) {
		//On d�fini le r�sultat
		int[][] result = new int[image.length][image[0].length];
		
		//On remplit les cases : celles au-dessus du seuil avec 1; 0 pour les autres
		for (int i = 0; i < image.length; i++)
			for (int j = 0; j < image[0].length; j++)
				if (image[i][j] > seuil)
					result[i][j] = 1;
				else
					result[i][j] = 0;
		return result;
	}

	/**
	 * Renvoie une image � 2 niveaux � partir d'une matrice binaire
	 * @param matrix2niveaux Matrice de 0 et 1
	 * @return Image � 2 niveaux de gris
	 */
	private static BufferedImage image2Niveaux(int[][] matrix2niveaux) {
		//Nouvelle image de m�me taille.
		//3eme parametre : On peut mettre en niveaux de gris
		BufferedImage result = new BufferedImage(matrix2niveaux.length, matrix2niveaux[0].length,
				BufferedImage.TYPE_INT_RGB);
		
		//Pour chaque pixel, si il est � 1 on met la case en noir
		for (int i = 0; i < matrix2niveaux.length; i++) {
			for (int j = 0; j < matrix2niveaux[0].length; j++) {
				int niveau = 255 * matrix2niveaux[i][j];
				Color c = new Color(niveau, niveau, niveau);
				result.setRGB(i, j, c.getRGB());
			}
		}
		
		return result;
	}
}
