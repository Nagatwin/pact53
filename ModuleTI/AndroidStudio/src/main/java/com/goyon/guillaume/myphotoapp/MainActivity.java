package com.goyon.guillaume.myphotoapp;

import android.content.Intent;
import android.graphics.Bitmap;
import android.media.MediaScannerConnection;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;

public class MainActivity extends AppCompatActivity {
    static final int REQUEST_IMAGE_CAPTURE = 1;
    ImageView mImageView;
    TextView mTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mImageView = (ImageView) findViewById(R.id.imageView);
        mTextView = (TextView) findViewById(R.id.textView);

        dispatchTakePictureIntent();
    }


    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            Bundle extras = data.getExtras();
            Bitmap bi;
            bi = (Bitmap) extras.get("data");
            bi.setHasAlpha(false);
            PlanImage pi = new PlanImage(bi);


            try {
                Bitmap imageBitmap = (pi.getFinishedBinImage());
                //pi.doAnalysis();
                Log.i("MPA_WP", getFilesDir().getAbsolutePath());
                writeToFile(pi.getXMLText());
                mImageView.setImageBitmap(imageBitmap);
            } catch (PhotoDegueuException e) {
                dispatchTakePictureIntent();
            }catch (Exception ex) {
                mTextView.setText(ex.getLocalizedMessage());
            }
        }
    }

    public void writeToFile(String data) {
        Log.i("MPA_WP", getFilesDir().getAbsolutePath());
        try {
            File file = new File(this.getExternalFilesDir(null), "plan.xml");
            Log.i("MPA_WP", "" + file.exists());

            FileOutputStream fileOutput = new FileOutputStream(file);
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(fileOutput);
            outputStreamWriter.write(data);
            outputStreamWriter.flush();
            fileOutput.getFD().sync();
            outputStreamWriter.close();
            MediaScannerConnection.scanFile(
                    this,
                    new String[]{file.getAbsolutePath()},
                    null,
                    null);
        } catch (Exception ex) {
            Log.i("MPA_WP", "ugh");
        }
    }
}
