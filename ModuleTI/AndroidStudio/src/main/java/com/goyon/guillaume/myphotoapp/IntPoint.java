package com.goyon.guillaume.myphotoapp;

public class IntPoint {
	private final int x, y;

	public IntPoint(int x, int y) {
		this.x = x;
		this.y = y;
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public DoublePoint toFloatPoint() {
		return new DoublePoint(x, y);
	}

    public int compareTo(IntPoint p2) {
			if (Math.abs(x- p2.getX()) ==0){
				if (Math.abs(y- p2.getY()) ==0) {
					return 0;
				}
				else return  y - p2.getY();
			} else
				return x-p2.getX();
    }

	public DoublePoint toDoublePoint() {
		return new DoublePoint(x,y);
	}
}
