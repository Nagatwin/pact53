package com.example.tarik.map_it;                                                                                                                                                                                                                                                                  ;import android.support.annotation.NonNull;

public class DoublePoint {
	private double _x;
	private double _y;

	public DoublePoint(double x, double y) {
		_x = x;
		_y = y;
	}

	public double getX() {
		return _x;
	}

	public double getY() {
		return _y;
	}

	public double distanceTo(DoublePoint p) {
		return (double) Math.sqrt(Math.pow((_x - p.getX()), 2) + Math.pow((_y - p.getY()), 2));
	}

	public double compareTo(DoublePoint p2) {
		if (Math.abs(_x- p2.getX()) < CONST.DOUBLE_MIN_VALUE){
			if (Math.abs(_y- p2.getY()) < CONST.DOUBLE_MIN_VALUE) {
				return 0;
			}
			else return  _y - p2.getY();
		} else
			return _x-p2.getX();
	}
}