package com.example.tarik.map_it;


import java.util.Arrays;

public abstract class MathLinear {
	/**
	 * R�souds le syst�me par pivot de Gauss
	 * Ne marche que sur des syst�mes carr�s avec des �quations non proportionelles
	 * @param systemA Matrice du syst�me
	 * @param systemB Le vecteur membre de droite
	 * @return Le vecteur solution
	 * @throws Exception
	 */
	public static double[] pivotDeGauss(double[][] systemA, double[] systemB) throws Exception {
		int n = systemA.length;
		if (n != systemA[0].length || n != systemB.length)
			throw new Exception("Dimensions du syst�me incorrecte (pas carr� ou pas m�me dimension).");

		// VERIF CLONAGE
		double[][] MA = clonefA(systemA);
		double[] MB = clonefB(systemB);

		// Pour chaque colonne
		for (int j = 0; j < n; j++) {

			// On trouve un coeff non nul
			int i0 = j;
			while (i0 < n && MA[i0][j] == 0) {
				i0++;
			}
			if (i0 == n)
				throw new Exception("Pas de coefficient non nul");

			// On swap les lignes
			if (i0 != j) {
				double[] tempA = MA[i0];
				MA[i0] = MA[j];
				MA[j] = tempA;

				double tempB = MB[i0];
				MB[i0] = MB[j];
				MB[j] = tempB;
			}

			// On triangularise
			if (j < n - 1) {
				for (int i = j + 1; i < n; i++) {
					double coeff = MA[i][j] / MA[j][j];
					for (int k = j; k < n; k++) {
						MA[i][k] = MA[i][k] - MA[j][k] * coeff;
					}
					MB[i] = MB[i] - MB[j] * coeff;
				}
			}
		}

		// On a la matrice Triangulaire

		// On remonte alors pour avoir le r�sultat
		double[] result = new double[n];
		result[n - 1] = MB[n - 1] / MA[n - 1][n - 1];

		for (int i = (n - 2); i >= 0; i--) {
			double somme = MB[i];
			for (int k = i + 1; k < n; k++) {
				somme -= MA[i][k] * result[k];
			}
			result[i] = somme / MA[i][i];
		}
		return result;
	}

	/**
	 * Teste le pivot de gauss avec un syst�me
	 */
	public static void TestFromSamples() {
		double[][] A = new double[3][3];
		double[] B = new double[3];
		A[0][0] = 1;
		A[0][1] = 2;
		A[0][2] = 1;
		A[1][0] = 2;
		A[1][1] = 3;
		A[1][2] = 2;
		A[2][0] = 1;
		A[2][1] = 3;
		A[2][2] = 2;
		B[0] = 1;
		B[1] = 3;
		B[2] = 1;
		try {
			System.out.println(Arrays.toString(MathLinear.pivotDeGauss(A, B)));;
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// Clone un tableau de flotants de dimention 2; copie ind�pendante
	private static double[][] clonefA(double[][] tab) {
		double[][] result = new double[tab.length][tab[0].length];
		for (int i = 0; i < tab.length; i++)
			for (int j = 0; j < tab[0].length; j++)
				result[i][j] = tab[i][j];
		return result;
	}

	// Clone un tableau de flotants de dimention 1; copie ind�pendante
	private static double[] clonefB(double[] tab) {
		double[] result = new double[tab.length];
		for (int i = 0; i < tab.length; i++)
			result[i] = tab[i];
		return result;
	}
}
