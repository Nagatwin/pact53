package com.example.tarik.map_it;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

public class NewProject extends AppCompatActivity {
public static String name;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_project);

        Button create = findViewById(R.id.button4);

        create.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                name = ((EditText) findViewById(R.id.editText)).getText().toString();

                startActivity(new Intent(NewProject.this,TakePlan.class));


            }
        });
    }
}
