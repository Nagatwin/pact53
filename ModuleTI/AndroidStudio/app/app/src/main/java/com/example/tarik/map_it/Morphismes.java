package com.example.tarik.map_it;

public abstract class Morphismes {

	protected static int[][] test(int[][] matrix) { // On r�cup�re le seuil avec Otsu
		return Morphismes.dilateThenErode(Morphismes.erodeThenDilate(matrix, 2),2);
	}

	protected static int[][] erodeThenDilate(int[][] matrix, int n) {
		int[][] result = matrix;
		for (int i = 0; i < n; i++)
			result = erode(result);
		for (int i = 0; i < n; i++)
			result = dilate(result);
		return result;
	}

	protected static int[][] dilateThenErode(int[][] matrix, int n) {
		int[][] result = matrix;
		for (int i = 0; i < n; i++)
			result = dilate(result);
		for (int i = 0; i < n; i++)
			result = erode(result);
		return result;
	}

	protected static int[][] dilate(int[][] matrix) {
		int[][] result = new int[matrix.length][matrix[0].length];

		for (int i = 0; i < matrix.length; i++)
			for (int j = 0; j < matrix[0].length; j++)
				result[i][j] = 1;

		for (int i = 0; i < matrix.length; i++)
			for (int j = 0; j < matrix[0].length; j++)
				if (matrix[i][j] == 0) {
					result[i][j] = 0;
					if (0 < i) {
						if (0 < j)
							result[i - 1][j - 1] = 0;
						result[i - 1][j] = 0;
						if (j < matrix[0].length - 1)
							result[i - 1][j + 1] = 0;
					}
					if (i < matrix.length - 1) {
						if (0 < j)
							result[i + 1][j - 1] = 0;
						result[i + 1][j] = 0;
						if (j < matrix[0].length - 1)
							result[i + 1][j + 1] = 0;
					}
					if (0 < j)
						result[i][j - 1] = 0;
					if (j < matrix[0].length - 1)
						result[i][j + 1] = 0;
				}
		return result;
	}

	protected static int[][] erode(int[][] matrix) {
		int[][] result = new int[matrix.length][matrix[0].length];
		for (int i = 0; i < matrix.length; i++)
			for (int j = 0; j < matrix[0].length; j++)
				if (matrix[i][j] == 1) {
					result[i][j] = 1;
					if (0 < i) {
						if (0 < j)
							result[i - 1][j - 1] = 1;
						result[i - 1][j] = 1;
						if (j < matrix[0].length - 1)
							result[i - 1][j + 1] = 1;
					}
					if (i < matrix.length - 1) {
						if (0 < j)
							result[i + 1][j - 1] = 1;
						result[i + 1][j] = 1;
						if (j < matrix[0].length - 1)
							result[i + 1][j + 1] = 1;
					}
					if (0 < j)
						result[i][j - 1] = 1;
					if (j < matrix[0].length - 1)
						result[i][j + 1] = 1;
				}
		return result;
	}
}
