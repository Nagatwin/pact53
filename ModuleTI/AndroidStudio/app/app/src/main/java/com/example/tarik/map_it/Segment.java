package com.example.tarik.map_it;

import android.util.Log;

import java.util.ArrayList;

public class Segment {
	private DoublePoint a, b;
	private Droite droite=null ;
    private ArrayList<DoublePoint> doorPoints = new ArrayList<>();
    private DoublePoint d1,d2;
    private ArrayList<DoublePoint> windowPoints = new ArrayList<>();
    private DoublePoint w1,w2;


	public Segment(DoublePoint a, DoublePoint b) {
		this.a = a;
		this.b = b;
	}

	public Segment(DoublePoint a, DoublePoint b,Droite droite) {
		this.a = a;
		this.b = b;
		this.droite=droite;
	}

	public Droite getDroite() {
		return this.droite ;
	}

	public DoublePoint getA() {
		return this.a ;
	}

	public DoublePoint getB() {
		return this.b ;
	}

	public boolean isOnSegment(DoublePoint p){
		return ((p.getX()>=Math.min(a.getX(), b.getX())&& p.getX()<=Math.max(a.getX(), b.getX()) &&
				p.getY()>=Math.min(a.getY(), b.getY())&& p.getY()<=Math.max(a.getY(), b.getY()))&&(droite.distToPoint(p)<2));
	}

    public boolean isOnDoor(IntPoint p){
        if(d1!=null)
            return ((p.getX()>=Math.min(d1.getX(), d2.getX())&& p.getX()<=Math.max(d1.getX(), d2.getX()) &&
                    p.getY()>=Math.min(d1.getY(), d2.getY())&& p.getY()<=Math.max(d1.getY(), d2.getY()))&&(droite.distToPoint(p.toDoublePoint())<2));
        else return false;
    }

    public void addDoorPoint(IntPoint intPoint) {
        DoublePoint proj = droite.getProject(intPoint.toDoublePoint());
        int k = 0;
        while (k < doorPoints.size() && doorPoints.get(k).compareTo(proj)<0)
            k++;
        doorPoints.add(k,proj);
    }

    public Segment getDoor() {
        if(3 < doorPoints.size()){
            d1 = doorPoints.get(0);
            d2=doorPoints.get(doorPoints.size()-1);
            Log.i("MPA_DOOR", d1.getX() +";"+d1.getY() + ":" + d2.getX() + ";" + d2.getY());
            return new Segment(d1,d2,droite);
        }
        return null;
    }

    public boolean isOnWindow(IntPoint p){
        if(w1!=null)
            return ((p.getX()>=Math.min(w1.getX(), w2.getX())&& p.getX()<=Math.max(w1.getX(), w2.getX()) &&
                    p.getY()>=Math.min(w1.getY(), w2.getY())&& p.getY()<=Math.max(w1.getY(), w2.getY()))&&(droite.distToPoint(p.toDoublePoint())<2));
        else return false;
    }

    public void addWindowPoint(IntPoint intPoint) {
        DoublePoint proj = droite.getProject(intPoint.toDoublePoint());
        int k = 0;
        while (k < windowPoints.size() && windowPoints.get(k).compareTo(proj)<0)
            k++;
        windowPoints.add(k,proj);
    }

    public Segment getWindow() {
        if(3 < windowPoints.size()){
            w1 = windowPoints.get(0);
            w2=windowPoints.get(windowPoints.size()-1);

            Log.i("MPA_DOOR", w1.getX() +";"+w1.getY() + ":" + w2.getX() + ";" + w2.getY());
            return new Segment(w1,w2,droite);
        }
        return null;
    }

    public Segment translate(DoublePoint p){
        return new Segment(new DoublePoint(a.getX()-p.getX(),a.getY()-p.getY()),new DoublePoint(b.getX()-p.getX(),b.getY()-p.getY()),droite);
    }

    public Segment scale(double factor){
        return new Segment(new DoublePoint(a.getX()*factor,a.getY()*factor), new DoublePoint(b.getX()*factor, b.getY()*factor),droite);
    }

    public double length() {
        return a.distanceTo(b);
    }
}
