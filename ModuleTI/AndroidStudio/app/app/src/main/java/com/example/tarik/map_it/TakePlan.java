package com.example.tarik.map_it;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

public class TakePlan extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_take_plan);

        Button confirm = findViewById(R.id.confirm);
        Button takePicture = findViewById(R.id.takePicture);
        TextView text = findViewById(R.id.textView5);
        text.setText(NewProject.name);

        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(TakePlan.this,PlanView.class));
            }
        });

        takePicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(TakePlan.this, TakeAPicture.class));
            }
        });
    }
}
