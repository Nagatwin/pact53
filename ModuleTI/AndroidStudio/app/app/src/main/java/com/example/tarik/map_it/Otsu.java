package com.example.tarik.map_it;

import java.io.File;
import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;

public abstract class Otsu {
	protected static int getOtsuFloor(int[][] matrix) {
		return getOtsuFloorFromHistogram(getHistogram(matrix));
	}

	protected static int getOtsuFloor(int[] tab) {
		return getOtsuFloorFromHistogram(getHistogram(tab));
	}

	/**
	 * Calcule l'histogram d'une matrix stock�e dans une matrice en niveaux de gris
	 * 
	 * @param matrix
	 *            Matrice de l'matrix en niveaux de gris
	 * @return histogram de l'matrix
	 */
	private static int[] getHistogram(int[][] matrix) {
		// Recherche du niveau de gris max
		int maxlevel = 0;
		int width = matrix.length;
		int height = matrix[0].length;

		for (int i = 0; i < width; i++)
			for (int j = 0; j < height; j++)
				if (matrix[i][j] != -1 && matrix[i][j] > maxlevel)
					maxlevel = matrix[i][j];

		// Cr�ation du tableau d'histogramme
		int[] result = new int[maxlevel + 1];

		// Construction de l'histogramme
		for (int i = 0; i < width; i++)
			for (int j = 0; j < height; j++)
				if (matrix[i][j] != -1)
					result[matrix[i][j]]++;

		// Renvoi du r�sultat
		return result;
	}
	private static int[] getHistogram(int[] tab) {
		// Recherche du niveau de gris max
		int maxlevel = 0;
		int width = tab.length;

		for (int i = 0; i < width; i++)
				if (tab[i] != -1 && tab[i] > maxlevel)
					maxlevel = tab[i];

		// Cr�ation du tableau d'histogramme
		int[] result = new int[maxlevel + 1];

		// Construction de l'histogramme
		for (int i = 0; i < width; i++)
					result[tab[i]]++;

		// Renvoi du r�sultat
		return result;
	}

	/**
	 * Applique l'algorithme d'Otsu a un histogram donn�
	 * 
	 * @param histogram
	 *            histogram de l'image
	 * @return Seuil de d�coupe calcul� par l'algorithme d'Otsu
	 */
	private static int getOtsuFloorFromHistogram(int[] histogram) {
		int nombreTotal = 0;// Nombre total de pixels
		int sommeTotale = 0;// Somme des poids totale
		for (int j = 0; j < histogram.length; j++) {
			sommeTotale += j * histogram[j];
			nombreTotal += histogram[j];
		}

		int nombreClasse1 = 0;// Nombre de pixels dans la classe 1
		int sommeClasse1 = 0;// Somme des poids de la classe 1

		double maxVariances = 0;// Max des variances
		int maxSeuil = 0;// Seuil max

		for (int t = 0; t < histogram.length; t++) {
			nombreClasse1 += histogram[t];// Nouveau nombre dans la classe 1
			sommeClasse1 += t * histogram[t];// Nouvelle somme de poids dans la classe 1

			float pbClasse1 = (float) nombreClasse1 / (float) nombreTotal;// Pb d'etre dans la classe 1
			float pbClasse2 = 1 - pbClasse1;// Pb d'etre dans la classe 2

			float moyenneClasse1 = (float) sommeClasse1 / (float) nombreClasse1;// Moyenne de la classe 1
			float moyenneClasse2 = (float) (sommeTotale - sommeClasse1) / (float) (nombreTotal - nombreClasse1);// Moyenne
			// de la classe 2
			float variance = pbClasse1 * pbClasse2 * (moyenneClasse1 - moyenneClasse2)
					* (moyenneClasse1 - moyenneClasse2);// Variance au seuil t

			// On maximise la variance
			if (variance > maxVariances) {
				maxVariances = variance;
				maxSeuil = t;
			}
		}

		// On renvoie le seuil qui correspond � la variance maximale
		return maxSeuil;
	}
}