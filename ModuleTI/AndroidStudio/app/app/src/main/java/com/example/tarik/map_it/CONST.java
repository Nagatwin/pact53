package com.example.tarik.map_it;

/**
 * Created by Guillaume on 5/2/2018.
 */

public abstract class CONST {
    static final double DOUBLE_MIN_VALUE = 0.0001f,
    //Angle de correction de droite
            ANGLE_DIFF_MIN = 0.05;

    //Seuils de saturation
    static final double S_FLOOR_WALL = 0.3;
    static final double S_FLOOR_WINDOW = 0.3;
    static final double S_FLOOR_DOOR = 0.4;

    //Angles de reconnaissance (en degrés)
    static final double MIN_GREEN_ANGLE = 36.0;
    static final double MAX_GREEN_ANGLE = 180;
    static final double MIN_RED_ANGLE = 180;
    static final double MAX_RED_ANGLE = 70;

    static final double PERCENT_OF_POINTS = 0.15;//% des points pour generer une droite de test
    static final double PERCENT_STOP_TOTAL = 0.05;//Nombre de points enleves
}
