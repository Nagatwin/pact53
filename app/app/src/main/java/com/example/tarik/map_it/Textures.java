package com.example.tarik.map_it;

import android.content.Intent;
import android.graphics.Bitmap;
import android.media.MediaScannerConnection;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;


public class Textures extends AppCompatActivity {

    static final int REQUEST_IMAGE_CAPTURE = 1;
    ImageView mImageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Button editMyPlan = findViewById(R.id.edit_my_plan);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_take_apicture);
        mImageView = (ImageView) findViewById(R.id.imageView);
        dispatchTakePictureIntent();
        editMyPlan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Textures.this, LoadedProject.class));
            }
        });
    }


    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            Bundle extras = data.getExtras();
            Bitmap bi;
            bi = (Bitmap) extras.get("data");
            bi.setHasAlpha(false);
            PlanImage pi = new PlanImage(bi);

            try {
                Bitmap imageBitmap = (pi.getFinishedBinImage());
                //pi.doAnalysis();
                writeToFile(pi.getXMLText());
                mImageView.setImageBitmap(imageBitmap);
            } catch (PhotoDegueuException e) {
                dispatchTakePictureIntent();
            } catch (Exception ex) {
                dispatchTakePictureIntent();
            }
        }
    }

    public void writeToFile(String data) {
        try {
            File file = new File(this.getExternalFilesDir(null), "plan.xml");

            FileOutputStream fileOutput = new FileOutputStream(file);
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(fileOutput);
            outputStreamWriter.write(data);
            outputStreamWriter.flush();
            fileOutput.getFD().sync();
            outputStreamWriter.close();
            MediaScannerConnection.scanFile(
                    this,
                    new String[]{file.getAbsolutePath()},
                    null,
                    null);
        } catch (Exception ex) {
        }

    }
}
