package com.example.tarik.map_it;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

public class TakePlan extends AppCompatActivity {
    ImageView mImageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_take_plan);


        mImageView = (ImageView) findViewById(R.id.imageView);
        Button browse = findViewById(R.id.browse);
        Button confirm = findViewById(R.id.confirm);
        Button takePicture = findViewById(R.id.takePicture);
        TextView text = findViewById(R.id.textView5);
        text.setText(NewProject.name);


        browse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mediaChooser = new Intent(Intent.ACTION_GET_CONTENT);
                mediaChooser.setType("image/*");
                startActivityForResult(mediaChooser, 1);
            }


            public void onActivityResult(int requestCode, int resultCode, Intent data) {

                if (resultCode == Activity.RESULT_OK) {
                    if (data != null && data.getData() != null) {
                        Uri uri = data.getData();
                        mImageView.setImageURI(uri);
                    }
                }
            }

        });

        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(TakePlan.this,PlanView.class));

            }
        });

        takePicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(TakePlan.this, TakeAPicture.class));
            }
        });
    }
}
