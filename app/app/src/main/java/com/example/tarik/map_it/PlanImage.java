package com.example.tarik.map_it;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

public class PlanImage {
    private static final float S_FLOOR_WALL = 0.1f;
    private static final float S_FLOOR_WINDOW = 0.3f;
    private static final float S_FLOOR_DOOR = 0.6f;
    private static final float MIN_GREEN_ANGLE = 36.0f;
    private static final float MAX_GREEN_ANGLE = 180f;
    private static final float MIN_RED_ANGLE = 185f;
    private static final float MAX_RED_ANGLE = 72f;
    private final Bitmap bitmap;
    private final float[][][] HSBMatrix;
    private final int height, width;
    private ArrayList<Segment> segmentsFound;
    private ArrayList<Segment> doorsFound;
    private ArrayList<Segment> windowsFound;
    private double xMin, xMax, yMin, yMax;

    protected PlanImage(Bitmap bitmap) {
        this.bitmap = bitmap;
        this.height = bitmap.getHeight();
        this.width = bitmap.getWidth();
        this.HSBMatrix = new float[this.width][this.height][3];
        buildHSB();
    }

    private static int[][] getBinMatrix(int floor, int[][] matrix) {
        int matWidth = matrix.length;
        int matHeight = matrix[0].length;
        int[][] result = new int[matWidth][matHeight];
        for (int i = 0; i < matWidth; i++)
            for (int j = 0; j < matHeight; j++)
                result[i][j] = (matrix[i][j] <= floor) ? 0 : 1;

        return result;
    }

    private static Bitmap getBinImage(int floor, int[][] matrix) {
        int matWidth = matrix.length;
        int matHeight = matrix[0].length;
        Bitmap result = Bitmap.createBitmap(matWidth, matHeight, Bitmap.Config.ARGB_8888);

        for (int i = 0; i < matWidth; i++)
            for (int j = 0; j < matHeight; j++)
                result.setPixel(i, j, ((matrix[i][j] <= floor) ? Color.WHITE : Color.BLACK));

        return result;
    }

    private static Bitmap getBinImage(int[][] matrix) {
        int matWidth = matrix.length;
        int matHeight = matrix[0].length;
        Bitmap result = Bitmap.createBitmap(matWidth, matHeight, Bitmap.Config.ARGB_8888);
        for (int i = 0; i < matWidth; i++)
            for (int j = 0; j < matHeight; j++)
                result.setPixel(i, j, ((matrix[i][j] == 0) ? Color.WHITE : Color.BLACK));
        return result;
    }

<<<<<<< HEAD
    private static Bitmap get3CImage(int[][] matrix) {
        int matWidth = matrix.length;
        int matHeight = matrix[0].length;
        Bitmap result = Bitmap.createBitmap(matWidth, matHeight, Bitmap.Config.ARGB_8888);
        for (int i = 0; i < matWidth; i++)
            for (int j = 0; j < matHeight; j++)
                switch (matrix[i][j]) {
                    case 0:
                        result.setPixel(i, j, Color.WHITE);
                        break;
                    case 1:
                        result.setPixel(i, j, Color.BLACK);
                        break;
                    case 2:
                        result.setPixel(i, j, Color.RED);
                        break;
                    case 3:

                        result.setPixel(i, j, Color.GREEN);
                        break;
                }
        return result;
    }

=======
>>>>>>> Module_Test_Integration
    private void buildHSB() {
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                int color = bitmap.getPixel(i, j);
                float[] pixelHSB = new float[3];
                Color.RGBToHSV((color >> 16) & 0xff, (color >> 8) & 0xff, (color) & 0xff, pixelHSB);
                HSBMatrix[i][j][0] = pixelHSB[0];
                HSBMatrix[i][j][1] = pixelHSB[1];
                HSBMatrix[i][j][2] = pixelHSB[2];
            }
        }
    }

    /**
     * Renvoie la matrice de l'image en niveau de gris en moyennant RGB
     *
     * @return Matrice de niveaux de gris correspondante
     */
    private int[][] getAverageRGBMatrix() {
        int[][] result = new int[bitmap.getWidth()][bitmap.getHeight()];
        for (int i = 0; i < bitmap.getWidth(); i++)
            for (int j = 0; j < bitmap.getHeight(); j++) {
                int color = bitmap.getPixel(i, j);
                result[i][j] = 255 - (((color >> 16) & 0xff) + ((color >> 8) & 0xff) + ((color) & 0xff)) / 3;// On moyenne rgb
            }
        return result;
    }

    protected Bitmap getBitmap() {
        return bitmap;
    }

    private int[][] getMatrixFromSValue(float sFloor) {
        int[][] result = new int[width][height];
        for (int i = 0; i < width; i++)
            for (int j = 0; j < height; j++)
                result[i][j] = (HSBMatrix[i][j][1] <= sFloor) ? (int) (255 - HSBMatrix[i][j][2] * 255) : -1;
        return result;
    }

    private int[][] getMatrixFromHValue(float sFloor, float minAngle, float maxAngle) {
        int[][] result = new int[width][height];
        for (int i = 0; i < width; i++)
            for (int j = 0; j < height; j++)

                result[i][j] = (HSBMatrix[i][j][1] > sFloor
                        && ((HSBMatrix[i][j][0] < maxAngle && HSBMatrix[i][j][0] > minAngle) || (maxAngle < minAngle
                        && (HSBMatrix[i][j][0] < maxAngle || HSBMatrix[i][j][0] > minAngle))))
                        ? Math.round(255 - HSBMatrix[i][j][1] * 255)
                        : -1;
        return result;
    }

    protected int[][] getGreyBinMatrix() {
        int[][] matrix = this.getAverageRGBMatrix();
        return PlanImage.getBinMatrix(Otsu.getOtsuFloor(matrix), matrix);
    }

    protected Bitmap getGreyBinImage() {
        int[][] matrix = this.getAverageRGBMatrix();
        return PlanImage.getBinImage(Otsu.getOtsuFloor(matrix), matrix);
    }

    protected int[][] getWallBinMatrix() {
        int[][] matrix = this.getMatrixFromSValue(S_FLOOR_WALL);
        return PlanImage.getBinMatrix(Otsu.getOtsuFloor(matrix), matrix);
    }

    protected Bitmap getWallBinImage() {
        int[][] matrix = this.getMatrixFromSValue(S_FLOOR_WALL);
        return PlanImage.getBinImage(Otsu.getOtsuFloor(matrix), matrix);
    }

    protected int[][] getDoorBinMatrix() {
        int[][] matrix = this.getMatrixFromHValue(S_FLOOR_DOOR, MIN_RED_ANGLE, MAX_RED_ANGLE);
        return PlanImage.getBinMatrix(Otsu.getOtsuFloor(matrix), matrix);
    }

    protected Bitmap getDoorBinImage() {
        int[][] matrix = this.getMatrixFromHValue(S_FLOOR_DOOR, MIN_RED_ANGLE, MAX_RED_ANGLE);
        return PlanImage.getBinImage(Otsu.getOtsuFloor(matrix), matrix);
    }

    protected int[][] getWindowBinMatrix() {
        int[][] matrix = this.getMatrixFromHValue(S_FLOOR_WINDOW, MIN_GREEN_ANGLE, MAX_GREEN_ANGLE);
        return PlanImage.getBinMatrix(Otsu.getOtsuFloor(matrix), matrix);
    }

    protected Bitmap getWindowBinImage() {
        int[][] matrix = this.getMatrixFromHValue(S_FLOOR_WINDOW, MIN_GREEN_ANGLE, MAX_GREEN_ANGLE);
        return PlanImage.getBinImage(Otsu.getOtsuFloor(matrix), matrix);
    }

    protected Bitmap getErodedWallBinImage() {
        return getBinImage(Morphismes.test(getWallBinMatrix()));
    }

    protected void getIntWallBinImage() {
        System.out.println(Ransac.chanfrein(Morphismes.test(getWallBinMatrix())));
    }

    protected Bitmap getRansacWallBinImage() throws Exception {
        segmentsFound = Ransac.ransac(getGreyBinMatrix());
        int[][] result = new int[width][height];
        for (int i = 0; i < width; i++)
            for (int j = 0; j < height; j++) {
                for (Segment s : segmentsFound)
                    if (s.isOnSegment(new DoublePoint(i, j)))
                        result[i][j] = 1;
            }

        return getBinImage(result);
    }

    protected void doAnalysis() throws PhotoDegueuException {
        Log.i("MPA_STEP", "APPLICATION RANSAC");
        segmentsFound = Ransac.ransac(getGreyBinMatrix());

        Log.i("MPA_STEP", segmentsFound.size() + " MURS OBTENUS");
        Log.i("MPA_STEP", "OBTENTION DES PORTES");
        int[][] doorBinMatrix = getDoorBinMatrix();
        for (int i = 0; i < width; i++)
            for (int j = 0; j < height; j++)
                for (Segment s : segmentsFound)
                    if (doorBinMatrix[i][j] == 1 && s.isOnSegment(new DoublePoint(i, j)))
                        s.addDoorPoint(new IntPoint(i, j));

        doorsFound = new ArrayList<>();
        for (Segment s : segmentsFound)
            if (s.getDoor() != null)
                doorsFound.add(s.getDoor());

        Log.i("MPA_STEP", doorsFound.size() + " PORTES TROUVEES");

        Log.i("MPA_STEP", "OBTENTION DES FENETRES");
        int[][] windowBinMatrix = getWindowBinMatrix();
        for (int i = 0; i < width; i++)
            for (int j = 0; j < height; j++)
                for (Segment s : segmentsFound)
                    if (windowBinMatrix[i][j] == 1 && s.isOnSegment(new DoublePoint(i, j)))
                        s.addWindowPoint(new IntPoint(i, j));

        windowsFound = new ArrayList<>();
        for (Segment s : segmentsFound)
            if (s.getWindow() != null)
                windowsFound.add(s.getWindow());
        Log.i("MPA_STEP", windowsFound.size() + " FENETRES TROUVEES");
<<<<<<< HEAD
        getMinMax();
        Log.i("MPA_STEP", "TERMINE");
    }

    protected Bitmap getFinished3CImage() throws PhotoDegueuException {
=======
getMinMax();
        Log.i("MPA_STEP", "TERMINE");
    }

    protected Bitmap getFinishedBinImage() throws PhotoDegueuException {
>>>>>>> Module_Test_Integration
        Log.i("MPA_STEP", "APPLICATION RANSAC");
        segmentsFound = Ransac.ransac(getGreyBinMatrix());

        Log.i("MPA_STEP", segmentsFound.size() + " MURS OBTENUS");
        Log.i("MPA_STEP", "OBTENTION DES PORTES");
        int[][] doorBinMatrix = getDoorBinMatrix();
        for (int i = 0; i < width; i++)
            for (int j = 0; j < height; j++)
                for (Segment s : segmentsFound)
                    if (doorBinMatrix[i][j] == 1 && s.isOnSegment(new DoublePoint(i, j)))
                        s.addDoorPoint(new IntPoint(i, j));

        doorsFound = new ArrayList<>();
        for (Segment s : segmentsFound)
            if (s.getDoor() != null)
                doorsFound.add(s.getDoor());

        Log.i("MPA_STEP", doorsFound.size() + " PORTES TROUVEES");

        Log.i("MPA_STEP", "OBTENTION DES FENETRES");
        int[][] windowBinMatrix = getWindowBinMatrix();
        for (int i = 0; i < width; i++)
            for (int j = 0; j < height; j++)
                for (Segment s : segmentsFound)
                    if (windowBinMatrix[i][j] == 1 && s.isOnSegment(new DoublePoint(i, j)))
                        s.addWindowPoint(new IntPoint(i, j));

        windowsFound = new ArrayList<>();
        for (Segment s : segmentsFound)
            if (s.getWindow() != null)
                windowsFound.add(s.getWindow());
        Log.i("MPA_STEP", windowsFound.size() + " FENETRES TROUVEES");
        getMinMax();

        Log.i("MPA_STEP", "CONSTRUCTION RESULTAT");
        int[][] result = new int[width][height];
        for (int i = 0; i < width; i++)
            for (int j = 0; j < height; j++) {
                for (Segment s : segmentsFound)
<<<<<<< HEAD
                    if (s.isOnDoor(new IntPoint(i, j)))
                        result[i][j] = 2;
                    else if (s.isOnWindow(new IntPoint(i, j)))
                        result[i][j] = 3;
                    else if (s.isOnSegment(new DoublePoint(i, j)))
=======
                    if (s.isOnSegment(new DoublePoint(i, j)))
>>>>>>> Module_Test_Integration
                        result[i][j] = 1;
            }

        Log.i("MPA_STEP", "TERMINE");

<<<<<<< HEAD
        return get3CImage(result);
    }

    public String getXMLText() {
        Log.i("MPA_STEP", "EXPORT");
        double min = minDoor();
        double factor = 1;
        if (min != 0)
            factor = 60.0 / min;
        Log.i("MPA_STEP", "Factor " + factor);
=======
        return getBinImage(result);
    }

    public String getXMLText() {
>>>>>>> Module_Test_Integration
        String result = "<?xml version=\"1.0\"?>\n<plan>\n" +
                "\t<core>\n" +
                "\t\t<wall_amount>" + segmentsFound.size() + "</wall_amount>\n" +
                "\t\t<door_amount>" + doorsFound.size() + "</door_amount>\n" +
                "\t\t<window_amount>" + windowsFound.size() + "</window_amount>\n" +
<<<<<<< HEAD
                "\t\t<xMin>" + (int)((double)((int)xMin) * factor) + "</xMin>\n" +
                "\t\t<xMax>" + (int)(xMax * factor+1) + "</xMax>\n" +
                "\t\t<yMin>" + (int)((double)((int)yMin) * factor) + "</yMin>\n" +
                "\t\t<yMax>" + (int)(yMax * factor+1) + "</yMax>\n" +
                "\t</core>\n" +
                "\t<walls>\n";
        int id = 0;
        for (Segment seg : segmentsFound) {
            Segment s = seg.scale(factor);
            Segment door = seg.getDoor();
            Segment window = seg.getWindow();
            result += "\t\t<wall id=\"" + id + "\">\n" +
                    "\t\t\t<x1>" + (int)s.getA().getX() + "</x1>\n" +
                    "\t\t\t<y1>" + (int)s.getA().getY() + "</y1>\n" +
                    "\t\t\t<x2>" + (int)s.getB().getX() + "</x2>\n" +
                    "\t\t\t<y2>" + (int)s.getB().getY() + "</y2>\n";
            if (door != null) {
                Segment d = door.scale(factor);
                result += "\t\t\t<door id=\"" + 0 + "\">\n" +
                        "\t\t\t\t<x1>" + (int)d.getA().getX() + "</x1>\n" +
                        "\t\t\t\t<y1>" + (int)d.getA().getY() + "</y1>\n" +
                        "\t\t\t\t<x2>" + (int)d.getB().getX() + "</x2>\n" +
                        "\t\t\t\t<y2>" + (int)d.getB().getY() + "</y2>\n" +
                        "\t\t\t</door>\n"
                ;
            }
            if (window != null) {
                Segment w = window.scale(factor);
                result += "\t\t\t<window id=\"" + 0 + "\">\n" +
                        "\t\t\t\t<x1>" + (int)w.getA().getX() + "</x1>\n" +
                        "\t\t\t\t<y1>" + (int)w.getA().getY() + "</y1>\n" +
                        "\t\t\t\t<x2>" + (int)w.getB().getX() + "</x2>\n" +
                        "\t\t\t\t<y2>" + (int)w.getB().getY() + "</y2>\n" + "\t\t\t</window>\n";
            }
            result += "\t\t</wall>\n";
=======
                "\t\t<xMin>" + xMin + "</xMin>\n" +
                "\t\t<xMax>" + xMax + "</xMax>\n" +
                "\t\t<yMin>" + yMin + "</yMin>\n" +
                "\t\t<yMax>" + yMax + "</yMax>\n" +
                "\t</core>\n" +
                "\t<walls>\n";
        int id = 0;
        for (Segment s : segmentsFound) {
            Segment d = s.getDoor();
            Segment w = s.getWindow();
            result += "\t\t<wall id=\"" + id + "\">\n" +
                    "\t\t\t<x1>" + s.getA().getX() + "</x1>\n" +
                    "\t\t\t<y1>" + s.getA().getY() + "</y1>\n" +
                    "\t\t\t<x2>" + s.getB().getX() + "</x2>\n" +
                    "\t\t\t<y2>" + s.getB().getY() + "</y2>\n";
            if (d!=null)
                    result += "\t\t\t<door id=\"" + 0 + "\">\n" +
                            "\t\t\t\t<x1>" + d.getA().getX() + "</x1>\n" +
                    "\t\t\t\t<y1>" + d.getA().getY() + "</y1>\n" +
                    "\t\t\t\t<x2>" + d.getB().getX() + "</x2>\n" +
                    "\t\t\t\t<y2>" + d.getB().getY() + "</y2>\n" +
                    "\t\t\t</door>\n"
                            ;
            if(w!= null)
                result += "\t\t\t<window id=\"" + 0 + "\">\n" +
                        "\t\t\t\t<x1>" + w.getA().getX() + "</x1>\n" +
                    "\t\t\t\t<y1>" + w.getA().getY() + "</y1>\n" +
                    "\t\t\t\t<x2>" + w.getB().getX() + "</x2>\n" +
                    "\t\t\t\t<y2>" + w.getB().getY() + "</y2>\n"+"\t\t\t</window>\n";
                result +="\t\t</wall>\n";
>>>>>>> Module_Test_Integration
            id++;
        }

        result += "\t</walls>\n" +
                "</plan>";

<<<<<<< HEAD
        Log.i("MPA_STEP", "EXPORT FINI");
        return result;
    }

    private ArrayList<Segment> getSegments() {
        return segmentsFound;
    }

    private void getMinMax() {
        xMin = segmentsFound.get(0).getA().getX();
        yMin = segmentsFound.get(0).getA().getY();
        xMax = segmentsFound.get(0).getA().getX();
        yMax = segmentsFound.get(0).getA().getY();
        for (Segment s : segmentsFound) {
=======
        return result;
    }

    private ArrayList<Segment> getSegments(){
        return segmentsFound;
    }

    private void getMinMax(){
        xMin = segmentsFound.get(0).getA().getX();
            yMin = segmentsFound.get(0).getA().getY();
            xMax = segmentsFound.get(0).getA().getX();
            yMax = segmentsFound.get(0).getA().getY();
        for (Segment s : segmentsFound){
>>>>>>> Module_Test_Integration
            if (s.getA().getX() < xMin)
                xMin = s.getA().getX();
            if (s.getA().getY() < yMin)
                yMin = s.getA().getY();
            if (s.getB().getX() < xMin)
                xMin = s.getB().getX();
            if (s.getB().getY() < yMin)
                yMin = s.getB().getY();
            if (s.getA().getX() > xMax)
                xMax = s.getA().getX();
            if (s.getA().getY() > yMax)
                yMax = s.getA().getY();
            if (s.getB().getX() > xMax)
                xMax = s.getB().getX();
            if (s.getB().getY() > yMax)
                yMax = s.getB().getY();
        }
    }
<<<<<<< HEAD

    private double minDoor() {
        if (doorsFound.size() == 0)
            return 0;
        double min = doorsFound.get(0).length();
        for (Segment d : doorsFound)
            if (d.length() < min)
                min = d.length();
        return min;
    }
=======
>>>>>>> Module_Test_Integration
}
