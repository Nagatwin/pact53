=== Tableau détaillé des tâches


[cols=",,^",options="header",]
|=================================================================================
| Tâche | Description                                           | Intégré au PAN4
| T1    | OpenGL                                                |
| T1.1  | Créer et afficher des formes simples                  | X
| T1.2  | Générer des textures à partir de PNG et les appliquer | X
| T1.3  | Réécrire le fichier OpenGL en .bsp lisible par CS     | X
| T1.4  | Créer une map à partir du VMF fourni par l'application| X
| T1.5  | Réaliser l'interface graphique en SWT                 | X
| T1.6  | Ajouter des objets préfabriqués (format obj)          | X
|=================================================================================

[cols=",,^",options="header",]
|=================================================================================
| Tâche | Description                                           | Intégré au PAN4
| T     | Traitement d'image                                                |
| T1    | Récupérer les positions des murs, fenetres et portes sur le dessin d'un plan                  | X
| T1.1  | Obtenir une image binaire noir/blanc avec l'algorithme d'Otsu| X
| T1.2  | Séparer les portes, murs et fenêtres selon leur couleur en utilisant le système HSV     | X
| T1.3  | Implémenter les morphologies mathématiques| X
| T1.4  | Implémenter l'algorithme de Ransac pour trouver les droites constituant le plan                 | X
| T1.5  | Obtenir les segments des murs à partir des droites obtenues avec Ransac         |X
| T2    | Construire le fichier xml contenant les coordonnées nécessaires        | X
| T3    | Reconnaitre un motif dans une image                                    |
|=================================================================================

[cols=",,^",options="header",]
|=================================================================================
| Tâche | Description                                           | Intégré au PAN4
| T     | Android                                               |
| T1    | Interface graphique         | X
| T1.1  | Création de l'application squelette  | X
| T1.2  | Possibilité de naviguer entre les différentes activités | X
| T1.3  | Organisation des layouts relatives aux activités| X
| T2  |    Implémentation des activités principales pour l'intégration              | X
| T2.1  |  Faire appel à l'activité "appareil photo" et stocker la photo prise dans une variable Bitmap       |X
| T2.2    | Renvoyer une imageview du plan reconnu par le module TI      | X
| T3    | Design et Interactivité|
| T3.1   | Design de l'application| X
| T3.2  | Accepter/refuser une photo en fonction de sa qualité| X
| T3.3  | Appel à la galerie pour charger des textures| X
| T3.4  | Interactivité du plan | X
|=================================================================================
