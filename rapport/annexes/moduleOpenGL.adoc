=== Module Open GL

* Expert : Jean Le Feuvre
* Etudiant 1 : Edward Tombre
* Etudiant 2 : Paul Monniot

==== Descriptions
Objectifs de ce module : +
Gérer l'environnement 3D dans le logiciel +
Exporter le projet en tant que fichier valide pour le moteur Source

==== Notes
* Gestion des collisions / vues caméra
* Possibilité de faire un premier export vers un fichier compatible avec un éditeur, puis d'automatiser une "conversion" à l'aide de cet éditeur.


==== Avancement

Voir compte rendu sur git : ModuleOpenGL/Exercices/Compte-rendu(1).pdf

==== Bibliographie spécifique

* le red book OpenGL:
http://www.glprogramming.com/red/

* la javadoc lwjgl:
https://javadoc.lwjgl.org/

* Snippets SWT:
http://git.eclipse.org/c/platform/eclipse.platform.swt.git/tree/examples/org.eclipse.swt.snippets/src/org/eclipse/swt/